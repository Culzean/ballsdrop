Balls Drop is a 3-D slalom racer. Race to the bottom to attain maxium speed and the best time!

Unzip the BallDrop folder into appropriate directory, eg. C:\Games
run c:\Games\BallDrop\Ball Demo.exe

Requirements:
graphics card supporting OpenGL version 3.1
Keyboard or pc controller

Multiplayer Requirements:
Multiplayer game requires at least one pc controller

PC controller:

Menu is accessed via the D-pad to move and 'A' to select.
You can choose the music track in the menu or exit.
Press 'start' to access the menu in game.
The left thumb stick is used for control.
Right trigger boost your speed!

Keyboard:
cursor keys used to navigate menu
cursor keys used to control ball
enter for selection
enter in game for boost!
backspace will return to main menu

Game will select a track at random to race on

Can you stay on the track all the way down? And set the best time?!


Credits:

Created by;
Daniel Waine
Kalid Salim
Chris Taylor

www.Mars-Station.com/

Textures:

see www.Mars-Station.com/?q=node/37 for further details

Music:

Daft Punk - Digital Love
AudioSlave - Cochise
TV on the Radio - DLZ

All music used under fair use for non-commercial teaching purposes