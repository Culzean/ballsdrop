// simple vertex shader - simple.vert
#version 140
// OpenGL 3.2 replace above with #version 150

struct Player {
	vec3 position;
	vec3 velocity;
};

const float MAX_VIEW = 358.0f;

uniform Player player;

uniform mat4x4 modelview;
uniform mat4x4 projection;
uniform vec4 lightPos;
uniform float time;
uniform vec4 playerPos;


in vec3 in_Position;
in vec3 in_Color;
in vec2 in_TexCoord;

out vec4 ex_Color;
out float ex_TexCoord;

// simple shader program
// particle vertex program
void main(void) {
	
	vec4 vertexPosition = modelview * vec4(in_Position,1.0);
	gl_Position = projection * vertexPosition;

	//determine size for rendering by distance to player
	float viewDist = distance( in_Position, player.position );
	float pointSize = (MAX_VIEW / viewDist);

	if(pointSize < 1)
		gl_PointSize = 0;
	else
		gl_PointSize = pointSize * 5.5;

	ex_Color = vec4(in_Color, 1.0);
	ex_TexCoord = in_TexCoord.x;
}
