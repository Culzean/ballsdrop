/*
	Author:		Floyd Chitalu
	BannerID:	B00203579
*/

#version 130

precision highp float;

struct light{
	vec4 position;
	vec4 diffuse;
	vec4 specular;
	vec4 ambient;
};

struct materialStruct{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	vec4 emissive;
	float shininess;
	float ReflectionFactor;
};

uniform light light0;
uniform materialStruct material0;

uniform samplerCube cubeMap;

in vec3 ex_N;
in vec3 lightPos;
in vec3 lightDir;
in float dist;
in vec4 ex_position;
in vec3 ex_ReflectionDirection;
in vec3 ex_RefractionDirection;

out vec4 final_Color;

void main(void){
	final_Color = material0.ambient * light0.ambient;	
	float NdotL = max(dot(ex_N, lightDir.xyz), 0.0);
			
	// Calculate R - reflection of light
	vec3 R = normalize(-reflect(lightDir, ex_N));
			
	final_Color += (material0.specular * light0.specular) * material0.shininess;		

	final_Color += material0.diffuse * light0.diffuse * NdotL;

	final_Color += material0.emissive;

	vec4 reflection = texture(cubeMap, ex_ReflectionDirection);
	vec4 refraction = texture(cubeMap, ex_RefractionDirection);
	
	final_Color = mix(reflection, refraction, material0.ReflectionFactor);
}
