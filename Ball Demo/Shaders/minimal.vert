// Vertex Shader � file "minimal.vert"

#version 130

uniform mat4 modelview;
uniform mat4 projection;

in  vec3 in_Position;
in  vec3 in_Color;

out vec3 ex_Color;

void main(void)
{
	ex_Color = in_Color;
	vec4 vertexPosition = modelview * vec4(in_Position,1.0);
	gl_Position = projection * vertexPosition;
}