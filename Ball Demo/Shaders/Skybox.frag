// cubeMap.frag cubeMap fragment shader
#version 130

// Some drivers require the following
precision highp float;

//out vec4 gl_FragColor;
smooth in vec3 cubeTexCoord;
uniform samplerCube cubeMap;

void main(void) {   
	// Fragment colour
	gl_FragColor = texture(cubeMap, cubeTexCoord);
}
