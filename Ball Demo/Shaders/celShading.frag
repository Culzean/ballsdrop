// simple fragment shader simple.frag
#version 130
// OpenGL 3.2 replace above with #version 150
// Some drivers require the following
precision highp float;
 

uniform sampler1D texture0;
uniform sampler2D texMap;

in vec3 ex_Color;
in vec3 ex_N;
in vec3 ex_V;
in vec3 ex_L;
in vec2 ex_TexCoord;
in float ONED_Coord;

// GLSL versions after 1.3 remove the built in type gl_FragColor
// If using a shader lang version greater than #version 130
// you *may* need to uncomment the following line:
// out vec4 gl_FragColor
 
void main(void) {
	// Fragment colour
	if(ex_Color.xyz == vec3(0.0))
		gl_FragColor = vec4(vec3(0.0), 1.0);
	else
	{
		gl_FragColor = texture(texMap,ex_TexCoord) * texture(texture0, ONED_Coord);
		//gl_FragColor = vec4(ex_TexCoord, 0.0f, 1.0f);
		//gl_FragColor = texture(texture0, ONED_Coord);
	}
}
