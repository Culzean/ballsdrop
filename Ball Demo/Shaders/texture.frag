// simple fragment shader simple.frag
#version 130
// OpenGL 3.2 replace above with #version 150
// Some drivers require the following
precision highp float;

// GLSL versions after 1.3 remove the built in type gl_FragColor
// If using a shader lang version greated than #version 130
// you *may* need to uncomment the following line:
// out vec4 gl_FragColor

struct lightStruct
{
	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
};

struct materialStruct
{
	vec4 ambient;
	vec4 diffuse;
	 vec4 specular;
	float shininess;
};

uniform lightStruct light;
uniform materialStruct material;
uniform sampler2D textureUnit0;

in vec2 ex_TexCoord;
in vec3 ex_N;
in vec3 ex_V;
in vec3 ex_L;

void main(void)
{
	//Ambient
	vec4 ambientI = light.ambient * material.ambient;

	//diffuse intensity
	vec4 diffuseI = light.diffuse * material.diffuse * max(dot(ex_N, ex_L),0);

	//Specular intensity
	// Calculate R - reflection of light
	vec3 R = normalize(-reflect(ex_L, ex_N));
	vec4 specularI = light.specular * material.specular;

	specularI = specularI * pow(max(dot(R, ex_V),0), material.shininess);

	vec4 litColour = ambientI + diffuseI + specularI;
	
	//Fragment colour
	gl_FragColor = litColour * texture2D(textureUnit0, ex_TexCoord);
	//gl_FragColor = litColour;
	//gl_FragColor = vec4(ex_N,1.0);
	//gl_FragColor = vec4(ex_TexCoord, 0.0f, 1.0f);
}