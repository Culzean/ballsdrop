#include "Demo.h"

Demo::Demo(void)
{
}

Demo::~Demo(void)
{
}

void Demo::Init(WindowManager windowManager, ShaderManager shaderManager, int musicSelection)
{
	FREE_CAMERA = false;
	UPDATING = false;
	count = 0;
	timer = 0;
	prevUpdating = false;
	selection = 0;

	textureManager.InitTextures();
	soundBank = SoundBank::GetInstance();

	projectionStack.loadMatrix(glm::perspective(45.0f, 4.0f / 3.0f, 0.5f, 5000.f));


	trackFactory = new TrackFactory(shaderManager );
	std::vector<TRACK_BENDS> theBends = trackFactory->SelectTrack( (rand() % 3) );
	pCrtTrack = trackFactory->GenerateTrack( theBends );
	pCrtTrack->ApplyTextures(textureManager.Load1DTexture("Res/1dTextureTrack1.txt"), textureManager.TRACK);

	BALL_MOVING = FALSE;
	ball = new Ball("Res/sphere.obj", glm::vec3(-50.0f, 20.0f, 20.0f));
	ball->ApplyTexture(textureManager.Load1DTexture("Res/1dTextureBall1.txt"), textureManager.BALL);
	ball->AssignShader(shaderManager.TOON);	
	
	terrain = new Terrain();
	terrain->ApplyTexture(textureManager.TERRAIN);
	terrain->ApplyShader(shaderManager.TEXTURE);
	//terrain->Generate(100, 100, 25, 25);
	terrain->Generate(256,256, 25, 25);
	terrainShader = shaderManager.TEXTURE;

	physicsM = new PhysicsModel(pCrtTrack);
	physicsM->AddBall(ball);


	//make an init method for the camera
	rotXAngle = 0.0f;
	rotYAngle = 0.0f;
	camera = new Camera(modelviewStack.getMatrix(), projectionStack.getMatrix());
	camera->SetProperties(camera->CAMERA_FLIGHT);

	camera->SetPosition(ball->GetPosition());

	glm::vec3 trackVec = glm::normalize(physicsM->GetCurrentPlane(0)->getNormal());
	trackVec = glm::vec3(trackVec.x, -1.0f, trackVec.z);
	currentLookVec = trackVec;

	camera->Move(currentLookVec, -17.5f);

	cameraMatrix = glm::lookAt(camera->GetPosition(), ball->GetPosition(), glm::vec3(0.0f,1.0f, 0.0f));



	skyBox = new SkyBox();
	skyBox->ApplyTexture(textureManager.SKYBOX);
	skyBox->AssignShader(shaderManager.SKYBOX);
	skyBox->Init();



	particle = new ParticleStandard( 580, 
		new Plane(glm::vec3(-100.0f, -70.0f, -10.0f), glm::vec3(100.0f, -70.0f, -10.0f), glm::vec3(-100.0f, 70.0f, -50.0f),glm::vec3(100.0f, 70.0f, -50.0f)) );
	particle->setTexID(textureManager.LoadTexBMP("Res/smoke1.BMP"));
	particle->AssignShader(shaderManager.PARTICLE);
	particle->setTexture2(textureManager.Load1DTexture("Res/1dTextureTrack1.txt"));
	//set particle system dist from player
	particle->setBall(ball, glm::vec3(0.0f, -80.0f, -20.0f) );


	grindEffect = new GrindParticles();
	grindEffect->ApplyTexture(textureManager.LoadTexBMP("Res/smoke1.BMP"));
	grindEffect->AssignShader(shaderManager.GRIND);
	grindEffect->Init(ball, 200);

	//debug rendering
	//this->debugRend = new DebugRend(200);
	//debugRend->AssignShader(shaderManager.COLOR_ONLY);
	//physicsM->addDebug(debugRend);

	hud.Init(windowManager, shaderManager);

	if (musicSelection == 0)
	{
		soundBank->rock.play();
		soundBank->rock.SetLoop(true);
	}
	if (musicSelection == 1)
	{
		soundBank->electro.play();
		soundBank->electro.SetLoop(true);
	}
	if (musicSelection == 2)
	{
		soundBank->weird.play();
		soundBank->weird.SetLoop(true);
	}

	NUM_COLLECTABLES = pCrtTrack->GetNumb() - 1; //racetrack includes a section that is outofbounds

	for (int i = 0; i < NUM_COLLECTABLES; ++i)
	{
		int random = rand() % 10;
		glm::vec3 direction = glm::vec3(pCrtTrack->getPlane()[i]->getPlanes()[random]->getNormal().x, 0, pCrtTrack->getPlane()[i]->getPlanes()[random]->getNormal().z);

		Collectable *arrow = new Collectable(pCrtTrack->getPlane()[i]->getPlanes()[random]->getPos() + glm::vec3(0.0, 1.0, 0.0), 
			-direction, "Res/arrow5.obj", shaderManager);

		arrows.push_back(arrow);
	}

	starter = 0.0f;

	std::cout << "Demo state initialized without fault." << std::endl;
}

void Demo::Update(GLfloat dt)
{
	//convert into seconds
	dt /= 1000;

	if (UPDATING)
	{
		timer += dt;
		hud.UpdateTimer(timer);

		if (!prevUpdating)
		{
			soundBank->rolling.play();
			soundBank->rolling.SetLoop(true);
		}
		physicsM->Update(camera->GetViewDirection(), dt);
		ball->Update();
		if(!ball->isOutOfBounds())
			terrain->Update(ball->GetPosition());
		hud.Update(ball, camera);
		particle->Update(camera, dt);
		glm::vec3 partsLooks = camera->GetViewDirection();
		//partsLooks.y = -1.0f;
		particle->setBall(ball, partsLooks);
		grindEffect->Update(ball, dt);
		UpdateCamera(dt);


		if(ball->GetPosition().y <= this->pCrtTrack->getLast().y)
		{
			selection = 2;
		}


		for (GLuint i = 0; i < arrows.size(); ++i)
		{
			if (arrows[i]->CollidesWith(ball))
			{
				arrows.erase(arrows.begin() + i);
				std::cout << "hit!" << std::endl;
				timer -= 5.0f;
			}
		}
	}
	else
	{
		UPDATING = false;

		starter += dt;
		if(starter > 3)
		{
			UPDATING = true;
			starter = 0.0f;
		}
	}

		prevUpdating = UPDATING;
}

void Demo::UpdateCamera(GLfloat dt)
{
	if (!FREE_CAMERA)
	{
		camera->SetPosition(ball->GetPosition());

		glm::vec3 trackVec = physicsM->GetCurrentPlane(0)->getNormal();
		trackVec = glm::vec3(trackVec.x, -0.5f, trackVec.z);
		glm::vec3 averageLookVec = (ball->getAvgVelocity() + trackVec) / 2.0f;
		

		glm::vec3 stepVec = averageLookVec - currentLookVec;
		float originalMag = glm::sqrt(stepVec.x * stepVec.x + stepVec.y * stepVec.y + stepVec.z * stepVec.z);
		
		stepVec = glm::normalize(stepVec);

		stepVec *= (75.0f * dt); 

		float newMag = (stepVec.x * stepVec.x + stepVec.y * stepVec.y + stepVec.z * stepVec.z);
		
		if(newMag < originalMag * originalMag)
			currentLookVec = currentLookVec + stepVec;
		else
			currentLookVec = averageLookVec;
		
		
		camera->Move(glm::normalize(currentLookVec), -17.5f);

		cameraMatrix = glm::lookAt(camera->GetPosition(), ball->GetPosition(), glm::vec3(0.0f,1.0f, 0.0f));
	}
}

void Demo::Render(bool WIRE_FRAME)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	if (WIRE_FRAME)
		glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
	else
		glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

	modelviewStack.loadIdentity();
	if(FREE_CAMERA)
		modelviewStack.push(camera->GetViewMatrix());
	else
		modelviewStack.push(cameraMatrix);
		
		glDisable(GL_DEPTH_TEST);
		skyBox->Render(projectionStack, modelviewStack, camera->GetPosition(), WIRE_FRAME);
		glEnable(GL_DEPTH_TEST);

		terrain->Render(projectionStack, modelviewStack);
		pCrtTrack->Render(projectionStack, modelviewStack, WIRE_FRAME);
		ball->Render(projectionStack, modelviewStack, WIRE_FRAME);
		particle->Render(projectionStack, modelviewStack, WIRE_FRAME);
		//debugRend->Render(projectionStack, modelviewStack, WIRE_FRAME);
		grindEffect->Render(projectionStack, modelviewStack);

		for (GLuint i = 0; i < arrows.size(); ++i)
		{
			arrows[i]->Render(projectionStack, modelviewStack, WIRE_FRAME);
		}

		hud.RenderDynamic(projectionStack, modelviewStack, WIRE_FRAME);

	modelviewStack.pop();

		hud.RenderStatic(projectionStack, modelviewStack, WIRE_FRAME);

		glFinish();
}

void Demo::KeyHandler( SDL_Event sdlEvent )
{
	if(sdlEvent.type == SDL_KEYDOWN)
	{
		switch(sdlEvent.key.keysym.sym)
		{
			case SDLK_1:
				if (!FREE_CAMERA)
				{
					std::cout << "FREE CAMERA" << std::endl;
					FREE_CAMERA = true;
				}
				else
				{
					std::cout << "FIXED CAMERA" << std::endl;
					FREE_CAMERA = false;
				}
			break;
			case SDLK_2:
				if (!UPDATING)
				{
					UPDATING = true;
					std::cout << "UPDATING" << std::endl;
				}
				else
				{
					UPDATING = false;
					std::cout << "PAUSED" << std::endl;
				}
			break;
			case SDLK_UP:
				if (UPDATING)
				{
					ball->SetPosZ(true);
				}
			break;
			case SDLK_DOWN:
				if (UPDATING)
				{
					ball->SetNegZ(true);
				}
			break;
			case SDLK_LEFT:
				if (UPDATING)
				{
					ball->SetNegX(true);
				}
			break;
			case SDLK_RIGHT:
				if (UPDATING)
				{
					ball->SetPosX(true);
				}
			break;
			case SDLK_RETURN:
				if (UPDATING)
				{
					ball->InputBoost(true);
				}

			break;
			default:
				break;
		}		
	}
	else if(sdlEvent.type == SDL_KEYUP)
	{
		switch(sdlEvent.key.keysym.sym)
		{
			case SDLK_UP:
				if (UPDATING)
				{
					ball->SetPosZ(false);
				}
			break;
			case SDLK_DOWN:
				if (UPDATING)
				{
					ball->SetNegZ(false);
				}
			break;
			case SDLK_LEFT:
				if (UPDATING)
				{
					ball->SetNegX(false);
				}
			break;
			case SDLK_RIGHT:
				if (UPDATING)
				{
					ball->SetPosX(false);
				}
			break;
			case SDLK_RETURN:
				if (UPDATING)
				{
					ball->InputBoost(false);
				}

			break;
			default:
				break;
		}
	}
}

void Demo::GetInput(CXBOXController *Player1)
{
	bool boost = false;

	if (Player1->IsConnected())
	{
		if (Player1->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_START && prevState.Gamepad.wButtons != XINPUT_GAMEPAD_START && !hud.IsPaused())
		{
			if (!UPDATING){
				UPDATING = true;
				hud.SetPaused(false);
			}
			else{
				if (count > 1)
				{
					UPDATING = false;
					hud.SetPaused(true);
				}
				count += 1;
			}
		}
		if (hud.IsPaused())
		{
			if (Player1->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_B)
			{
				selection = 1;
			}
			else
			if (Player1->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_A)
			{
				UPDATING = true;
				hud.SetPaused(false);
			}
		}
		hud.SetDisconnected(false);

		/*if (FREE_CAMERA){

			if(Player1->GetState().Gamepad.bRightTrigger != 0)
			{
				boost = true;
			}
			else
				boost = false;


			if (Player1->GetLeftMagnitude() != 0)
			{
				if(boost)
					camera->Move(Player1->GetLeftThumbX() * 0.001f, 0.0, Player1->GetLeftThumbY() * 0.001f);
				else
					camera->Move(Player1->GetLeftThumbX() * 0.0001f, 0.0, Player1->GetLeftThumbY() * 0.0001f);
			}

			if (Player1->GetRightMagnitude() != 0)
			{
				camera->Rotate(-Player1->GetRightThumbX() * 0.0001f, Player1->GetRightThumbY() * 0.0001f , 0.0);
			}
		}
		else*/ if(UPDATING)
		{
			ball->UseXBoxInput(Player1);
		}
	}
	/*else
	{
		hud.SetDisconnected(true);
		UPDATING = false;
	}*/

	prevState = Player1->GetState();
}

void Demo::Cleanup(void)
{
	ball->Cleanup();
	delete ball;
	delete camera;
	delete trackFactory;
	delete pCrtTrack;
	//delete debugRend;
	delete physicsM;
	delete skyBox;
	delete terrain;
	delete particle;
	delete grindEffect;

	for (GLuint i = 0; i < arrows.size(); ++i)
	{
		arrows[i]->Cleanup();
		delete arrows[i];
	}
	arrows.clear();
	textureManager.cleanUp();
	hud.Cleanup();
}