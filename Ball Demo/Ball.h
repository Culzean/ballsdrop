#ifdef _MSC_VER
	#pragma once
#endif

#ifndef BALL_H
#define BALL_H

#include <glm/gtc\matrix_transform.hpp>
#include <glm/gtc\type_ptr.hpp>

#include "OBJ_Model.h"
#include "MatrixStack.h"
#include "Frame.h"
#include "CXBOXController.h"
#include "stafx.h"

class CXBOXController;
class Plane;

const float MAX_VELOCITY = 120.0f;
const GLfloat MAX_OUTOFBOUNDS_TIME = 2.28f;

class Ball{

	struct lightStruct
	{
		glm::vec4 ambient;
		glm::vec4 diffuse;
		glm::vec4 specular;
		glm::vec4 position;
		glm::vec3 direction;
		float fieldOfLuminance;
		float intensityExponent;
		float constantAttenuation;
		float linearAttenuation;
		float quadraticAttenuation;
		float Eta;
	};

	struct materialStruct
	{
		glm::vec4 ambient;
		glm::vec4 diffuse;
		glm::vec4 specular;
		glm::vec4 emmisive;
		float shininess;
		float ReflectionFactor;
	}; 
		
	lightStruct light;
	materialStruct material;

	public:
		Ball(char *fileName, glm::vec3 position);
		~Ball(void);
		friend class PhysicsModel;

		void ApplyTexture(GLuint texture, GLuint texture2);
		void AssignShader(GLuint shader);
		void Update(void);
		void Render(MatrixStack &projectionStack, MatrixStack &modelviewStack, bool WIRE_FRAME);
		void Cleanup(void);
		
		void UpdatePhysics(GLfloat dt);
		void ResolveCollision( Ball* other, glm::vec3 colVec  );

		void SetPosition(glm::vec3 position) {ballFrame->SetOrigin(position);}
		glm::vec3 GetPosition(void) { return ballFrame->GetOrigin(); }
		GLfloat getRadius()					{	return radius;	}
		void setRadius(GLfloat val)			{	radius = val;	}
		GLfloat getMass()					{ return mass; }
		void setMass( GLfloat _mass )		{ mass = _mass; }

		glm::vec3 getVelocity()				{	return this->vVelocity;	}
		void setVelocity( glm::vec3 _vel )	{ vVelocity = _vel; }
		glm::vec3 getAvgVelocity()			{ return vVelocityAvg; }
		GLfloat getfVelocity()				{	return fVelocity;	}
		
		void setForwardVector(glm::vec3 &lhs)		{	ballFrame->SetForwardVector(lhs);	}
		glm::vec3 getForwardVector()				{	return ballFrame->GetForwardVector();	}

		void setOnGround( bool val )			{	onGround = val;	};
		bool isOnGround()						{	return onGround;	};

		Frame*	getFrame()										{	return ballFrame;	}

		void SetXRot(float newXRot)	{ xRot = newXRot; }
		void SetYRot(float newYRot)	{ yRot = newYRot; }
		void SetZRot(float newZRot)	{ zRot = newZRot; }

		float GetXRot(void) { return xRot; }
		float GetYRot(void) { return yRot; }
		float GetZRot(void) { return zRot; }

		glm::vec3 getUpdateVec()									{	return vBallUpdate;	};

		void addUpdateVec( glm::vec3 vecAdd, GLfloat dt )			{	vBallUpdate += vecAdd * dt; 	};
		float GetVelocityValue(void);
		void moveX(GLfloat amountX)								{	vBallUpdate.x += amountX;	};
		void moveY(GLfloat amountY)								{	vBallUpdate.y += amountY;	};
		void moveZ(GLfloat amountZ)								{	vBallUpdate.z += amountZ;	};

		//ui
		void UseXBoxInput(CXBOXController *Input1);
		//for keyboard input
		void SetNegX( bool val )							{ movingNegX = val; }
		void SetPosX( bool val )							{ movingPosX = val; }
		void SetNegZ( bool val )							{ movingNegZ = val; }
		void SetPosZ( bool val )							{ movingPosZ = val; }
		void InputBoost(bool val)		{ boost = val; }

		glm::vec3 vGravity;
		bool isOutOfBounds()				{	return outOfBounds;	}
		void setOutOfBounds( bool bounds )	{	outOfBounds = bounds;	}

		GLfloat getOutOfBoundsTime()		{ return outOfBoundsTimer; }

	protected:
		Frame *ballFrame;

		//variable to store average of velocity input
	private:

		void ResetPosition( Plane* _plane );

		enum {
			numVelAvg = 30
		};
		std::vector< glm::vec3 >	velocityHist;
		GLint		crtVelAvg;
		OBJ_Model *model;
		glm::vec3 vBallUpdate;
		glm::vec3 vVelocity;
		glm::vec3 vVelocityAvg;
		GLfloat fVelocity;
		bool	outOfBounds;
		GLfloat outOfBoundsTimer;
		GLuint vao;
		GLuint vbo[3];
		GLuint texture[2];
		GLuint shader;
		bool onGround;
		bool boost;
		bool movingPosX;
		bool movingNegX;
		bool movingPosZ;
		bool movingNegZ;
		GLfloat radius;
		GLfloat mass;
		float xRot;
		float yRot; 
		float zRot;
		glm::vec2 inputScale;
		glm::vec3 renderScale;
		std::vector<glm::vec3> vertices;
		std::vector<glm::vec3> normals;
		std::vector<glm::vec2> texCoords;
};

#endif