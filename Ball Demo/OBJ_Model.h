#ifdef _MSC_VER
	#pragma once
#endif

#ifndef OBJ_MODEL_H
#define OBJ_MODEL_H

#include <GL\glew.h>
#include <glm/glm.hpp>
#include <fstream>
#include <sstream>
#include <vector>


class OBJ_Model{

	public:
		OBJ_Model(void);
		~OBJ_Model(void);
		bool Load(char* fileName);
		bool LoadPointList(char* fileName);//used for track factory
		void ClearVectors(void);
		std::vector<glm::vec3> GetVertices() { return vertices; }
		inline std::vector<glm::vec3> GetNormals() { return normals; }
		inline std::vector<glm::vec2> GetTexCoords() { return texCoords; }

	private:
		std::vector <unsigned int>vertexIndices, texIndices, normalIndices;
		std::vector <glm::vec3>vertices, normals;
		std::vector <glm::vec2>texCoords;
		std::vector <glm::vec3>temp_vertices;
		std::vector <glm::vec2>temp_tex;
		std::vector <glm::vec3>temp_normals;
};

#endif