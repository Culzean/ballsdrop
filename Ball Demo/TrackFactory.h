#ifndef TRACKFACTORY_H
#define TRACKFACTORY_H


#include "stafx.h"
#include <vector>
#include "Frame.h"
#include "OBJ_Model.h"
#include <glm\gtx\vector_angle.hpp>

/*
		factory class

		to build sections of a track to order.
		
		parameters are the start positions and number of sections

		returns a container holding completed track complete with physics and rendering
	*/

class Plane;
class RaceTrack;
class TrackSect;
class PhysicsManager;
class TextureManager;

const float		TRACK_DEPTH = 22.0f;
const float		TRACK_WIDTH = 64.0f;

enum TRACK_BENDS{ TRK_STRAIGHT = 0, TRK_LEFT, TRK_RIGHT, TRK_JUMP, TRK_OUTOFBOUNDS };

class TrackFactory {

public:
	TrackFactory(ShaderManager _shaderManager);
	~TrackFactory();


	//from file
	Plane* buildTrack(const char* fname);
	std::vector< TRACK_BENDS > SelectTrack( int _select );
	//build from rand
	RaceTrack* GenerateTrack( const std::vector< TRACK_BENDS > theBends);

	RaceTrack* track;

private:

	ShaderManager shaderManager;
	PhysicsManager*	physics;
	
	OBJ_Model *trackLoader;

	std::vector<glm::vec3> vertices;
	std::vector<glm::vec3> normals;
	std::vector<glm::vec2> texCoords;

	std::vector< TRACK_BENDS > GetEasyTrack();
	std::vector< TRACK_BENDS > GetToughTrack();
	std::vector< TRACK_BENDS > GetNormalTrack();


	void AddOutOfBounds(glm::vec3 _bottomLeft, glm::vec3 _topRight, TrackSect* _pSect);
	void CompPoints( glm::vec3 &_left, glm::vec3 &_right, const glm::vec3 _comp );
	void CompBottomRight( glm::vec3 &_right, const glm::vec3 _near );
	void CompBottomLeft( glm::vec3 &_left, const glm::vec3 _near );
	glm::vec3 TrackFactory::TranslateSection(glm::vec3 _point, glm::vec3 diff);
	void RotateSection(glm::vec3 &_point, glm::vec2 v1, glm::vec2 v2);
	glm::vec3 findNorm(const glm::vec3 &v0, const glm::vec3 &v1);
};


#endif