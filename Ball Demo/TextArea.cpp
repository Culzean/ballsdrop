#include "TextArea.h"

///////////////////////////////////////////////////////////////////////////////////////
//The code for placing text to a texture is taken from Daniel Livingstones Label class.
///////////////////////////////////////////////////////////////////////////////////////


TextArea::TextArea(glm::vec3 centerPosition, char *fontFile, char *message, SDL_Color color, ShaderManager shaderManager)
{
	Error::CheckTTFInit();
	font = TTF_OpenFont(fontFile, 100);
	Error::CheckTTFOpenFont(font);
	textColor = color;
	centerPos = centerPosition;
	this->message = message;
	alpha = 1.0;
	shaderP = shaderManager.TEXTURE_ONLY;
	glGenTextures(1, &tex);
	TextToTexture(tex, message, font, textColor, width, height);
	MakeQuad(centerPosition, width, height);
}

TextArea::TextArea(glm::vec3 centerPosition, char *fileName, ShaderManager shaderManager)
{
	centerPos = centerPosition;
	shaderP = shaderManager.TEXTURE_ONLY;
	tex = TextureManager::loadTexture(fileName);
	alpha = 1.0;
	MakeQuad(centerPosition, width, height);
}

void TextArea::TextToTexture(GLuint &texture, char *message, TTF_Font *font, SDL_Color colour, GLuint &width, GLuint &height) {
        SDL_Surface *stringImage;
		stringImage = TTF_RenderText_Blended(font, message, colour);
		SDL_Color bg = {0, 0, 0};

        Error::TTF_RenderText_Blended(stringImage);

        width = stringImage->w;
        height = stringImage->h;

        GLuint colours = stringImage->format->BytesPerPixel;

        GLuint format, internalFormat;
        if (colours == 4) {   // alpha
                if (stringImage->format->Rmask == 0x000000ff)
                        format = GL_RGBA;
            else
                    format = GL_BGRA;
        } else {             // no alpha
                if (stringImage->format->Rmask == 0x000000ff)
                        format = GL_RGB;
            else
                    format = GL_BGR;
        }
        internalFormat = (colours == 4) ? GL_RGBA : GL_RGB;

        glBindTexture(GL_TEXTURE_2D, texture);
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S, GL_CLAMP);
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T, GL_CLAMP);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_LINEAR);
        glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, width, height, 0,
                    format, GL_UNSIGNED_BYTE, stringImage->pixels);

        // SDL surface was used to generate the texture but is no longer
        // required. Release it to free memory
        SDL_FreeSurface(stringImage);
}

void TextArea::UpdateTextString(char *newMessage)
{
	TextToTexture(tex, newMessage, font, textColor, width, height);
}

void TextArea::UpdateTextInteger(int value)
{
  char string[100];
  _itoa_s (value, string, 10);
  TextToTexture(tex, string, font, textColor, width, height);
}

void TextArea::UpdateTextFloat(float value)
{
	char string[100];
	sprintf_s(string, "%.2f", value);
	TextToTexture(tex, string, font, textColor, width, height);
}

void TextArea::SetColour(SDL_Color newColor)
{
	textColor = newColor;
	TextToTexture(tex, this->message, font, textColor, width, height);
} 

void TextArea::MakeQuad(glm::vec3 centerPosition, GLuint width, GLuint height)
{
	verts[0] = glm::vec3(0.0, 0.0, 10.0);
	verts[1] = glm::vec3(1.0, 0.0, 10.0);
	verts[2] = glm::vec3(1.0, 1.0, 10.0);
	verts[3] = glm::vec3(0.0, 1.0, 10.0);

	glGenVertexArrays(1, &vao); // Allocate & assign Vertex Array Object (VAO)
	glBindVertexArray(vao); // Bind VAO as current object

	glGenBuffers(1, vbo); // Allocate three Vertex Buffer Object (VBO)
	
	glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
	glBufferData(GL_ARRAY_BUFFER, 4 * sizeof(glm::vec3), verts, GL_STATIC_DRAW);
	glVertexAttribPointer(ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(ATTRIBUTE_VERTEX);
}

void TextArea::Render(MatrixStack &projectionStack, MatrixStack &modelviewStack, bool WIRE_FRAME, bool FLIPPED)
{
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glm::mat4 matrix(1.0);
	modelviewStack.push(matrix);
	
	matrix = glm::translate(matrix, centerPos);
	matrix = glm::rotate(matrix, 180.0f, glm::vec3(0.0f, 1.0f, 0.0f));

	modelviewStack.multiplyMatrix(matrix);

		glBindVertexArray(vao);
		glUseProgram(shaderP);
		int	uniformIndex = glGetUniformLocation(shaderP, "projection");
		glUniformMatrix4fv(uniformIndex, 1, GL_FALSE, glm::value_ptr(projectionStack.getMatrix()));
		uniformIndex = glGetUniformLocation(shaderP, "modelview");
		glUniformMatrix4fv(uniformIndex, 1, GL_FALSE, glm::value_ptr(modelviewStack.getMatrix()));
		uniformIndex = glGetUniformLocation(shaderP, "textureUnit0");
		glUniform1i(uniformIndex, 0);
		uniformIndex = glGetUniformLocation(shaderP, "flipped");
		glUniform1i(uniformIndex, FLIPPED);
		uniformIndex = glGetUniformLocation(shaderP, "alpha");
		glUniform1f(uniformIndex, alpha);


		if (WIRE_FRAME)
		{
			glDrawArrays(GL_LINE_LOOP, 0, 4);
		}
		else
		{
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, tex);
			glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
			glBindTexture(GL_TEXTURE_2D, 0);
			glActiveTexture(GL_TEXTURE0);
		}

	modelviewStack.pop();
}

TextArea::~TextArea(void)
{
}