#include <SFML\Audio.hpp>

#ifdef _MSC_VER
	#pragma once
#endif

#ifndef SOUNDEFFECT_H
#define SOUNDEFFECT_H

class SoundEffect{
public:
	void loadSound(char *filename);
	void play();
	void stop();
	sf::Sound::Status GetStatus(void) { return Sound.getStatus(); }
	void SetLoop(bool loop);

private:
	sf::SoundBuffer Buffer;
	sf::Sound Sound;
};

#endif