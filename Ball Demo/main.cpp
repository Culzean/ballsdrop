#include <SDL.h>
#include <GL\glew.h>
#include <ctime>

#include "WindowManager.h"
#include "Demo.h"
#include "Multi.h"
#include "Menu.h"
#include "Options.h"
#include "CXBOXController.h"
#include "Finish.h"

#if _DEBUG
#pragma comment(linker, "/subsystem:\"console\" /entry:\"WinMainCRTStartup\"")
#endif

bool RUNNING;
bool WIRE_FRAME = false;
int WIDTH = 1024, HEIGHT = 768;	//Window width, height!

WindowManager windowManager;	//WindowManager variable... providing window operations eg. fullscreen. 
ShaderManager shaderManager;
State *demo, *menu, *options, *currentState, *finish, *multi;
CXBOXController* Player1, *Player2;
XINPUT_STATE prevState;
SoundBank *soundBank;

int musicSelection = 0;
bool keyboardSelect;

#define FRAME_VALUES 10

time_t frameTimes[FRAME_VALUES];
GLint frameCount;

time_t currentTick;
time_t prevTick;
GLint currentFrame;
GLfloat deltaTime;
GLfloat fps;

void Init(void)
{
	shaderManager.InitShaders();
	soundBank = SoundBank::GetInstance();

	demo = new Demo();
	multi = new Multi(WIDTH, HEIGHT);
	menu = new Menu();
	options = new Options();
	finish = new Finish();

	keyboardSelect = false;
	menu->Init(windowManager, shaderManager, 0);

	Player1 = new CXBOXController(1);
	Player2 = new CXBOXController(2);
	currentState = menu;

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
	
	prevTick = 0;
	currentTick = SDL_GetTicks();
	deltaTime = 0;
	frameCount = 0;
	currentFrame = 0;
	memset (frameTimes, 0, sizeof(frameTimes));
	fps = 0;
	soundBank->InitSounds();

	SDL_GL_SetSwapInterval(1);
}

void Cleanup(void)
{
	shaderManager.ClearShaders();
	delete demo;
	delete multi;
	delete menu;
	delete options;
	delete finish;
	delete(Player1);
	delete Player2;
}

bool handleSDLEvent(SDL_Event const &sdlEvent)
{
	if(sdlEvent.type == SDL_QUIT)
		return false;


	currentState->KeyHandler(sdlEvent);
	

	if(sdlEvent.type == SDL_KEYDOWN)
	{
		//Can extend this to handle a wider range of keys
		switch(sdlEvent.key.keysym.sym)
		{
			case SDLK_0:
			if (WIRE_FRAME)
			{
				std::cout << "RENDER MODE" << std::endl;
				WIRE_FRAME = false;
			}
			else
			{
				std::cout << "WIRE-FRAME MODE" << std::endl;
				WIRE_FRAME = true;
			}
			break;

			case SDLK_RETURN:
				keyboardSelect = true;
			break;
			case SDLK_ESCAPE:
				return false;
			case SDLK_F1:
				windowManager.ToggleFullscreen();
				break;
			case SDLK_BACKSPACE:
				if(currentState != menu){
					currentState->SetSelection(0);
					currentState->Cleanup();
					soundBank->StopAllSounds();
					soundBank->confirm.play();
					menu->Init(windowManager, shaderManager, 0);
					currentState = menu;
				}
				break;
			default:
				break;
		}
	}
	else if(sdlEvent.type == SDL_KEYUP)
	{
		switch(sdlEvent.key.keysym.sym)
		{

			case SDLK_RETURN:
				keyboardSelect = false;
			break;
			default:
				break;
		}
	}

	return true;
}

void UpdateTimeStep() {

	//find the time step and update the average fps
	currentTick = ( SDL_GetTicks() );
	
	deltaTime = (GLfloat)(currentTick - prevTick);

	//std::cout << "Time Stamp " << deltaTime << " Time in milli " << std::endl;
	

	prevTick = currentTick;

	//reset the counter if we have taken a pause. ie debugging
	if(deltaTime > 100) {
		deltaTime = 80;
	}

	//find correct frame we are on
	//and record the time step
	currentFrame = frameCount++ % FRAME_VALUES;
	frameTimes[currentFrame] = (time_t)deltaTime;

	//find fps
	fps = 0;
	for(int i = 0; i < FRAME_VALUES; ++i) {
		fps += frameTimes[i];
	}
	fps /= FRAME_VALUES;
	fps = 1000 / fps;
}

void UpdateController(void)
{
	currentState->GetInput(Player1);

	if(currentState == multi)
		currentState->GetInput(Player2);

	//if(Player1->IsConnected())
	{
		if (currentState == menu)
		{
			if ( (Player1->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_A) || keyboardSelect == true)
			{
				soundBank->confirm.play();
				if (currentState->GetSelection() == 0) //play state selection
				{
					glClear(GL_COLOR_BUFFER_BIT);
					demo->Init(windowManager, shaderManager, options->GetMusic());
					currentState = demo;
					menu->Cleanup();
				}
				else
				if (currentState->GetSelection() == -1) // option state slection
				{
					glClear(GL_COLOR_BUFFER_BIT);
					multi->Init(windowManager, shaderManager, options->GetMusic());
					currentState = multi;
					menu->Cleanup();
				}
				else
				if (currentState->GetSelection() == -2) // quit selection
				{
					options->Init(windowManager, shaderManager, 0);
					currentState = options;
					menu->Cleanup();
					options->SetSelection(-3);
				}
				else
				if (currentState->GetSelection() == -3) // quit selection
				{
					RUNNING = false;
				}
			}
		}
		else
		if (currentState == demo)
		{
			if (demo->GetSelection() == 1)
			{
				menu->Init(windowManager, shaderManager, 0);
				currentState = menu;
				demo->SetSelection(0);
				demo->Cleanup();
				soundBank->StopAllSounds();
			}
			else
			if (demo->GetSelection() == 2)
			{
				finish->AddTime(demo->GetTime());
				finish->Init(windowManager, shaderManager, 0);
				currentState = finish;
				demo->SetSelection(0);
				demo->Cleanup();
				soundBank->StopAllSounds();
			}
		}
		else
		if (currentState == multi)
		{
			if (multi->GetSelection() == 1)
			{
				menu->Init(windowManager, shaderManager, 0);
				currentState = menu;
				multi->SetSelection(0);
				multi->Cleanup();
				soundBank->StopAllSounds();
			}
			else
			if (multi->GetSelection() == 2)
			{
				finish->AddTime(multi->GetTime());
				finish->Init(windowManager, shaderManager, 0);
				currentState = finish;
				multi->SetSelection(0);
				multi->Cleanup();
				soundBank->StopAllSounds();
			}
		}

		else
		if (currentState == options)
		{
			musicSelection = options->GetMusic();
			if (Player1->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_B)
			{
				glClear(GL_COLOR_BUFFER_BIT);
				menu->Init(windowManager, shaderManager, 0);
				currentState = menu;
				options->Cleanup();
			}
		}
		else
		if(currentState == finish)
		{
			if(Player1->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_B)
			{
				menu->Init(windowManager, shaderManager, 0);
				currentState = menu;
				finish->Cleanup();
			}
			else
			if(Player1->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_A)
			{
				demo->Init(windowManager, shaderManager, 0);
				currentState = demo;
				finish->Cleanup();
			}
		}
	}

	prevState = Player1->GetState();
}

void Update(float dt)
{
	currentState->Update(deltaTime);
	musicSelection = options->GetMusic();
	UpdateController();
}

void Render(void)
{
	currentState->Render(WIRE_FRAME);
	SDL_GL_SwapWindow(windowManager.GetWindow()); //swap buffers
}

int main(int argc, char *argv[])
{
	SDL_GLContext glContext; //OpenGl context handle
	SDL_Window *hWindow; //window handle
	SDL_Event sdlEvent; //variable to detect SDL events
	RUNNING = true; // variable to show game is running

	hWindow = windowManager.SetupRC(glContext, WIDTH, HEIGHT, "Balls Drop!");
	
	GLenum valid = glewInit();
	const GLubyte *version = glGetString( GL_VERSION );
	std::cout << "Found version of OpenGL : " << version << std::endl;
	Error::CheckGlewInitError(valid);
	Init();

	while(RUNNING)
	{
		SDL_PollEvent(&sdlEvent);
		RUNNING = handleSDLEvent(sdlEvent);
		Update(deltaTime);
		UpdateTimeStep();
		Render();
	}

	Cleanup();
	SDL_DestroyWindow(hWindow);
	SDL_GL_DeleteContext(glContext);
	SDL_Quit();

	return 0;
}