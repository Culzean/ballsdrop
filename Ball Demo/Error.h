#ifdef _MSC_VER
	#pragma once
#endif

#ifndef ERROR_H
#define ERROR_H

#include <GL\glew.h>
#include <SDL_ttf.h>
#include <iostream>


class Error{

public:
	static void ExitFatalError(char *message)	//Something went wrong - print error message and quit!
	{
		std::cout << message << " " << std::endl;
		std::cout << SDL_GetError();
		SDL_Quit();
		exit(1);
	}

	static void ReportError(char *message)		//Something went wrong - print error message!
	{
		std::cout << message << " " << std::endl;
		std::cout << SDL_GetError();
	}

	static void CheckGlewInitError(GLenum valid)
	{
		if(GLEW_OK != valid)
			ExitFatalError("glewInit failed, aborting!");
		else
			std::cout << "GLEW initialised without fault" << std::endl;
	}


	//Place the line below into frame buffer initialisation code and pass valid to the function to check condition of framebuffer!
	//GLenum valid = glCheckFramebufferStatus(GL_DRAW_FRAMEBUFFER);
	static void CheckFramebufferErrors(GLenum valid)
	{
		
		if (valid != GL_FRAMEBUFFER_COMPLETE)
		std::cout << "Framebuffer object not complete!" << std::endl;
		if (valid == GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT)
		std::cout << "Framebuffer incomplete attachment!" << std::endl;
		if (valid == GL_FRAMEBUFFER_UNSUPPORTED)
		std::cout << "FBO attachments unsupported!" << std::endl;
	}

	static void CheckTTFInit()
	{
		if (TTF_Init() != 0)
		{
			std::cout << "TTF_Init() Failed: " << TTF_GetError() << std::endl;
		}
	}

	static void CheckTTFOpenFont(TTF_Font *font)
	{
		if (font == NULL)
		{
			std::cout << "TTF_OpenFont() Failed: " << TTF_GetError() << std::endl;
		}
	}

	static void TTF_RenderText_Blended(SDL_Surface *text)
	{
		if (text == NULL)
		{
			std::cout << "TTF_RenderText_Solid() Failed: " << TTF_GetError() << std::endl;
		}
	}
};

#endif