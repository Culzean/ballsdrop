#ifndef DEMO_H
#define DEMO_H

#include <iostream>
#include <cstdlib>

#include "stafx.h"
#include "State.h"
#include "HUDSystem.h"
#include "Collectable.h"


class Demo : public State
{
	public:
		Demo(void);
		~Demo(void);

		void Init(WindowManager windowManager, ShaderManager shaderManager, int musicSelection);
		void Render(bool WIRE_FRAME);
		void KeyHandler( SDL_Event sdlEvent);
		void GetInput(CXBOXController *Player1);
		void Update(GLfloat dt);
		void UpdateCamera(GLfloat dt);
		glm::quat RotateTowards(glm::quat q1, glm::quat q2, float maxAngle);
		void Cleanup(void);
		int GetSelection(void) {return selection;}
		void SetSelection(int value) {selection = value;}
		int GetMusic(void) {return 0;}
		float GetTime(void) { return timer;}
		void AddTime(float time) {;}

		friend class Ball;

	private:
		void InitSoundEffects(int musicSelection, SoundBank sounds);
		bool FREE_CAMERA;
		bool UPDATING;
		bool BALL_MOVING;


		TextureManager textureManager;

		glm::mat4x4 cameraMatrix;
		glm::vec3 currentLookVec;
		
		
		MatrixStack projectionStack;
		MatrixStack modelviewStack;

		Ball					*ball;
		TrackFactory			*trackFactory;
		RaceTrack				*pCrtTrack;
		Camera					*camera;
		PhysicsModel			*physicsM;
		SkyBox					*skyBox;
		Terrain					*terrain;
		GLuint					terrainShader;
		ParticleStandard		*particle;
		GrindParticles			*grindEffect;
		DebugRend				*debugRend;
		HUDSystem				hud;
		XINPUT_STATE			prevState;
		SoundBank				*soundBank;

		std::vector<Collectable*> arrows;
		int NUM_COLLECTABLES;

		float rotXAngle;
		float rotYAngle;
		float prevXCameraAngle;

		int count;
		bool prevUpdating;
		float timer;
		int selection;
};

#endif