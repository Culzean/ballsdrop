#include "RaceTrack.h"


RaceTrack::RaceTrack(PhysicsManager* _physics) {
	physics = _physics;
	trackFrame = new Frame();
	trackFrame->SetOrigin(glm::vec3(0.0, 0.0, -100.0));


	light.ambient = glm::vec4( 0.6, 0.6, 0.6, 1.0);				//ambient
	light.diffuse = glm::vec4( 0.4, 0.4, 0.4, 1.0);				//diffuse
	light.specular = glm::vec4( 0.1, 0.1, 0.1, 1.0),			//specular
	light.position = glm::vec4(0.0f, 0.0f, 0.0f, 1.0);			//position

	material.ambient = glm::vec4( 0.1, 0.1, 0.1, 1.0 ); //ambient
	material.diffuse = glm::vec4( 0.4, 0.4, 0.4, 1.0 ); //diffuse
	material.specular = glm::vec4( 0.2, 0.2, 0.2, 1.0 ); //specular
	material.emmisive = glm::vec4( 0.0, 0.0, 0.0, 1.0);	//emmisive
	material.shininess = 5.0f;
	material.ReflectionFactor = 0.6f;
	crtSect = 0;
}

RaceTrack::~RaceTrack() {
	for(GLuint i = 0; i< raceTrack.size(); ++i)
	{
		delete raceTrack[i];
	}
	delete trackFrame;
}

void RaceTrack::Update(GLfloat dt) {
}

void RaceTrack::Init() {

}

void RaceTrack::ApplyTextures(GLuint texture, GLuint texture2)
{
	this->texture[0] = texture;
	this->texture[1] = texture2;
}

void RaceTrack::Render( MatrixStack &projectionStack, MatrixStack &modelviewStack, bool wireFrame ) {
	glBindVertexArray(vao);
	glUseProgram(this->shaderID);

	int uniformIndex = glGetUniformLocation(shaderID, "projection");
			glUniformMatrix4fv(uniformIndex, 1,GL_FALSE,glm::value_ptr(projectionStack.getMatrix()));
			uniformIndex = glGetUniformLocation(shaderID, "modelview");
			glUniformMatrix4fv(uniformIndex, 1,GL_FALSE,glm::value_ptr(modelviewStack.getMatrix()));

			uniformIndex = glGetUniformLocation(shaderID, "texture0");
			glUniform1i(uniformIndex, 0);

			uniformIndex = glGetUniformLocation(shaderID, "texMap");
			glUniform1i(uniformIndex, 1);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_1D, texture[0]);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, texture[1]);
		//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo[ATTRIBUTE_INDICES]);
		//glDrawElements(GL_TRIANGLE_STRIP, this->iNoFaces, GL_UNSIGNED_INT, 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glDrawArrays(GL_TRIANGLES, 0, iNoVerts);
		//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindTexture(GL_TEXTURE_2D, 0);

	glUseProgram(0);
	glBindVertexArray(0);
}


TrackSect* RaceTrack::findSection( glm::vec3 pos, GLfloat rad ) {

	for(GLuint j = 0; j < raceTrack.size(); j++)
	{
		if( physics->findTrackSect(raceTrack[j], pos, rad) )
		{
			if(!raceTrack[j]->getStart()->isOutOfBounds())
			crtSect = j;
			return raceTrack[j];
		}
	}
	//return the last section
	return raceTrack[raceTrack.size() -1];
}

TrackSect* RaceTrack::getSection()
{ 
	if(crtSect >= 0 && crtSect < raceTrack.size())
		return raceTrack[crtSect]; 
	else {
		return raceTrack[raceTrack.size()-1];
	}

}