#pragma once
#include "renderable.h"
class CloudEffect :
	public Renderable
{
public:

	struct CloudPoint {
		glm::vec3 position;
		GLuint texture;
	};

	struct Cloud {
		glm::vec3 position;
		GLint iCloudPoints;
	};

	CloudEffect(const int n);
	virtual ~CloudEffect(void);

	virtual void Init();
	virtual void Update(float dt);
	virtual void Render( MatrixStack &projectionStack, MatrixStack &modelviewStack, bool WIRE_FRAME );

private:

	int noParticles;
	glm::vec3 ballPosition;
	glm::vec3 ballVector;
	void createParticle(CloudPoint* p);
	void initBuffers(void);
};

