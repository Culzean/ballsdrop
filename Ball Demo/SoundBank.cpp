#include "SoundBank.h"

SoundBank* SoundBank::soundBank = 0;

SoundBank::SoundBank()
{
}

void SoundBank::InitSounds(void)
{
	navigate.loadSound("Res/navigate.wav");
	confirm.loadSound("Res/confirm.wav");
	rolling.loadSound("Res/rolling.wav");
	rock.loadSound("Res/cochise.ogg");
	electro.loadSound("Res/daft.ogg");
	weird.loadSound("Res/DLZ.ogg");
}

void SoundBank::StopAllSounds(void)
{
	if (rock.GetStatus() == sf::Sound::Playing)
	{
		rock.stop();
	}
	if (electro.GetStatus() == sf::Sound::Playing)
	{
		electro.stop();
	}
	if (weird.GetStatus() == sf::Sound::Playing)
	{
		weird.stop();
	}
	if (rolling.GetStatus() == sf::Sound::Playing)
	{
		rolling.stop();
	}
}

SoundBank::~SoundBank()
{
}