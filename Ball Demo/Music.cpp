#include "Music.h"

void Music::loadSound(char *filename){

	if (!music.openFromFile(filename))
	{
		//Error
	}
}

void Music::play()
{
	music.play();
}

void Music::stop()
{
	music.stop();
}

void Music::SetLoop(bool loop)
{
	if (loop)
	{
		music.setLoop(true);
	}
	else
	{
		music.setLoop(false);
	}
}