#include "GrindParticles.h"

GrindParticles::GrindParticles(void)
{
}

GrindParticles::~GrindParticles(void)
{
	delete[] particles;
	delete[] vertices;
	delete[] colours;
}

void GrindParticles::Init(Ball* ball, int noParticles)
{
	this->noParticles = noParticles;

	particles = new Particle[noParticles];
	vertices = new glm::vec3[noParticles];
	colours = new glm::vec3[noParticles];

	ballPosition = ball->GetPosition() - glm::vec3(0.0, ball->getRadius(), 0.0);
	ballVector = ball->getForwardVector();

	for(int i=0; i < noParticles; i++)
	{

		createParticle(&particles[i]);

		vertices[i] = particles[i].position;
		colours[i] = glm::vec3((float)rand() / RAND_MAX, (float)rand()/ RAND_MAX, (float)rand() / RAND_MAX);
	}

	initBuffers();


	glEnable(GL_DEPTH_TEST);
	glEnable(GL_POINT_SPRITE);
	glEnable(GL_PROGRAM_POINT_SIZE);
}

void GrindParticles::createParticle(Particle* p)
{
	p->position = ballPosition;
	p->velocity = -ballVector + glm::vec3(10.5 + (float)rand()/((float)RAND_MAX/(10.5, 10.5)),
											10.5 + (float)rand()/((float)RAND_MAX/(10.5, 10.5)),
											0.0f);
	//std::cout << p->velocity.p << " " << p->velocity.s << " " << p->velocity.t << "\n";
	/*p->velocity = -ballVector + glm::vec3(5.25 + (float)rand()/((float)RAND_MAX/(10.5, 5.25)),
											5.25 + (float)rand()/((float)RAND_MAX/(10.5, 5.25)),
											5.25 + (float)rand()/((float)RAND_MAX/(10.5, 5.25)));*/
	p->timeAlive = 0.0f;
	p->lifeSpan = 0.25f + glm::compRand1(0.0f, 0.25f);
}

void GrindParticles::Update(Ball* ball, float dt)
{
	ballPosition = ball->GetPosition() - glm::vec3(0.0, ball->getRadius(), 0.0);
	ballVector = ball->getVelocity();

	//advanced each particle by STEP_TIME
	for(int i = 0; i < noParticles; i++)
	{
		//Particle* p = particles + i;

		//particles[i].position += particles[i].velocity * 0.1f * dt;
		particles[i].timeAlive += dt;

		if(particles[i].timeAlive > particles[i].lifeSpan)
		{
			createParticle(&particles[i]);
		}

		
		
		vertices[i] = particles[i].position;
	}

	glBindBuffer(GL_ARRAY_BUFFER, vbo[0]); //Bind 1st VBO as active uffer object
	//Copy the vertex data from model to VBO
	glBufferData(GL_ARRAY_BUFFER, noParticles * sizeof(glm::vec3), vertices, GL_DYNAMIC_DRAW);
	// Position data is going into attribute index 0 & has 3 floats per vertex
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);  //Enable attribute index 0 (position)


}

void GrindParticles::ApplyTexture(GLuint texID)
{
	this->texID = texID;
}

void GrindParticles::AssignShader(GLuint shader)
{
	this->shaderID = shader;
}


void GrindParticles::initBuffers(void)
{
	glGenVertexArrays(1, &vao); // Allocate & assign Vertex Array Object (VAO)
	glBindVertexArray(vao); // Bind VAO as current object



	glGenBuffers(2, vbo); // Allocate three Vertex Buffer Object (VBO)
	
	glBindBuffer(GL_ARRAY_BUFFER, vbo[0]); //Bind 1st VBO as active uffer object
	//Copy the vertex data from model to VBO
	glBufferData(GL_ARRAY_BUFFER, noParticles * sizeof(glm::vec3), vertices, GL_DYNAMIC_DRAW);
	// Position data is going into attribute index 0 & has 3 floats per vertex
	glVertexAttribPointer(ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(ATTRIBUTE_VERTEX);  //Enable attribute index 0 (position)
	

	// Colours data in attribute 1, 3 floats per vertex
	glBindBuffer(GL_ARRAY_BUFFER, vbo[1]); // bind VBO for colours
	glBufferData(GL_ARRAY_BUFFER, noParticles *	sizeof(glm::vec3), colours, GL_STATIC_DRAW);
	glVertexAttribPointer(ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(ATTRIBUTE_COLOR);    // Enable attribute index 1

	glBindVertexArray(0);
}

void GrindParticles::Render(MatrixStack &projectionStack, MatrixStack &modelviewStack)
{
	glBindVertexArray(this->vao); // bind member vao
	glUseProgram(shaderID);

	//glDisable(GL_DEPTH_TEST);

	glEnableVertexAttribArray(ATTRIBUTE_VERTEX);
	glEnableVertexAttribArray(ATTRIBUTE_COLOR);
	glEnable(GL_POINT_SPRITE);

	int uniformIndex = glGetUniformLocation(shaderID, "projection");
	glUniformMatrix4fv(uniformIndex, 1,GL_FALSE,glm::value_ptr(projectionStack.getMatrix()));
	uniformIndex = glGetUniformLocation(shaderID, "modelview");
	glUniformMatrix4fv(uniformIndex, 1,GL_FALSE,glm::value_ptr(modelviewStack.getMatrix()));

	GLint texUniform = glGetUniformLocation(shaderID, "texture0");
	glUniform1i( texUniform, 0 );

	// Now draw the particles... as easy as this!
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA ); //RUGH?
	glDepthMask(0);
	glEnable(GL_BLEND);
	//glDisable(GL_DEPTH_TEST);

	glPointSize(10);
	glLineWidth(20.0);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texID);
	glDrawArrays(GL_LINES, 0, noParticles);

	glDisableVertexAttribArray(ATTRIBUTE_COLOR);
	glDisableVertexAttribArray(ATTRIBUTE_VERTEX);
	glDisable(GL_BLEND);
	glDepthMask(1);
	//glEnable(GL_DEPTH_TEST);
}