#pragma once
#include <glm\glm.hpp>
#include <glm\ext.hpp>
#include <iostream>

class Ball;

#include "stafx.h"

class Camera{
public:
	enum CamProperties{
		CAMERA_FIRST_PERSON,
		CAMERA_FLIGHT
	};

	Camera();
	Camera(glm::mat4 &modelview, glm::mat4 &projection);
	~Camera();

	void LookAt(const glm::vec3 &position);
	void LookAt(const glm::vec3 &eye, const glm::vec3 &position, const glm::vec3 &up);
	void Move(float x, float y, float z);
	void Move(const glm::vec3 &direction, const glm::vec3 &amount);
	void Camera::Move(const glm::vec3 &direction, const GLfloat &amount);
	void Perspective(float fovx, float aspect, float znear, float zfar);
	void Rotate(float headingDegrees, float pitchDegrees, float rollDegrees);
	void UpdatePosition(const glm::vec3 &direction, float secondsPassed);
	void ResetView(Ball* pBall);

	//Some simple vector3 calculations 
	inline glm::vec3 Inverse(glm::vec3 vec) const{ return glm::vec3(-vec[1], -vec[2], -vec[3]);}
	inline float MagnitudeSqaured(glm::vec3 vec)const{return (vec[1] * vec[1], vec[2] * vec[2], vec[3] * vec[3]);}
	inline bool CloseEnough(float f1, float f2){ return fabsf((f1 - f2) / ((f2 == 0.0f) ? 1.0f : f2)) < EPSILON;}

	//Getter methods.
	inline const glm::vec3 &GetAcceleration() const{return c_Acceleration;};
	inline CamProperties GetProperties() const{return c_Properties;};
	inline const glm::vec3 &GetCurrentVelocity() const{return c_CurrentVelocity;}
	inline const glm::quat &GetOrientation() const{return c_Orientation;}
	inline const glm::vec3 &GetPosition() const{return c_Eye;};
	inline const glm::mat4 &GetProjectionMatrix() const{return c_ProjMatrix;}
	inline const glm::vec3 &GetVelocity() const{return c_Velocity;}
	inline const glm::vec3 &GetViewDirection() const{return c_ViewDir;}
	inline const glm::mat4 &GetViewMatrix() const{return c_ViewMatrix;}
	inline const glm::mat4 &GetIdentMatrix() const {return c_IdMatrix;}
	inline const glm::vec3 &GetXAxis() const{return c_XAxis;}
	inline const glm::vec3 &GetYAxis() const{return c_YAxis;}
	inline const glm::vec3 &GetZAxis() const{return c_ZAxis;}
	inline const glm::vec3 &GetCamAccel() const{return c_Acceleration;}
	inline const glm::vec3 &GetCamVel() const{return c_Velocity;}
	inline const float &GetFOVX() const{return c_Fovx;}
	inline const float &GetZNear() const{return c_ZNear;}
	inline const float &GetZFar() const{return c_ZFar;}
	inline const float &GetPosX() const{return c_PosX;}
	inline const float &GetPosY() const{return c_PosY;}
	inline const float &GetPosZ() const{return c_PosZ;}
	
	//Setter methods.
	void SetAcceleration(float x, float y, float z);
	void SetAcceleration(const glm::vec3 &acceleration);
	void SetProperties(CamProperties newProperties);
	void SetCurrentVelocity(float x, float y, float z);
	void SetCurrentVelocity(const glm::vec3 &currentVelocity);
	void SetOrientation(const glm::quat &orientation);
	void SetPosition(float x, float y, float z);
	void SetPosition(const glm::vec3 &position);
	void SetVelocity(float x, float y, float z);
	void SetVelocity(const glm::vec3 &velocity);
	void SetViewDirection(float x, float y, float z);
	void SetViewDirection(glm::vec3 &direction);
	void SetViewMatrix(const glm::mat4 &modelview);
	void SetProjMatrix(const glm::mat4 &projection);

private:
	void RotateFlight(float headingDegrees, float pitchDegrees, float rollDegrees);
	void RotateFirstPerson(float headingDegrees, float pitchDegrees);
	void UpdateVelocity(const glm::vec3 &direction, float secondsPassed);
	void UpdateViewMatrix(void);

	static const float DEFAULT_FOVX;
	static const float  DEFAULT_ZFAR;
	static const float  DEFAULT_ZNEAR;

	static const glm::vec3 WORLD_XAXIS;
	static const glm::vec3 WORLD_YAXIS;
	static const glm::vec3 WORLD_ZAXIS;
	float EPSILON;

	CamProperties c_Properties;
	float c_Fovx;
	float c_ZNear;
	float c_ZFar;
	float c_AspectRatio;
	float c_PitchDegrees;
	float c_PosX, c_PosY, c_PosZ;
	glm::vec3 c_Eye;
	glm::vec3 c_XAxis;
	glm::vec3 c_YAxis;
	glm::vec3 c_ZAxis;
	glm::vec3 c_ViewDir; 
	glm::vec3 c_Acceleration;
	glm::vec3 c_CurrentVelocity;
	glm::vec3 c_Velocity;
	glm::quat c_Orientation;
	glm::mat4 c_ViewMatrix;
	glm::mat4 c_ProjMatrix;
	glm::mat4 c_IdMatrix;
	glm::mat4 identity;
};