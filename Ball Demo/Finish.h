#pragma once

#ifndef FINISH_H
#define FINISH_H

#include "State.h"
#include "TextArea.h"
#include <fstream>

class Finish : public State
{
public:
	Finish(void);
	~Finish(void);
	void Init(WindowManager windowManager, ShaderManager shaderManager, int musicSelection);
	void Update(GLfloat dt);
	void GetInput(CXBOXController *Player1);
	void KeyHandler( SDL_Event sdlEvent) {;}
	void Render(bool WIRE_FRAME);
	void Cleanup(void);
	int  GetSelection(void) { return selection; }
	void SetSelection(int value) { selection = value; }
	int GetMusic(void) {return musicSelection;}
	float GetTime(void) { return 0;}
	void AddTime(float time);
	void OrganiseScores(void);

private:
	MatrixStack		projectionStack;
	MatrixStack		modelviewStack;
	XINPUT_STATE	prevState;
	TextArea		*you;
	TextArea		*score1, *score2, *score3;
	SoundBank		*soundBank;
	int				selection;
	int				musicSelection;
	int				themeSelection;
	bool			subMenu;
	float			tempTime;
	float			scores[3];
	TextArea		*ok;
	TextArea		*back;
	TextArea		*resume;
	TextArea		*menu;
};

#endif