#include "HUDSystem.h"



void HUDSystem::Init(WindowManager windowManager, ShaderManager shaderManager)
{
	SDL_Color blue = {0, 205, 102};
	SDL_Color color = {0, 0, 100};
	HUD_velocity = new TextArea(glm::vec3(0.0f, 0.0f, 0.0f), "digital-7.ttf", "0", blue, shaderManager);
	HUD_velocity->SetAlpha(0.6f);
	HUD_altitude = new TextArea(glm::vec3(-4.0f, -4.0f, -0.0f), "digital-7.ttf", "DEPTH:", blue, shaderManager);
	HUD_altitudeValue = new TextArea(glm::vec3(-3.0f, -4.0f, 0.0f), "digital-7.ttf", "0", blue, shaderManager);
	HUD_altitudeValue->SetAlpha(0.6f);
	HUD_disconnected = new TextArea(glm::vec3(0.5, 0.0, 2.0), "Res/nocontrol.bmp", shaderManager);
	HUD_timerValue = new TextArea(glm::vec3(5.0f, 3.0f, 0.0f), "digital-7.ttf", "0", blue, shaderManager);
	HUD_timer = new TextArea(glm::vec3(3.5f, 3.0f, 0.0f), "digital-7.ttf", "TIME:", blue, shaderManager);
	
	HUD_ok = new TextArea(glm::vec3(-1.0, 1.0, -3.0), "Res/a.bmp", shaderManager);
	HUD_back = new TextArea(glm::vec3(-1.0, -1.0, -3.0), "Res/b.bmp", shaderManager);
	HUD_resume = new TextArea(glm::vec3(0.5, 0.5, 0.0), "GROBOLD.ttf", "RESUME", color, shaderManager);
	HUD_menu = new TextArea(glm::vec3(0.5, -1.0, 0.0), "GROBOLD.ttf", "MENU", color, shaderManager);
	HUD_ok->SetAlpha(0.6f);
	HUD_back->SetAlpha(0.6f);
	HUD_resume->SetAlpha(0.8f);
	HUD_menu->SetAlpha(0.8f);
	
	this->SetDisconnected(false);

	PAUSED = false;
	timer = 0.0f;
}

void HUDSystem::Update(Ball *ball, Camera *camera)
{
	HUD_velocity->SetPosition(ball->GetPosition() + glm::vec3(0.0f, 2.0f, 10.0f));
	HUD_velocity->UpdateTextInteger((GLint)ball->GetVelocityValue());
	HUD_altitudeValue->UpdateTextInteger((GLint)ball->GetPosition().y + 4);
	HUD_timerValue->UpdateTextFloat(timer);
}

void HUDSystem::RenderDynamic(MatrixStack &projectionStack, MatrixStack &modelviewStack, bool WIRE_FRAME)
{
	if (!PAUSED)
	{
		HUD_velocity->Render(projectionStack, modelviewStack, WIRE_FRAME, false);
	}
}

void HUDSystem::RenderStatic(MatrixStack &projectionStack, MatrixStack &modelviewStack, bool WIRE_FRAME)
{
	glDisable(GL_DEPTH_TEST);
	
	if (PAUSED)
	{
		HUD_back->Render(projectionStack, modelviewStack, WIRE_FRAME, true);
		HUD_ok->Render(projectionStack, modelviewStack, WIRE_FRAME, true);
		HUD_menu->Render(projectionStack, modelviewStack, WIRE_FRAME, true);
		HUD_resume->Render(projectionStack, modelviewStack, WIRE_FRAME, true);
	}
	else
	{
		HUD_altitude->Render(projectionStack, modelviewStack, WIRE_FRAME, true);
		HUD_altitudeValue->Render(projectionStack, modelviewStack, WIRE_FRAME, true);
		HUD_timerValue->Render(projectionStack, modelviewStack, WIRE_FRAME, true);
	}
	if (DISCONNECTED)
	{
		HUD_disconnected->Render(projectionStack, modelviewStack, WIRE_FRAME, true);
	}

	HUD_timer->Render(projectionStack, modelviewStack, WIRE_FRAME, true);

	glEnable(GL_DEPTH_TEST);
}

void HUDSystem::Cleanup(void)
{
	delete		HUD_velocity;
	delete		HUD_altitude;
	delete		HUD_altitudeValue;
	delete		HUD_disconnected;
	delete		HUD_timer;
	delete		HUD_timerValue;
	delete		HUD_back;
	delete		HUD_ok;
	delete		HUD_resume;
	delete		HUD_menu;
}