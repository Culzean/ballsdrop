#ifndef _HEIGHTFIELD_H_
#define _HEIGHTFIELD_H_


#include <iostream>
#include <SDL.h>

class HeightField
{
	public:
		HeightField(void);
		~HeightField(void);
		bool Create(char *filename, const int width, const int height);
		void Render(void);

		float getHeight(int _posX, int _posY)  { return hHeightField[_posX][_posY]; };
		int getMapWidth() { return hmWidth; };
		int getMapHeight() { return hmHeight; };

	private:
		Uint32 getpixel(SDL_Surface *surface, int x, int y);
		float** hHeightField;
		int hmHeight;
		int hmWidth;
};

#endif