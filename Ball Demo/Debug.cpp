#include "DebugRend.h"

DebugRend::DebugRend(GLint maxVerts) {
	iMaxVerts = maxVerts;

	pNorms = std::vector< glm::vec3 >(iMaxVerts);
	pVerts = std::vector< glm::vec3 >(iMaxVerts);

	/*pNorms = new glm::vec3[this->iMaxVerts];
	pVerts = new glm::vec3[this->iMaxVerts];*/
	Init();
}

DebugRend::~DebugRend() {
	glDeleteBuffers(this->iNoBuffers, vbo);
	glDeleteVertexArrays(1, &vao);
	delete [] vbo;
	pVerts.clear();
	pNorms.clear();
    //delete [] pVerts;
    //delete [] pNorms;
}

bool DebugRend::loadData(glm::vec3 data[3]) {

	return 0;
}

void DebugRend::Init() {
	this->iNoBuffers = 2;
	this->vbo = new GLuint[iNoBuffers];
	glGenBuffers(this->iNoBuffers, vbo);
	glGenVertexArrays(1, &vao);
}

void DebugRend::Update(float dt) {
	//draw is dynamic
	iNoVerts = 0;

}

bool DebugRend::loadVector(glm::vec3 _vector, glm::vec3 _pos, glm::vec3 _col ,bool normalized) {

	//load normals for debug display
	
	if(normalized) {
		_vector *= 30.0f;}

	this->pNorms[iNoVerts] = _col;
	this->pVerts[iNoVerts++] = _pos;
	this->pNorms[iNoVerts] = _col;
	this->pVerts[iNoVerts++] = _pos + _vector;

	return true;
}

void DebugRend::EndMesh() {

	//bind the data points dynamically
	//vert contains the vector data, norms the colour data

	glBindVertexArray(vao);

	//bind position
	glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
	glBufferData(GL_ARRAY_BUFFER, iNoVerts * sizeof(GLfloat) * 3, &pVerts[0] , GL_DYNAMIC_DRAW);
	glVertexAttribPointer(ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(ATTRIBUTE_VERTEX);

	//bind normals buffer
	glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);
	glBufferData(GL_ARRAY_BUFFER, iNoVerts * sizeof(GLfloat) * 3, &pNorms[0] , GL_DYNAMIC_DRAW);
	glVertexAttribPointer(ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(ATTRIBUTE_COLOR);

}

void DebugRend::AssignShader(GLuint shader)
{
	this->shader = shader;
}

void DebugRend::Render( MatrixStack &projectionStack, MatrixStack &modelviewStack, bool WIRE_FRAME ) 
{
	glUseProgram(shader);
	//so draw already
	//update the data to draw
	EndMesh();
	glLineWidth(7);
	//glDisable(GL_DEPTH_TEST);


	int uniformIndex = glGetUniformLocation(shader, "projection");
	glUniformMatrix4fv(uniformIndex, 1,GL_FALSE,glm::value_ptr(projectionStack.getMatrix()));
	uniformIndex = glGetUniformLocation(shader, "modelview");
	glUniformMatrix4fv(uniformIndex, 1,GL_FALSE,glm::value_ptr(modelviewStack.getMatrix()));


	glDrawArrays(GL_LINES, 0, iNoVerts );

	glEnable(GL_DEPTH_TEST);
	glLineWidth(3);
}