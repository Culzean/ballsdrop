#include "Plane.h"

using namespace std;

Plane::Plane( glm::vec3 _pos0, glm::vec3 _pos1, glm::vec3 _pos2 )
{
	//triangle
	this->iNumPos = 3;
	pos = new glm::vec3[iNumPos];
	pos[0] = _pos0;
	pos[1] = _pos1;
	pos[2] = _pos2;

	glm::vec3 tot = glm::vec3(0.0f);
	for(int i=0; i< 3; ++i) { tot+= pos[i]; }

	centre = tot / 3.0f;

	normal = FindNormal();

	width = 0.0;	depth = 0.0f;

	for(int i= 0; i<2; ++i) {
		
		if(abs(pos[i].x - pos[i+1].x) > width) {
			width = abs(pos[i].x - pos[i+1].x);
		}
		if(abs(pos[i].z - pos[i+1].z) > depth) {
			depth = abs(pos[i].z - pos[i+1].z);
		}
	}

	planeDot = glm::dot(-normal, centre);

}


Plane::Plane(glm::vec3 _pos0, glm::vec3 _pos1, glm::vec3 _pos2, glm::vec3 _pos3)
{
	//quad
	this->iNumPos = 4;
	pos = new glm::vec3[iNumPos];
	pos[0] = _pos0;
	pos[1] = _pos1;
	pos[2] = _pos2;
	pos[3] = _pos3;

	centre = ( pos[3] - pos[0] );
	centre /= 2;
	centre += pos[0];

	pos[3] = centre - pos[0];
	pos[3] *= 2;
	pos[3] += pos[0];

	normal = FindNormal();

	width = abs(pos[0][0] - pos[1][0]);
	depth = abs(pos[0][2] - pos[2][2]);

	planeDot = glm::dot(-normal, centre);

}

Plane::~Plane(){
	//will this call the destructor being pointed at?
	delete[] pos;
}

glm::vec3 Plane::FindNormal()
{
	glm::vec3 vec1 = pos[2] - pos[1];
	glm::vec3 vec2 = pos[0] - pos[1];

	glm::vec3 vec3 = pos[2] - pos[2];
	glm::vec3 vec4 = pos[0] - pos[2];

	glm::vec3 test1 = ( glm::cross( vec1, vec2 ) );
	glm::vec3 test2 = ( glm::cross( vec4, vec3 ) );

	return glm::normalize( glm::cross( vec1, vec2 ) );
}

void Plane::PrintOut()
{
	cout << "Normal x: " << normal[0] << " y: " << normal[1] << " z: " << normal[2]  << endl;
}

GLfloat Plane::GetAngleDegrees(void)
{
	float adj = pos[0].z + pos[3].z;
	float opp = pos[0].y + pos[3].y;

	return tan(opp / adj);
}

GLfloat	Plane::findPoint( GLfloat x, GLfloat z ) {
	//give various configartions of inputs
	//find the missing value such that the point is on the plane

	return ( -1 * ( normal.x * x + normal.z * z + planeDot ) / normal.y );
}