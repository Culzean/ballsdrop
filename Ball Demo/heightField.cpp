#include "heightField.h"


HeightField::HeightField(void)
{

}

HeightField::~HeightField(void)
{

}

Uint32 HeightField::getpixel(SDL_Surface *surface, int x, int y)
{
    int bpp = surface->format->BytesPerPixel;
    /* Here p is the address to the pixel we want to retrieve */
    Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;

    switch(bpp) {
    case 1:
        return *p;
        break;

    case 2:
        return *(Uint16 *)p;
        break;

    case 3:
        if(SDL_BYTEORDER == SDL_BIG_ENDIAN)
            return p[0] << 16 | p[1] << 8 | p[2];
        else
            return p[0] | p[1] << 8 | p[2] << 16;
        break;

    case 4:
        return *(Uint32 *)p;
        break;

    default:
        return 0;       /* shouldn't happen, but avoids warnings */
    }
}

bool HeightField::Create(char *filename, int width, int height)
{
	hmHeight = height;
	hmWidth = width;

	hHeightField = new float*[width];
	for(int i = 0; i < width; i++)
		hHeightField[i] = new float[height];
		
	//load file - using sdl library
	SDL_Surface *tmpSurface;
	tmpSurface = SDL_LoadBMP(filename);

	if(!tmpSurface)
	{
		std::cout << "Error loading bitmap" << std::endl;
	}

	Uint32 temp = 0;
	Uint8 red = 0;
	Uint8 green = 0;
	Uint8 blue = 0;

	for(int x = 0; x < width; x++)
	{
		for(int z = 0; z < height; z++)
		{
			temp = getpixel(tmpSurface, x, z);
			SDL_GetRGB(temp, tmpSurface->format, &red, &green, &blue);
			hHeightField[x][z] = (float)green;
			//std::cout << hHeightField[x][z] << "\n";
		}
	}

	return true;
}

void HeightField::Render(void)
{

}