#ifndef MULTI_H
#define MULTI_H

#include <iostream>
#include <cstdlib>

#include "stafx.h"
#include "State.h"
#include "HUDSystem.h"
#include "Collectable.h"

class Multi : public State
{
	public:
		Multi(int _WIDTH, int _HEIGHT);
		~Multi(void);

		void Init(WindowManager windowManager, ShaderManager shaderManager, int musicSelection);
		void Render(bool WIRE_FRAME);
		void KeyHandler( SDL_Event sdlEvent);
		void GetInput(CXBOXController *Player1);
		void Update(GLfloat dt);
		void UpdateCamera(int index, GLfloat dt, Ball* _ball, Camera* _camera);
		glm::quat RotateTowards(glm::quat q1, glm::quat q2, float maxAngle);
		void Cleanup(void);
		int GetSelection(void) {return selection;}
		void SetSelection(int value) {selection = value;}
		int GetMusic(void) {return 0;}
		float GetTime(void) { return timer;}
		void AddTime(float time) {;}

		friend class Ball;

	private:
		float HEIGHT;
		float WIDTH;
		void InitSoundEffects(int musicSelection, SoundBank sounds);
		bool FREE_CAMERA;
		bool UPDATING;
		bool BALL_MOVING;


		TextureManager textureManager;


		glm::mat4x4 cameraMatrix;
		glm::mat4x4 cameraMatrix2;
		glm::vec3 currentLookVec, currentLookVec2;
		
		MatrixStack projectionStack;
		MatrixStack modelviewStack;

		Ball					*ball;
		Ball					*ball2;
		TrackFactory			*trackFactory;
		RaceTrack				*pCrtTrack;
		Camera					*camera;
		Camera					*camera2;
		PhysicsModel			*physicsM;
		SkyBox					*skyBox;
		Terrain					*terrain;
		GLuint					terrainShader;
		ParticleStandard		*particle;
		GrindParticles			*grindEffect;
		DebugRend				*debugRend;
		HUDSystem				hud;
		XINPUT_STATE			prevState;
		SoundBank				*soundBank;

		std::vector<Collectable*> arrows;
		int NUM_COLLECTABLES;

		float rotXAngle;
		float rotYAngle;
		float prevXCameraAngle;

		int count;
		bool prevUpdating;
		float timer;
		int selection;
};

#endif