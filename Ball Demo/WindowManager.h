#ifdef _MSC_VER
	#pragma once
#endif

#ifndef WINDOWMANAGER_H
#define WINDOWMANAGER_H

#include <sdl.h>
#include <GL/glew.h>

#include "Error.h"	//Includes Error.h for initalization error checking.

class WindowManager{

public:
	SDL_Window* SetupRC(SDL_GLContext &context, int width, int height, char *windowName);	//takes in a context and a window width and height.
	void ToggleFullscreen();
	SDL_Window*	  GetWindow(void)	{ return window; }
	int GetWidthOfWindow() { return WIDTH; }
	int GetHeightOfWindow() { return HEIGHT; }

private:
	SDL_Window *window;
	bool FULLSCREEN;
	int WIDTH, HEIGHT;
};

#endif