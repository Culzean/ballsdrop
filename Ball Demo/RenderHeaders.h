#pragma once

#include <SDL.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <GL\glew.h>

#include <glm/glm.hpp>
#include <glm/gtc\matrix_transform.hpp>
#include <glm/gtc\type_ptr.hpp>

#include "Renderable.h"
#include "Mesh.h"
#include "MatrixStack.h"
#include "Frame.h"