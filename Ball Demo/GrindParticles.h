#ifndef _GRINDPARTICLES_H_
#define _GRINDPARTICLES_H_

#include "stafx.h"

const float STEP_TIME = 0.1f;

class GrindParticles
{


	struct Particle
	{
		glm::vec3 position;
		glm::vec3 velocity;
		float timeAlive; //how long the particle has been alive for
		float lifeSpan;	 //how long the particle stays alive for
	};

	public:
		GrindParticles(void);
		~GrindParticles(void);
		void Init(Ball* ball, int noParticles);
		void AssignShader(GLuint shader);
		void ApplyTexture(GLuint texID);
		void Update(Ball* ball, float dt);
		void Render(MatrixStack &projectionStack, MatrixStack &modelviewStack);

	private:
		Particle* particles;
		glm::vec3* vertices;
		glm::vec3* colours;

		GLuint vao;
		GLuint vbo[2];
		GLuint texID;
		GLuint shaderID;

		int noParticles;
		glm::vec3 ballPosition;
		glm::vec3 ballVector;
		void createParticle(Particle* p);
		void initBuffers(void);

};

#endif _GRINDPARTICLES_H_