#include "Mesh.h"


Mesh::Mesh() {
	nMaxVerts = 0;
	frame.SetOrigin(glm::vec3(0.0f,0.0f,0.0f));
}

Mesh::~Mesh() {
	delete[] vbo;
}

void Mesh::Init() {
}

void Mesh::createVertices(GLfloat iWidth, GLfloat iDepth, GLfloat iMaxWidth, GLfloat iMaxDepth) {

	GLint iNoPointsWidth = (GLint)glm::ceil(iWidth / iMaxWidth) + 1;
	GLint iNoPointDepth = (GLint)glm::ceil(iDepth / iMaxDepth) + 1;

	BeginMesh(iNoPointsWidth* iNoPointDepth * 6);

	glm::vec3 vVerts[4];
	glm::vec3 vNorms[4];
	glm::vec2 vTexCoords[4];

	for(int iX =0; iX< iNoPointsWidth; ++iX)
	{
		GLfloat posX = iX * iMaxWidth;
		for(int iZ =0; iZ< iNoPointDepth; ++iZ)
		{
			GLfloat posZ = iZ * iMaxDepth;

			vVerts[0][0] = posX + iMaxWidth;
			vVerts[0][1] = 0.0f;
			vVerts[0][2] = posZ + iMaxDepth;

			vVerts[1][0] = posX;
			vVerts[1][1] = 0.0f;
			vVerts[1][2] = posZ + iMaxDepth;

			vVerts[2][0] = posX;
			vVerts[2][1] = 0.0f;
			vVerts[2][2] = posZ;

			vVerts[3][0] = posX + iMaxWidth;
			vVerts[3][1] = 0.0f;
			vVerts[3][2] = posZ;

			/*vTexCoords[0][0] = 1;
			vTexCoords[0][1] = 1;

			vTexCoords[1][0] = 0;
			vTexCoords[1][1] = 1;

			vTexCoords[2][0] = 0;
			vTexCoords[2][1] = 0;

			vTexCoords[3][0] = 1;
			vTexCoords[3][1] = 0;*/

			vNorms[0][0] = 0;		vNorms[0][1] = 1;		vNorms[0][2] = 0;
			vNorms[1][0] = 0;		vNorms[1][1] = 1;		vNorms[1][2] = 0;
			vNorms[2][0] = 0;		vNorms[2][1] = 1;		vNorms[2][2] = 0;
			vNorms[3][0] = 0;		vNorms[3][1] = 1;		vNorms[3][2] = 0;

			AddTriangle(vVerts,vNorms,vTexCoords);

			//swap around verts for next triangle
			vVerts[1] = vVerts[2];
			vNorms[1] = vNorms[2];
			vTexCoords[1] = vTexCoords[2];

			vVerts[2] = vVerts[3];
			vNorms[2] = vNorms[3];
			vTexCoords[2] = vTexCoords[3];

			AddTriangle(vVerts,vNorms,vTexCoords);
		}
	}
	EndMesh();
}


void Mesh::BeginMesh(GLuint _nMaxVerts) {

	//ask for 6 times more vertices than you will use.
	//This will be cleared up once the check for duplicates is complete
	//possibly it's the index list that is running so long?

	nMaxVerts = _nMaxVerts * 5;
	pVerts = std::vector< glm::vec3 >(nMaxVerts);
	pNorms = std::vector< glm::vec3 >(nMaxVerts);
	pTexCoords = std::vector< glm::vec2 >(nMaxVerts);
	pIndexes = std::vector< GLuint >(nMaxVerts);
	//pVerts = new glm::vec3[nMaxVerts];
	//pNorms = new glm::vec3[nMaxVerts];
	//pIndexes = new GLuint[nMaxVerts];
	//pTexCoords = new glm::vec2[nMaxVerts];
	iNoVerts = 0;
	iNoFaces = 0;
	iNoBuffers = 5;

	vbo = new GLuint[iNoBuffers];
}

void Mesh::AddQuad(glm::vec3 _verts[4], glm::vec2 _texCoords[4]) {

	if(nMaxVerts == 0)
	{
		std::cout << "Please begin mesh and state a max no of vertices in mesh" << std::endl;
	}
	//find normals
	//note this does not find the average normal for a vertex if it shares many triangles
	//adding this would give much smoother lighting
	glm::vec3 vNorms[4];


	vNorms[0] = glm::normalize (findNorm(_verts[2], _verts[0], _verts[1]) ); 
	vNorms[1] = glm::normalize (findNorm(_verts[0], _verts[1], _verts[2]) );
	vNorms[2] = glm::normalize (findNorm(_verts[1], _verts[2], _verts[0]) );
	vNorms[3] = glm::normalize (findNorm(_verts[1], _verts[3], _verts[2]) );

	AddTriangle(_verts,vNorms,_texCoords);

	//swap around verts for next triangle
	_verts[0] = _verts[1];
	vNorms[0] = vNorms[1];
	//_texCoords[0] = _texCoords[1];

	_verts[1] = _verts[3];
	vNorms[1] = vNorms[3];
	//_texCoords[1] = _texCoords[3];
	
	/*_verts[1] = _verts[2];
	vNorms[1] = vNorms[2];
	_texCoords[1] = _texCoords[2];

	_verts[2] = _verts[3];
	vNorms[2] = vNorms[3];
	_texCoords[2] = _texCoords[3];*/

	AddTriangle(_verts,vNorms,_texCoords);
}

void Mesh::AddIndexedTriangle(std::vector< glm::vec3 > verts, glm::vec3 norms[3], glm::vec2 texCoords[3])
{
	const float close = 0.0000001f;

	for(int i = 0; i < 3; i++)
	{
		pVerts[this->iNoVerts] = verts[i];
		pNorms[this->iNoVerts] = norms[i];
		pTexCoords[this->iNoVerts] = texCoords[i];
		this->pIndexes[iNoVerts] = iNoVerts+1;

		++iNoVerts;
	}

	++iNoFaces;
}

void Mesh::AddTriangle(glm::vec3 verts[3], glm::vec3 norms[3], glm::vec2 texCoords[3]) {

	const float close = 0.0000001f;

	for(GLuint i =0; i< 4; ++i)
	{
		glm::normalize(norms[i]);
	}	
	GLuint j = 0;
	for(GLuint i =0; i< 4; ++i)
	{
		
		for(j =0; j < iNoVerts; ++j)
		{
			if( comparePoints( verts[i], pVerts[j], close) &&
				comparePoints( norms[i], pNorms[j], close) &&
				comparePoints( texCoords[i], pTexCoords[j], close))
			{
				//this point is part of the mesh already, include only within the index list
				this->pIndexes[iNoFaces] = j;
				this->iNoFaces++;
				break;
			}
		}
		if(j == iNoVerts)
		{
				//this is a new point to be added to index list
				pVerts[this->iNoVerts] = verts[i];
				pNorms[this->iNoVerts] = norms[i];
				pTexCoords[this->iNoVerts] = texCoords[i];
				this->pIndexes[iNoFaces] = iNoVerts;
				++iNoVerts;
				++iNoFaces;
		}
	}


}

void Mesh::EndMesh() {

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(this->iNoBuffers, vbo);

	//bind first vertex buffer
	glBindBuffer(GL_ARRAY_BUFFER, vbo[ATTRIBUTE_VERTEX]);
	glBufferData(GL_ARRAY_BUFFER, iNoVerts * sizeof(glm::vec3), &pVerts[0] , GL_STATIC_DRAW);
	glVertexAttribPointer(ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(ATTRIBUTE_VERTEX);

	//bind normals buffer
	glBindBuffer(GL_ARRAY_BUFFER, vbo[ATTRIBUTE_NORMAL]);
	glBufferData(GL_ARRAY_BUFFER, iNoVerts * sizeof(glm::vec3), &pNorms[0] , GL_STATIC_DRAW);
	glVertexAttribPointer(ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(ATTRIBUTE_NORMAL);

	//bind texturedata
	glBindBuffer(GL_ARRAY_BUFFER, vbo[ATTRIBUTE_TEXTURE0]);
	glBufferData(GL_ARRAY_BUFFER, iNoVerts * sizeof(glm::vec2), &pTexCoords[0], GL_STATIC_DRAW);
	glVertexAttribPointer(ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(ATTRIBUTE_TEXTURE0);   

	//bind color buffer
	glBindBuffer(GL_ARRAY_BUFFER, vbo[ATTRIBUTE_COLOR]);
	glBufferData(GL_ARRAY_BUFFER, iNoVerts * sizeof(glm::vec3), &pNorms[0] , GL_STATIC_DRAW);
	glVertexAttribPointer(ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(ATTRIBUTE_COLOR);

	//indices to be bound next!
	//note the GL_ELEMENT_ARRAY_BUFFER is not GL_ARRAY_BUFFER
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo[ATTRIBUTE_INDICES]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, iNoVerts * sizeof(GLint), &pIndexes[0],GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	//clear up
	pVerts.clear();
	pNorms.clear();
	pTexCoords.clear();
	pIndexes.clear();
	/*delete [] pIndexes;
	delete [] pVerts;
	delete [] pNorms;
	delete [] pTexCoords;*/

	// Reasign pointers so they are marked as unused
	/*pIndexes = NULL;
	pNorms = NULL;
	pTexCoords = NULL;*/
}

//
// helper function to compare to points and return true if they are within margin to be considered the same
// much like m3d Close enough
// return true is same point
//

bool Mesh::comparePoints( glm::vec3 &p0, glm::vec3 &p1, GLfloat close ) {
	if(abs ( p0.x - p1.x) < close &&
		abs ( p0.y - p1.y) < close &&
		abs ( p0.z - p1.z) < close)
	{	return true;	}
	else
	{return false;}
}

bool Mesh::comparePoints( glm::vec2 &p0, glm::vec2 &p1, GLfloat close ) {
	if(abs ( p0.x - p1.x) < close &&
		abs ( p0.y - p1.y) < close)
	{	return true;	}
	else
	{return false;}
}

void Mesh::Render(MatrixStack &projectionStack, MatrixStack &modelviewStack, bool WIRE_FRAME) {
	glBindVertexArray(vao);
	glUseProgram(this->shaderID);

	int uniformIndex = glGetUniformLocation(shaderID, "projection");
	glUniformMatrix4fv(uniformIndex, 1,GL_FALSE,glm::value_ptr(projectionStack.getMatrix()));
	uniformIndex = glGetUniformLocation(shaderID, "modelview");
	glUniformMatrix4fv(uniformIndex, 1,GL_FALSE,glm::value_ptr(modelviewStack.getMatrix()));

	uniformIndex = glGetUniformLocation(shaderID, "cameraPosition");
	glUniform3fv(uniformIndex, 1, glm::value_ptr(glm::vec3(0.0f, 0.0f, 0.0f)));
	//move this to the object class
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//glDrawElements(GL_LINES, this->iNoFaces, GL_UNSIGNED_INT, 0);
	glDrawArrays(GL_TRIANGLES, 0, iNoVerts);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glUseProgram(0);
	glBindVertexArray(0);
}

void Mesh::Update() {

}