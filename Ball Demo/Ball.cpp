#include "Ball.h"
#include <iostream>
#include "Plane.h"

Ball::Ball(char *fileName, glm::vec3 position)
{
	model = new OBJ_Model();
	model->Load(fileName);

	vertices = model->GetVertices();
	normals = model->GetNormals();
	texCoords = model->GetTexCoords();

	delete model;

	ballFrame = new Frame();
	ballFrame->SetOrigin(position);
	ballFrame->RotateLocalY(180);
	fVelocity = 0.0f;
	texture[0] = NULL;
	texture[1] = NULL;
	shader = NULL;
	onGround = false;
	movingPosX = false;
	movingNegX = false;
	movingPosZ = false;
	movingNegZ = false;
	fVelocity = 0.0f;
	
	renderScale =glm::vec3(2.0f);
	radius = 0.0f;
	for(GLuint i = 0; i < vertices.size(); i++)
	{ 
		if(vertices[i].x > radius)
		{
			radius = vertices[i].x;
		}
	}
	radius *=renderScale.x;
	mass = 1.0f;
	
	xRot = 0;
	yRot = 0;
	zRot = 0;
	inputScale = glm::vec2(0.0005f, 0.0005f);

	this->setOutOfBounds(false);

	velocityHist = std::vector< glm::vec3 >(numVelAvg);
	crtVelAvg = 0;
	//start facing the z direction
	for(int i=0; i<numVelAvg; ++i)
	{
		velocityHist[i] = glm::vec3(0.0f, -0.5f, 0.5f);
	}

	light.ambient = glm::vec4( 0.6, 0.6, 0.6, 1.0);				//ambient
	light.diffuse = glm::vec4( 0.4, 0.4, 0.4, 1.0);				//diffuse
	light.specular = glm::vec4( 0.1, 0.1, 0.1, 1.0),			//specular
	light.position = glm::vec4(0.0f, 0.0f, 0.0f, 1.0);			//position

	material.ambient = glm::vec4( 0.1, 0.1, 0.1, 1.0 ); //ambient
	material.diffuse = glm::vec4( 0.4, 0.4, 0.4, 1.0 ); //diffuse
	material.specular = glm::vec4( 0.2, 0.2, 0.2, 1.0 ); //specular
	material.emmisive = glm::vec4( 0.0, 0.0, 0.0, 1.0);	//emmisive
	material.shininess = 5.0f;
	material.ReflectionFactor = 0.6f;

	glGenVertexArrays(1, &vao); // Allocate & assign Vertex Array Object (VAO)
	glBindVertexArray(vao); // Bind VAO as current object

	glGenBuffers(3, vbo);

	glBindBuffer(GL_ARRAY_BUFFER, vbo[0]); //Bind the vertex buffer
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * vertices.size(), &vertices[0], GL_STATIC_DRAW);
	glVertexAttribPointer(ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(ATTRIBUTE_VERTEX);

	glBindBuffer(GL_ARRAY_BUFFER, vbo[1]); //Bind the normal buffer
	glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(glm::vec3), &normals[0] , GL_STATIC_DRAW);
	glVertexAttribPointer(ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(ATTRIBUTE_NORMAL);

	//glBindBuffer(GL_ARRAY_BUFFER, vbo[2]); //Bind the TexCoords buffer
	//glBufferData(GL_ARRAY_BUFFER, texCoords.size() * sizeof(glm::vec2), &texCoords[0] , GL_STATIC_DRAW);
	//glVertexAttribPointer(ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, 0);
	//glEnableVertexAttribArray(ATTRIBUTE_TEXTURE0);
}

Ball::~Ball(void)
{
}

void Ball::ApplyTexture(GLuint texture, GLuint texture2)
{
	this->texture[0] = texture;
	this->texture[1] = texture2;
}

void Ball::AssignShader(GLuint shader)
{
	this->shader = shader;
}

void Ball::Update(void)
{

	//std::cout << ballFrame->zRot << std::endl;
}

float Ball::GetVelocityValue(void)
{
	return this->fVelocity;
}

void Ball::UpdatePhysics(GLfloat dt) {

	if(glm::abs(vBallUpdate.y) > 0.002) {
		SetXRot(-vBallUpdate.z * 2);}
	if(glm::abs(vBallUpdate.x) > 0.002) {
		SetXRot(-vBallUpdate.x * 2); }

	//find an average value of the history over the course of numVelAvg frames
	glm::vec3 tempVel(0.0f);
	if(++crtVelAvg % numVelAvg == 0)
	{
		crtVelAvg = 0;
	}
	velocityHist[crtVelAvg] = vVelocity;
	
	for(int i= 0;i< numVelAvg; ++i)
	{
		tempVel += velocityHist[i];
	}
	vVelocityAvg = tempVel / (GLfloat)numVelAvg;

	SetPosition( ballFrame->GetOrigin() + vBallUpdate );

	GLfloat totVel = vVelocity.x * vVelocity.x + vVelocity.y * vVelocity.y + vVelocity.z * vVelocity.z;
	fVelocity = glm::sqrt(totVel);

	if(this->outOfBounds) {
		outOfBoundsTimer += dt;
	} else {
		outOfBoundsTimer = 0.0f;
	}

	vBallUpdate.x = 0.0f;	vBallUpdate.y = 0.0f;	vBallUpdate.z = 0.0f;
}

void Ball::Render(MatrixStack &projectionStack, MatrixStack &modelviewStack, bool WIRE_FRAME)
{	
	glCullFace(GL_FRONT);
	glBindVertexArray(vao);
	glUseProgram(shader);
	
	glm::mat4 matrix(1.0);

	modelviewStack.push(matrix);
	matrix = ballFrame->GetMatrix();
	matrix = ballFrame->RotateLocalX(xRot);
	matrix = ballFrame->RotateLocalY(yRot);
	matrix = ballFrame->RotateLocalZ(zRot);
	matrix = glm::scale(matrix, renderScale.x,renderScale.y,renderScale.z);
	modelviewStack.multiplyMatrix(matrix);

			int uniformIndex = glGetUniformLocation(shader, "projection");
			glUniformMatrix4fv(uniformIndex, 1,GL_FALSE,glm::value_ptr(projectionStack.getMatrix()));
			uniformIndex = glGetUniformLocation(shader, "modelview");
			glUniformMatrix4fv(uniformIndex, 1,GL_FALSE,glm::value_ptr(modelviewStack.getMatrix()));


			uniformIndex = glGetUniformLocation(shader, "cameraPosition");
			glUniform3fv(uniformIndex, 1, glm::value_ptr(glm::vec3(0.0f, 10.0f, -10.0f)));


			uniformIndex = glGetUniformLocation(shader, "texture0");
			glUniform1i(uniformIndex, 0);

			uniformIndex = glGetUniformLocation(shader, "texMap");
			glUniform1i(uniformIndex, 1);
			
				glEnableVertexAttribArray(ATTRIBUTE_VERTEX);
				glEnableVertexAttribArray(ATTRIBUTE_NORMAL);
					
					if (WIRE_FRAME)
					{
						//glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
						glDrawArrays(GL_LINES, 0, vertices.size());
					}
					else
					{
						glActiveTexture(GL_TEXTURE0);
						glBindTexture(GL_TEXTURE_1D, texture[0]);
						glActiveTexture(GL_TEXTURE1);
						glBindTexture(GL_TEXTURE_2D, texture[1]);
						glDrawArrays(GL_TRIANGLES, 0, vertices.size()*2);

						glBindTexture(GL_TEXTURE_2D, 0);
					}

					glDisableVertexAttribArray(ATTRIBUTE_VERTEX);
					glDisableVertexAttribArray(ATTRIBUTE_NORMAL);

	modelviewStack.pop();

	glUseProgram(0);
	glBindVertexArray(0);
	glDisable(GL_CULL_FACE);
}

void Ball::UseXBoxInput(CXBOXController *Input1) {
	boost = false;
	movingPosX = false;
	movingNegX = false;
	movingPosZ = false;
	movingNegZ = false;

	if(Input1->GetState().Gamepad.bRightTrigger != 0)
	{
		boost = true;
	}

	if (Input1->GetLeftMagnitude() != 0)
	{
		////range is -36000(i think) to 36000
		if(Input1->GetLeftThumbX() < -15000)
			movingNegX = true;

		if(Input1->GetLeftThumbX() > 15000)
			movingPosX = true;

		//std::cout << "X: " << Input1->GetLeftThumbX() << "Y: " << Input1->GetLeftThumbY() << "\n";
		if(Input1->GetLeftThumbY() > 15000)
			movingPosZ = true;

		if(Input1->GetLeftThumbY() < -15000)
			movingNegZ = true;
	}
}

void Ball::ResetPosition( Plane* _plane )
{
	glm::vec3 pos = _plane->getPos() + glm::vec3( 0.0f, 20.0f, 0.0f );
	SetPosition(pos);
	fVelocity = 0.0f;
	crtVelAvg = 0;
	setOutOfBounds(false);
	//start facing the z direction
	for(int i=0; i<numVelAvg; ++i)
	{
		velocityHist[i] = glm::vec3(0.0f, -0.5f, 0.5f);
	}
}

void Ball::Cleanup(void)
{
}