#ifdef _MSC_VER
	#pragma once
#endif

#ifndef SKYBOX_H
#define SKYBOX_H

#include "stafx.h"

class SkyBox
{
	public:
		SkyBox();
		~SkyBox();

		void Init();
		void ApplyTexture(GLuint texture);
		void AssignShader(GLuint shader);
		void Render(MatrixStack &projectionStack, MatrixStack &modelviewStack, glm::vec3 position, bool wireFrame);

	private:
		GLuint vao;
		GLuint vbo[3];
		GLuint texID;
		GLuint texture;
		GLuint shader;
		OBJ_Model *model;
		std::vector<glm::vec3> vertices;
		std::vector<glm::vec3> normals;
		std::vector<glm::vec2> texCoords;
};

#endif