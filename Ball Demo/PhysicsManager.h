#ifndef AGP_PHYSICS

#define AGP_PHYSICS

#include "stafx.h"

class Plane;
class Ball;
class TrackSect;

class PhysicsManager
{
public:
	PhysicsManager();
	~PhysicsManager();

	/*
	parameters - uses the physics objects Ball and plane

	method sets the ball ontop of the plane 

	returns - the value of distance between them

	*/

	GLfloat BallVsPlane( Ball* b0, Plane* p0 );

	GLfloat distanceToPlane( glm::vec3 point, Plane* const plane );
	glm::vec3 projOnPlane( glm::vec3& vec, Plane* const plane );
	bool findTrackSect( TrackSect* _sect, glm::vec3 obPos, GLfloat obRad );

	bool pnpoly(int numPoints, glm::vec3* poly, float x, float z);

	bool BallVsBall( Ball* b0, Ball* b1 );
	bool ResolveBallCol( Ball* b0, Ball* b1 );

private:


};


#endif AGP_PHYSICS