#include "SoundEffect.h"
#include "Music.h"

#ifdef _MSC_VER
	#pragma once
#endif

#ifndef SOUNDBANK_H
#define SOUNDBANK_H

class SoundBank
{
public: 
	SoundBank();
	~SoundBank();

	void InitSounds(void);
	void StopAllSounds(void);

	static SoundBank* GetInstance(){
	if(!soundBank)
		soundBank = new SoundBank();
	return soundBank;
	
	}
	SoundEffect navigate;
	SoundEffect confirm;
	SoundEffect rolling;
	Music rock, electro, weird;

private:
	static SoundBank*	soundBank;
};


#endif