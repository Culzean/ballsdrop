#include <SFML\Audio.hpp>
#include <iostream>

#ifdef _MSC_VER
	#pragma once
#endif

#ifndef MUSIC_H
#define MUSIC_H

class Music{

public:
	void loadSound(char *filename);
	void play();
	void stop();
	sf::Music::Status GetStatus(void) { return music.getStatus(); }
	void SetLoop(bool loop);

private:
	sf::Music music;
};

#endif