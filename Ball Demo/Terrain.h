#ifndef _TERRAIN_H_
#define _TERRAIN_H_

#include <GL\glew.h>
#include <iostream>
#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtc\type_ptr.hpp>
#include "heightField.h"
#include "MatrixStack.h"
#include "TextureManager.h"

class Terrain
{

	struct lightStruct
	{
		glm::vec4 ambient;
		glm::vec4 diffuse;
		glm::vec4 specular;
	};

	struct materialStruct
	{
		glm::vec4 ambient;
		glm::vec4 diffuse;
		glm::vec4 specular;
		float shininess;
	}; 
		
	public:
		Terrain(void);
		~Terrain(void);
		void Generate(int _numCellsX, int _numCellsZ, int _sizeX, int _sizeZ);
		void Render(MatrixStack &projectionStack, MatrixStack &modelviewStack);
		void ApplyTexture(GLuint texture);
		void ApplyShader(GLuint shader);
		void Update(glm::vec3 position);

	private:
		void initBuffers(void);
		void genVerts(int _sizeX, int _sizeZ);
		void createIndexList(void);
		void calcNormals(void);
		void calcTexCoords(void);
		void indexVerts(void);
		glm::vec3 *vertices;
		glm::vec3 *tempVerts;
		glm::vec3 *tempNormals;
		glm::vec3 *normals;
		glm::vec3 *tempTexCoords;
		glm::vec2 *texCoords;
		int *indices;

		lightStruct light0;
		glm::vec4 lightPosition;

		GLuint vao;
		GLuint vbo[3];
		GLuint texture;
		GLuint shader;
		
		int numVerts;
		int numVertsX;
		int numVertsZ;
		int indexSize;
		int numCellsWide;
		int numCellsHigh;

		int terrainX;
		int terrainY;
		int terrainZ;

		HeightField* height;

		materialStruct material0;
};

#endif _TERRAIN_H_