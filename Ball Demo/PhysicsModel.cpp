#include "PhysicsModel.h"

//gravity magnitude
const GLfloat GRAVITY = 3.98f;
const GLfloat FRICTION = 0.89f;


PhysicsModel::PhysicsModel(RaceTrack *raceTrack)
{
	this->track = raceTrack;
	this->physics = new PhysicsManager();
	currentPlane = track->getStart()->getStart();
}
PhysicsModel::~PhysicsModel()
{
	collisionLookup.clear();
	delete physics;
}

void PhysicsModel::AddBall(Ball *ball)
{
	balls.push_back(ball);
	collisionLookup.push_back(track->getStart()->getStart());
}

void PhysicsModel::AddWall(void)
{
}

void PhysicsModel::Update(glm::vec3 cameraVec, GLfloat dt)
{
	//find dist to plane, and adjust
	
	static GLfloat playerVel = 5.5f;

	for (GLuint i = 0; i < balls.size(); ++i)
	{
		currentSect = track->findSection(balls[i]->ballFrame->GetOrigin(), balls[i]->getRadius());
		currentPlane = currentSect->getPlane(balls[i]->ballFrame->GetOrigin());

		if(currentPlane == NULL) {
			//outside the track!
			balls[i]->setOutOfBounds(true);
			currentPlane = currentSect->getPlane(currentSect->GetNumb() - 1);
		} else {
			balls[i]->setOutOfBounds(false);
			//set for lookup of collision test
			collisionLookup[i] = currentPlane;
		}

		if( currentPlane->isOutOfBounds() ) {
			//what value to hit death at?
			balls[i]->setOutOfBounds(true);
			if(balls[i]->getOutOfBoundsTime() > MAX_OUTOFBOUNDS_TIME ) {
				std::cout << " We are falling  " << std::endl;
				balls[i]->setOutOfBounds(false);
				balls[i]->vGravity = GRAVITY * glm::vec3(0.0f, -1.0f, 0.0f);
				currentSect = track->getSection();
				balls[i]->ResetPosition(currentSect->getStart());
			}
		}
		glm::vec3 right;

		if(!balls[i]->isOnGround()) {
			//how to handle gravity?
			balls[i]->vGravity = balls[i]->vGravity + GRAVITY * glm::vec3(0.0f, -1.0f, 0.0f);
			right = cross(glm::vec3(0.0f, 0.0f, 1.0f), glm::vec3(0.0f, 1.0f, 0.0f)); // this value is totally arbitary!
			right = glm::normalize(right);
		} else {
			balls[i]->vGravity += -physics->projOnPlane( glm::vec3(0.0f, 1.0f, 0.0f), currentPlane ) * GRAVITY;

			right = cross(balls[i]->vVelocity, glm::vec3(0.0f, 1.0f, 0.0f));
			right = glm::normalize(right);
		}

		balls[i]->vVelocity *= FRICTION;
		balls[i]->vGravity *= FRICTION;

		if(balls[i]->movingPosX)
			balls[i]->vVelocity += right * playerVel;


		if(balls[i]->movingNegX)
			balls[i]->vVelocity += right * -playerVel;

		if(balls[i]->boost)
			balls[i]->vVelocity *= 1.05f;


		
		balls[i]->vVelocity += balls[i]->vGravity;
		balls[i]->addUpdateVec(balls[i]->vVelocity, dt);
		balls[i]->UpdatePhysics(dt);

		//collision test with other balls
		for(unsigned int j = i+1; j < balls.size(); ++j)
		{
			if(physics->BallVsBall(balls[i], balls[j]))
			{
				ResolveBallCollision( balls[i], balls[j] );
			}
		}
		physics->BallVsPlane( balls[i] , currentPlane );


		//if(debug) {
		//	debug->Update(0);
		//	debug->loadVector( balls[i]->vVelocity, balls[i]->GetPosition(), glm::vec3(1.0f, 0.0f, 0.0f), false);
		//	debug->loadVector( balls[i]->vGravity, balls[i]->GetPosition(), glm::vec3(1.0f, 0.0f, 0.0f), false);
		//	debug->loadVector( balls[i]->vVelocityAvg, balls[i]->GetPosition(), glm::vec3(0.0f, 0.0f, 1.0f), false);
		//	debug->loadVector( (balls[i]->GetPosition() - balls[i]->getRadius()) - balls[i]->GetPosition(), balls[i]->GetPosition(), glm::vec3(0.0f, 1.0f, 0.0f), false);
		//	debug->loadVector((currentPlane->getPos() + (currentPlane->getNormal() * 7.0f)) - currentPlane->getPos() , currentPlane->getPos(), glm::vec3(0.0f, 1.0f, 0.0f), false);
		//}
	}
}

void PhysicsModel::ResolveBallCollision( Ball* b0, Ball* b1 )
{
	//seperate by half dist to other
	// find momentum before
	// find momentum after
	// find reflected direction
	// set veolcity to new veolcity

	glm::vec3 colVec = b0->GetPosition() - b1->GetPosition();
	glm::vec3 nColVec = glm::normalize(colVec);

	// Find the length of the component of each of the movement
	// vectors along n. 
	// a1 = v1 . n
	// a2 = v2 . n
	GLfloat a1 = glm::dot( b0->getVelocity(), nColVec );
	GLfloat a2 = glm::dot( b1->getVelocity(), nColVec );

	// Using the optimized version, 
	// optimizedP =  2(a1 - a2)
	//              -----------
	//                m1 + m2
	float optimizedP = (2.0f * (a1 - a2)) / (b0->getMass() + b1->getMass());

	// Calculate v0', the new movement vector of b0
	// v0' = v0 - optimizedP * m2 * n
	glm::vec3 v0 = b0->getVelocity() - optimizedP * b0->getMass() * nColVec;

	// Calculate v1', the new movement vector of circle1
	// v2' = v2 + optimizedP * m1 * n
	glm::vec3 v1 = b1->getVelocity() + optimizedP * b1->getMass()* nColVec;

	//move balls out of collision
	b0->SetPosition( b0->GetPosition() + colVec * 0.5f);
	b1->SetPosition( b1->GetPosition() - colVec * 0.5f);

	b0->setVelocity(v0);
	b1->setVelocity(v1);
}

void PhysicsModel::HandleSideCollision(Ball *ball,  Plane *currentPlane)
{
	if ((ball->GetPosition().x + ball->getRadius()) > (currentPlane->getWidth() / 2))
	{
		ball->SetPosition(glm::vec3((currentPlane->getWidth() / 2) - ball->getRadius(), ball->GetPosition().y, ball->GetPosition().z));
	}
	if ((ball->GetPosition().x - ball->getRadius()) < -(currentPlane->getWidth() / 2))
	{
		ball->SetPosition(glm::vec3(-(currentPlane->getWidth() / 2) + ball->getRadius(), ball->GetPosition().y, ball->GetPosition().z));
	}
}

glm::quat PhysicsModel::GetNewOrientation(void)
{
	glm::quat tempQuat = glm::quat(glm::cross(GetCurrentPlane(0)->FindNormal(), glm::vec3(0.0, 1.0, 0.0)));
	return tempQuat * glm::quat(0.0, 0.0, -1.0, 0.0);
}
