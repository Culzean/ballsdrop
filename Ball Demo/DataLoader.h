#ifdef _MSC_VER
	#pragma once
#endif

#ifndef DATALOADER_INC
#define DATALOADER_INC

#include <glew.h>
#include <fstream>
#include <iostream>

class DataLoader{

public:
	DataLoader();
	static GLfloat* loadData(char* fileName);	//For loading in a formatted set of vertices.
	static char* loadFile(char* fileName);		//For loading the contents of a file.
	static GLubyte* loadGLubyte(char* fileName);  //load texture data from a list
	~DataLoader();

private:
};


#endif