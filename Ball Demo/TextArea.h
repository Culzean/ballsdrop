#pragma once

#include "stafx.h"
#include <SDL_ttf.h>
#include <SDL_image.h>

class TextArea : public TextureManager
{
	public:
		TextArea(glm::vec3 centerPosition, char *fontFile, char *message, SDL_Color color, ShaderManager shaderManager);
		TextArea(glm::vec3 centerPosition, char *fileName, ShaderManager shaderManager);
		~TextArea();
		void TextToTexture(GLuint &texture, char *message, TTF_Font *font, SDL_Color colour, GLuint &width, GLuint &height);
		void MakeQuad(glm::vec3 centerPosition, GLuint width, GLuint height);
		virtual void Render(MatrixStack &projectionStack, MatrixStack &modelviewStack, bool WIRE_FRAME, bool FLIPPED);
		void UpdateTextString(char *newMessage);
		void UpdateTextInteger(int value);
		void UpdateTextFloat(float value);
		void SetColour(SDL_Color newColor);
		void SetAlpha(float alphaValue) {alpha = alphaValue; }
		glm::vec3 GetPosition(void) { return centerPos; }
		void SetPosition(glm::vec3 position) { centerPos = position; }
		void SetZ(float newZ) { centerPos.z = newZ; }

	private:
		TTF_Font	*font;
		SDL_Color	textColor;
		GLuint		width, height;
		glm::vec3	centerPos;
		GLuint		tex;
		char		*message;
		GLuint		vao;
		GLuint		vbo[2];
		glm::vec3	verts[4];
		glm::vec2	texCoords[4];
		GLuint		shaderP;
		float		alpha;
};