#ifndef DEBUG_RENDER_H
#define DEBUG_RENDER_H

#include "stafx.h"

#include "RenderHeaders.h"

class DebugRend : public Renderable {

public:
	DebugRend(GLint maxVerts);
	virtual ~DebugRend();

	virtual void Render( MatrixStack &projectionStack, MatrixStack &modelviewStack, bool WIRE_FRAME );
	virtual void Init();
	virtual void Update(float dt);

	bool loadData(char * fname);
	bool loadData(glm::vec3 data[3]);

	bool loadVector(glm::vec3 _vector, glm::vec3 _pos, glm::vec3 _col ,bool normalized = true);


	void SetNumbVerts( GLuint iVerts )	{	this->iNoVerts = iVerts;	}

	void EndMesh();

	void AssignShader(GLuint shader);

private:

	GLint		iMaxVerts;
	GLuint		shader;

};


#endif