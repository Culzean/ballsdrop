#include "TrackSect.h"


TrackSect::TrackSect(PhysicsManager* _physics) {
	physics = _physics;
	iNumbPlanes = 0;
}

TrackSect::~TrackSect() {
	int end = planes.size();
	for(GLuint i=0; i<planes.size() ; ++i)
	{
		if(planes[i] != NULL)
			delete planes[i];
		planes[i] = 0;
	}
}

bool TrackSect::findDimensions() {
	if(this->iNumbPlanes < 0) {
		std::cout << "This track contains no points! " << iNumbPlanes << std::endl;
		return false;
	}
	CompPoints(  );
	return true;
}

void TrackSect::CompPoints(  ) {
	glm::vec3 min = glm::vec3(10000000);
	glm::vec3 max = glm::vec3(-10000000);

	for(unsigned int k = 0; k< planes.size(); k++) {

		glm::vec3* _vertexList = planes[k]->getPoints();

		for(GLint i = 0; i< planes[k]->GetNumbPoints(); i++){
			for(unsigned int j = 0; j < 3; j++){
				if(min[j] > _vertexList[i][j]){
					min[j] = _vertexList[i][j];
				}
				if(max[j] < _vertexList[i][j]){
					max[j] = _vertexList[i][j];
				}
			}
		}
	}
	setDimensions(min, max);
}

void TrackSect::setDimensions(glm::vec3 _bottomLeft, glm::vec3 _topRight) {

	width = glm::abs(_bottomLeft.x - _topRight.x);
	depth = glm::abs(_bottomLeft.z - _topRight.z);
	height = glm::abs(_bottomLeft.y - _topRight.y);


	pos = _bottomLeft + ( (_topRight - _bottomLeft) / 2.0f);

}

Plane* TrackSect::getPlane(glm::vec3 _point) {
	//find the correct 
	for(GLuint i=0; i<planes.size() ; ++i){
		if( physics->pnpoly( planes[i]->GetNumbPoints(), planes[i]->getPoints(), _point.x, _point.z ) ) {
			return planes[i];
		}
	}
	//else return the last plane
	return NULL;
}