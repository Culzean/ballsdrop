#ifndef RACE_TRACK_H
#define RACE_TRACK_H

#include "Frame.h"

#include <vector>
#include "RenderHeaders.h"
#include "stafx.h"
#include "PhysicsManager.h"


class Plane;
class TrackSect;
class PhysicsManager;

class RaceTrack : public Mesh {

struct lightStruct
{
	glm::vec4 ambient;
	glm::vec4 diffuse;
	glm::vec4 specular;
	glm::vec4 position;
	glm::vec3 direction;
	float fieldOfLuminance;
	float intensityExponent;
	float constantAttenuation;
	float linearAttenuation;
	float quadraticAttenuation;
	float Eta;
};

struct materialStruct
{
	glm::vec4 ambient;
	glm::vec4 diffuse;
	glm::vec4 specular;
	glm::vec4 emmisive;
	float shininess;
	float ReflectionFactor;
}; 

public:
	RaceTrack(PhysicsManager* _physics);
	virtual ~RaceTrack();

	virtual void Render( MatrixStack &projectionStack, MatrixStack &modelviewStack, bool wireFrame );
	virtual void Update(GLfloat dt);
	virtual void Init();

	void ApplyTextures(GLuint texture, GLuint texture2);

	void addSection( TrackSect* nextSection )								{	raceTrack.push_back(nextSection);	};
	void addFinishPositions(glm::vec3 _last, glm::vec3 _secondLast)			{	lastPoint = _last; 	secondLastPoint = _secondLast;			};
	glm::vec3 getLast()														{	return lastPoint;	}	
	Plane* getSection( Plane* crtSection, glm::vec3 vCentPos );
	std::vector< TrackSect* > getPlane()									{	return raceTrack;	};

	TrackSect* getSection(GLint _idx)		{ return raceTrack[_idx]; }
	TrackSect* getSection();			

	TrackSect* getStart()								{	return raceTrack.front(); 	};
	TrackSect* findSection( glm::vec3 pos, GLfloat rad );

	Frame *trackFrame;

	GLint GetNumb()			{	return raceTrack.size();	}

private:

	PhysicsManager* physics;

	GLuint texture[2];
	glm::vec3 lastPoint;
	glm::vec3 secondLastPoint;

	GLuint crtSect;

	lightStruct light;
	materialStruct material;

	bool loadData();

	std::vector< TrackSect* > raceTrack;
};

#endif