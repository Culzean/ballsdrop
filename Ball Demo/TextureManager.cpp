#include "TextureManager.h"

using namespace std;

int TextureManager::lastID = 0;

TextureManager* TextureManager::pTexManager = 0;


TextureManager::TextureManager()
{
	//first texture to load is null texture.
	//if this cannot be found we have trouble
	//what recourse is there?
	//loadTexture("NullTexture.tga");
}

GLuint TextureManager::getTexID(const char* fname)
{
	index = Texmap.find(fname);

	if(index == Texmap.end())
		{
			if(loadTexture(fname) == -1)//load failed. attempt to load NULL texture
			{
				cout << "This texture cannot be found" << endl;
				index = Texmap.find("NullTexture.tga");
					if(index == Texmap.end())
					{
						cout << "Backup texture cannot be found!! Failed to load texture anything";
						return -1;//total failure. something has been put together wrong
					}else
						return Texmap["NullTexture.tga"];
			}
		}

		return Texmap[fname];
}

GLuint TextureManager::LoadTexBMP(char *fname)
{
	// generate texture ID
	GLuint	texID[1];
	glGenTextures(1, texID);
	glBindTexture(GL_TEXTURE_2D, *texID);

	SDL_Surface *tmpSurface;
	tmpSurface = SDL_LoadBMP(fname);

	SDL_PixelFormat *format = tmpSurface->format;

	// bind texture and set parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	if (format->Amask){
			glTexImage2D(GL_TEXTURE_2D, 0, GL_BGRA,
			tmpSurface->w, tmpSurface->h,
			0, GL_BGRA, GL_UNSIGNED_BYTE, tmpSurface->pixels);
	} 
	else{
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB,
			tmpSurface->w, tmpSurface->h,
			0, GL_BGR, GL_UNSIGNED_BYTE, tmpSurface->pixels);
	}
	SDL_FreeSurface(tmpSurface);

	return *texID;	// return value of texure ID
}

void TextureManager::InitTextures(void){

		const char *SKYBOXFILES[] = {
		"SkyBoxTextures/Skybox-Right.png", "SkyBoxTextures/Skybox-Left.png", "SkyBoxTextures/Skybox-Top.png",
		"SkyBoxTextures/Skybox-Bottom.png", "SkyBoxTextures/Skybox-Front.png", "SkyBoxTextures/Skybox-Back.png" };

	SKYBOX = LoadCubeMapTexPNG(SKYBOXFILES);

	TERRAIN = LoadTexPNG("Res/terrain.png");

	BALL = LoadTexPNG("Res/track.png");

	TRACK = LoadTexPNG("Res/moondown.png");
}

GLuint TextureManager::LoadTexPNG( const char* fname)
{
	GLuint texID = 0;
	//load texture using SDL method
	glGenTextures(1, &texID);

	//loading using the sdl basic bmp method
	SDL_Surface*	tempSurface = 0;

	tempSurface = IMG_Load(fname);
	if(NULL == tempSurface)
	{
		cout << "Error Loading png " << fname << endl;
		return NULL;
	}

	SDL_PixelFormat *format = tempSurface->format;

	glBindTexture(GL_TEXTURE_2D, texID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );

	//Test! for alpha channel
	if (format->Amask){
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA,
				tempSurface->w, tempSurface->h,
				0, GL_RGBA, GL_UNSIGNED_BYTE, tempSurface->pixels);
		} 
		else{
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB,
				tempSurface->w, tempSurface->h,
				0, GL_RGB, GL_UNSIGNED_BYTE, tempSurface->pixels);
		}

	glGenerateMipmap(GL_TEXTURE_2D);
	SDL_FreeSurface(tempSurface);

	//fname is the key. Add this texture to the map
	Texmap.insert(pair<string, GLuint>(fname,texID));

	cout << "New texture loaded : " << fname << endl;
	return texID;	// return value of texure ID
}

GLuint TextureManager::LoadCubeMapTexPNG(const char *fname[])
{
	GLuint texID[6];
	glGenTextures(6, texID);
	glBindTexture(GL_TEXTURE_CUBE_MAP, *texID); 

	GLuint targets[] = {
		GL_TEXTURE_CUBE_MAP_POSITIVE_X,
		GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
		GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
		GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
		GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
		GL_TEXTURE_CUBE_MAP_NEGATIVE_Z
	};
	SDL_Surface *tmpSurface;
	for( int i = 0; i < 6; i++ ) {
		
		tmpSurface = IMG_Load(fname[i]);

		SDL_PixelFormat *format = tmpSurface->format;

		glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

		if (format->Amask){
			glTexImage2D(targets[i], 0, GL_RGBA,
				tmpSurface->w, tmpSurface->h,
				0, GL_RGBA, GL_UNSIGNED_BYTE, tmpSurface->pixels);
		} 
		else{
			glTexImage2D(targets[i], 0, GL_RGB,
				tmpSurface->w, tmpSurface->h,
				0, GL_RGB, GL_UNSIGNED_BYTE, tmpSurface->pixels);
		}
		SDL_FreeSurface(tmpSurface);
		//fname is the key. Add this texture to the map

		Texmap.insert(pair<string, GLuint>(fname[i],texID[i]));
	}

	return *texID;
}


GLuint TextureManager::loadTexture(const char* fname)
{
	GLuint texID = 0;

	//load texture using SDL method
	glGenTextures(1, &texID);

	//loading using the sdl basic bmp method
	SDL_Surface*	tempSurface;

	tempSurface = SDL_LoadBMP(fname);
	if(!tempSurface)
	{
		cout << "Error Loading bitmap " << fname << endl;
		return NULL;
	}

	SDL_PixelFormat *format = tempSurface->format;

	glBindTexture(GL_TEXTURE_2D, texID);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );

	//Test! for alpha channel
	if (format->Amask){
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA,
			tempSurface->w, tempSurface->h,
			0, GL_RGBA, GL_UNSIGNED_BYTE, tempSurface->pixels);
	} 
	else{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB,
			tempSurface->w, tempSurface->h,
			0, GL_RGB, GL_UNSIGNED_BYTE, tempSurface->pixels);
	}

	glGenerateMipmap(GL_TEXTURE_2D);
	SDL_FreeSurface(tempSurface);

	//fname is the key. Add this texture to the map
	Texmap.insert(pair<string, GLuint>(fname,texID));

	cout << "New texture loaded : " << fname << endl;
	return texID;	// return value of texure ID
}

GLuint TextureManager::LoadCubeMapTexBMP(const char *fname[])
{
	GLuint texID[6];
	glGenTextures(6, texID);
	glBindTexture(GL_TEXTURE_CUBE_MAP, *texID); 

	GLuint targets[] = {
		GL_TEXTURE_CUBE_MAP_POSITIVE_X,
		GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
		GL_TEXTURE_CUBE_MAP_POSITIVE_Y,
		GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
		GL_TEXTURE_CUBE_MAP_POSITIVE_Z,
		GL_TEXTURE_CUBE_MAP_NEGATIVE_Z
	};

	for( int i = 0; i < 6; i++ ) {
		SDL_Surface *tmpSurface;
		tmpSurface = SDL_LoadBMP(fname[i]);

		SDL_PixelFormat *format = tmpSurface->format;

		glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

		if (format->Amask){
			glTexImage2D(targets[i], 0, GL_BGRA,
				tmpSurface->w, tmpSurface->h,
				0, GL_BGRA, GL_UNSIGNED_BYTE, tmpSurface->pixels);
		} 
		else{
			glTexImage2D(targets[i], 0, GL_RGB,
				tmpSurface->w, tmpSurface->h,
				0, GL_BGR, GL_UNSIGNED_BYTE, tmpSurface->pixels);
		}
	}

	//fname is the key. Add this texture to the map

	Texmap.insert(pair<string, GLuint>(*fname,texID[0]));

	return *texID;
}

GLuint TextureManager::Load1DTexture(char *fname) {

	if(this->Texmap.find(fname) != Texmap.end()) {
		//have this texture already, return this
		return this->getTexID(fname);
	}

	GLuint texID = 0;
	//load a 1d texture for use with toon shader
	//currently creating the texture from hard coded numbers
	glGenTextures(1, &texID);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_1D, texID);
	
	GLubyte *textureData;
	textureData = loader.loadGLubyte(fname);

	glTexImage1D(GL_TEXTURE_1D, 0, GL_RGB, 7, 0, GL_RGB, GL_UNSIGNED_BYTE, (const GLvoid*)textureData);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);

	Texmap.insert(pair<string, GLuint>(fname,texID));

	delete[] textureData;

	return this->getTexID(fname);
}

void TextureManager::cleanUp()
{
	index = Texmap.begin();
	for(; index != Texmap.end(); index++)
	{
		glDeleteTextures(1, ( &(*index).second ));
	}
	Texmap.clear();
}

TextureManager::~TextureManager(){
		

	}