#ifndef PLANE_H
	#define PLANE_H

#include "stafx.h"

class Plane;

class Plane
{
private:
	glm::vec3 normal;
	glm::vec3*	pos;
	GLint		iNumPos;
	glm::vec3 centre;
	GLfloat width, depth;
	GLfloat		planeDot;

	//gameplay variable
	bool	outOfBounds;

public:
	//quad or a triangle
	Plane::Plane(glm::vec3 pos0, glm::vec3 pos1, glm::vec3 pos2, glm::vec3 pos3);
	Plane::Plane(glm::vec3 pos0, glm::vec3 pos1, glm::vec3 pos2);
	~Plane();
	GLfloat GetAngleDegrees(void);
	GLint	GetNumbPoints()		{return this->iNumPos;};
	glm::vec3* getPoints()		{	return pos;	};

	void PrintOut();

	bool isOutOfBounds()				{	return outOfBounds;	}
	void setOutOfBounds( bool bounds )	{	outOfBounds = bounds;	}

	glm::vec3 getNormal()			{	return normal;	}
	glm::vec3 getPos()				{	return centre;	}
	GLfloat		getWidth()			{	return width;	}
	GLfloat		getDepth()			{	return depth;	}

	glm::vec3 FindNormal();
	GLfloat	Plane::findPoint( GLfloat x, GLfloat z );
};

#endif