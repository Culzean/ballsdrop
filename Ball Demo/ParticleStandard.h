#ifndef PARTICLE_STANDARD_H
#define PARTICLE_STANDARD_H

#include "stafx.h"
#include "Renderable.h"

class Plane;
class Frame;
class Camera;

const glm::vec3 MAX_VEL(5.0f, 5.0f, 5.0f);

class ParticleStandard : public Renderable{
private:
	int numParticles;
	GLfloat ratio;
	GLint target;
	GLint curParts;
	GLfloat* positions;
	GLfloat* colours;
	GLfloat* randTexCoord;
	GLfloat* vel;
	GLfloat* accel;
	GLfloat* life;
	void createParticle(int index);
	void InitArray();
	glm::vec3 norm;
	//plane object will follow a ball object
	Plane* spawnPlane;
	Ball* pBall;

	GLuint vao;
	GLuint* vbo;
	GLuint iNumBuffers;
	GLuint tex2;

	//vasriables to position the particle effect in relation to the player
	Frame *frame;
	glm::vec3 playerOffset;

public:
	ParticleStandard(const int n);
	ParticleStandard(const int n, Plane* cSpawnPlane);
	virtual ~ParticleStandard();
	virtual void Init();
	void bindParticle(void);
	int getNumParticles(void) const { return numParticles; }
	GLfloat* getPositions(void) const { return positions; }
	GLfloat* getColours(void) const { return colours; }
	GLfloat* getVel(void) const { return vel; }

	void setBall(Ball* _pBall, glm::vec3 _offSet);
	void ParticleStandard::setSpawnPos(glm::vec3 _pos) ;
	void setTexture2( GLuint _tex2 )		{	tex2 = _tex2;	};

	virtual void Update(float dt);
	virtual void Update(Camera* cam, float dt);
	virtual void Render( MatrixStack &projectionStack, MatrixStack &modelviewStack, bool WIRE_FRAME );
};

#endif
