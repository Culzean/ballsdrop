#pragma once

#ifndef OPTIONS_H
#define OPTIONS_H

#include "State.h"
#include "TextArea.h"

class Options : public State
{
	public:
		Options(void);
		~Options(void);
		void Init(WindowManager windowManager, ShaderManager shaderManager, int musicSelection);
		void Update(GLfloat dt);
		void GetInput(CXBOXController *Player1);
		void KeyHandler( SDL_Event sdlEvent);
		void Render(bool WIRE_FRAME);
		void Cleanup(void);
		int  GetSelection(void) { return selection; }
		void SetSelection(int value) { selection = value; }
		int GetMusic(void) {return musicSelection;}
		float GetTime(void) { return 0;}
		void AddTime(float time) {;}

	private:
		MatrixStack		projectionStack;
		MatrixStack		modelviewStack;
		XINPUT_STATE	prevState;
		TextArea		*music;
		TextArea		*theme;
		TextArea		*rock, *electro, *weird;
		SoundBank		*soundBank;
		int				selection;
		int				musicSelection;
		int				themeSelection;
		bool			subMenu;
};

#endif