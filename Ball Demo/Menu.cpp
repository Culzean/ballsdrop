#include "Menu.h"


Menu::Menu()
{
	selection = 0;
	repeatTimer = 0.0f;
}

Menu::~Menu()
{
}

void Menu::Init(WindowManager windowManager, ShaderManager shaderManager, int musicSelection)
{
	soundBank = SoundBank::GetInstance();
	projectionStack.loadMatrix(glm::perspective(45.0f, 4.0f / 3.0f, 0.5f, 1000.f));
	SDL_Color white = {255, 255, 255};

	single = new TextArea(glm::vec3(0.5, 0.8, 3.0), "GROBOLD.ttf", "SINGLE", white, shaderManager);
	multi = new TextArea(glm::vec3(0.5, -0.2, 3.0), "GROBOLD.ttf", "MULTI", white, shaderManager);
	options = new TextArea(glm::vec3(0.5, -1.2, 3.0), "GROBOLD.ttf", "MENU", white, shaderManager);
	quit = new TextArea(glm::vec3(0.5, -2.2, 3.0), "GROBOLD.ttf", "QUIT", white, shaderManager);
}

void Menu::Update(GLfloat dt)
{
	SDL_Color blue = {0, 0, 100};
	SDL_Color white = {255, 255, 255};

	repeatTimer += dt;
	if(repeatTimer > TIME_REPEAT_KEY)
	{
		repeat = true;
		repeatTimer = 0.0f;
	}

	if (selection == 0)
	{
		single->SetColour(blue);
		single->SetZ(4.5);
	}
	else
	{
		single->SetColour(white);
		single->SetZ(3.0);
	}

	if (selection == -1)
	{
		multi->SetColour(blue);
		multi->SetZ(4.5);
	}
	else
	{
		multi->SetColour(white);
		multi->SetZ(3.0);
	}

	if (selection == -2)
	{
		options->SetColour(blue);
		options->SetZ(4.5);
	}
	else
	{
		options->SetColour(white);
		options->SetZ(3.0);
	}

	if (selection == -3)
	{
		quit->SetColour(blue);
		quit->SetZ(4.5);
	}
	else
	{
		quit->SetColour(white);
		quit->SetZ(3.0);
	}
}

void Menu::GetInput(CXBOXController *Player1)
{
	if (Player1->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_UP 
		&& prevState.Gamepad.wButtons != XINPUT_GAMEPAD_DPAD_UP && (selection + 1) != 1)
	{
		selection += 1;
		soundBank->navigate.play();
	}

	if (Player1->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_DOWN 
		&& prevState.Gamepad.wButtons != XINPUT_GAMEPAD_DPAD_DOWN && (selection - 1) != -4)
	{
		selection -= 1;
		soundBank->navigate.play();
	}

	prevState = Player1->GetState();
}

void Menu::KeyHandler( SDL_Event sdlEvent )
{
	if(sdlEvent.type == SDL_KEYDOWN)
	{
		switch(sdlEvent.key.keysym.sym)
		{
			case SDLK_UP:
				if((selection + 1) != 1 && repeat){
					selection += 1;
				soundBank->navigate.play();
				}
				repeat = false;
			break;
			case SDLK_DOWN:
				if((selection - 1) != -4 && repeat){
					selection -= 1;
				soundBank->navigate.play();
				}
				repeat = false;
			break;
			case SDLK_LEFT:

			break;
			case SDLK_RIGHT:

			break;
			case SDLK_RETURN:
				

			break;
			default:
				break;
		}		
	}
}

void Menu::Render(bool WIRE_FRAME)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	if (WIRE_FRAME)
		glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	else
		glClearColor(0.0f, 0.8f, 0.4f, 1.0f);

	modelviewStack.loadIdentity();
	modelviewStack.push(); 
	
		single->Render(projectionStack, modelviewStack, WIRE_FRAME, true);
		multi->Render(projectionStack, modelviewStack, WIRE_FRAME, true);
		options->Render(projectionStack, modelviewStack, WIRE_FRAME, true);
		quit->Render(projectionStack, modelviewStack, WIRE_FRAME, true);

	modelviewStack.pop();
}

void Menu::Cleanup(void)
{
	delete single;
	delete multi;
	delete options;
	delete quit;
}