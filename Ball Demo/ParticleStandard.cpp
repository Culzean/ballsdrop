#include "ParticleStandard.h"
#include <cstdlib>
#include <ctime>


ParticleStandard::ParticleStandard(const int n) : numParticles( n ) {

	if ( numParticles <= 0 ) // trap invalid input
		return;
	InitArray();
	frame = new Frame();
	frame->SetOrigin(glm::vec3(0.0f));
	pBall = NULL;
	InitArray();
}

void ParticleStandard::bindParticle() {

	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(iNumBuffers, vbo);

	//and sort the particle system
	glBindVertexArray(vao);
	glBindBuffer(GL_ARRAY_BUFFER, vbo[0]); // bind VBO for positions
	glBufferData(GL_ARRAY_BUFFER, getNumParticles() * 3 * 
	sizeof(GLfloat), getPositions(), GL_DYNAMIC_DRAW); //DYNAMIC
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);     // Enable attribute index 0

	// Colours data in attribute 1, 3 floats per vertex
	glBindBuffer(GL_ARRAY_BUFFER, vbo[ATTRIBUTE_COLOR]); // bind VBO for colours
	glBufferData(GL_ARRAY_BUFFER, getNumParticles() * 3 * 
		sizeof(GLfloat), getColours(), GL_STATIC_DRAW);
	glVertexAttribPointer(ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(ATTRIBUTE_COLOR);    // Enable attribute index 1

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_POINT_SPRITE);
	glEnable(GL_PROGRAM_POINT_SIZE);
}

ParticleStandard::ParticleStandard(const int n, Plane* cSpawnPlane) : numParticles( n ) {

	if ( numParticles <= 0 ) // trap invalid input
		return;
	spawnPlane = cSpawnPlane;
	frame = new Frame();
	pBall = NULL;
	norm = spawnPlane->getNormal();
	InitArray();
}

void ParticleStandard::createParticle(int index) {

	//only make a particle if we want more!
	if(target > curParts) {

	curParts++;

	GLfloat velScalar = glm::linearRand(1.0f, 0.0f);
	GLfloat offSetX = this->spawnPlane->getWidth() * 0.5f;

	positions[index] = ( spawnPlane->getPos().x + glm::compRand1(-spawnPlane->getWidth()/2, spawnPlane->getWidth()/2) ) ;
	positions[index+2] = ( spawnPlane->getPos().z + glm::compRand1(-spawnPlane->getDepth()/2, spawnPlane->getDepth()/2) );		
	positions[index+1] =  spawnPlane->getPos().y + spawnPlane->findPoint(positions[index], positions[index+2]);

	norm /= 3;

	vel[index] = velScalar * norm.x;
	vel[index+1] = velScalar * norm.y;
	vel[index+2] = velScalar * norm.z;

	accel[index] = glm::compRand1(0.1f, 0.001f);
	GLfloat randY = glm::compRand1(2.0f,0.02f) + glm::compRand1(2.0f,0.02f)+ glm::compRand1(2.0f,0.02f);
	randY /= 3;
	accel[index+1] = randY;
	accel[index+2] = glm::compRand1(0.1f, 0.001f);

	colours[index] = (1.0f);
	colours[index+1] = (1.0f);
	colours[index+2] = (1.0f);

	randTexCoord[index] =  glm::compRand1(0.0f,1.0f);
	randTexCoord[index+1] =  glm::compRand1(0.0f,1.0f);

	//life in ms
	life[index] = glm::compRand1(2480.0f,640.0f);
	} else {
		//remove a particle!
		colours[index] = (0.0f);
		colours[index+1] = (0.0f);
		colours[index+2] = (0.0f);
		randTexCoord[index+1] =  0.0f;
	}
}


void ParticleStandard::Render( MatrixStack &projectionStack, MatrixStack &modelviewStack, bool WIRE_FRAME ) {

	glm::mat4 matrix(1.0);

	modelviewStack.push(matrix);
	matrix = frame->GetMatrix();
	modelviewStack.multiplyMatrix(matrix);


	glBindVertexArray(this->vao); // bind member vao
	glUseProgram(shaderID);

	int uniformIndex = glGetUniformLocation(shaderID, "projection");
	glUniformMatrix4fv(uniformIndex, 1,GL_FALSE,glm::value_ptr(projectionStack.getMatrix()));
	uniformIndex = glGetUniformLocation(shaderID, "modelview");
	glUniformMatrix4fv(uniformIndex, 1,GL_FALSE,glm::value_ptr(modelviewStack.getMatrix()));
	uniformIndex = glGetUniformLocation(shaderID, "player.position");
	glUniform4fv(uniformIndex, 1, glm::value_ptr( pBall->GetPosition()) );
	uniformIndex = glGetUniformLocation(shaderID, "player.velocity");
	glUniform3fv(uniformIndex, 1, glm::value_ptr( pBall->getVelocity() ));

	GLint texUniform = glGetUniformLocation(shaderID, "texture0");
	glUniform1i( texUniform, 0 );
	texUniform = glGetUniformLocation(shaderID, "texture1");
	glUniform1i( texUniform, 1 );

	// Now draw the particles... as easy as this!
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA ); //RUGH?
	glDepthMask(0);
	glEnable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);
	glPointSize(9);


	glEnableVertexAttribArray(ATTRIBUTE_VERTEX); // Enable attribute index 0
	glEnableVertexAttribArray(ATTRIBUTE_COLOR);
	glEnableVertexAttribArray(ATTRIBUTE_TEXTURE0);
	// particle data updated - so need to resend to GPU
	glBindBuffer(GL_ARRAY_BUFFER, vbo[0]); // bind VBO 4
	glBufferData(GL_ARRAY_BUFFER, this->getNumParticles() * 3 *
		sizeof(GLfloat), this->positions, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, 0);
	//adn send in the colour data
	glBindBuffer(GL_ARRAY_BUFFER, vbo[ATTRIBUTE_COLOR]); // bind VBO for colours
	glBufferData(GL_ARRAY_BUFFER, getNumParticles() * 3 * 
		sizeof(GLfloat), getColours(), GL_DYNAMIC_DRAW);
	glVertexAttribPointer(ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ARRAY_BUFFER, vbo[ATTRIBUTE_TEXTURE0]); // bind texture coords
	glBufferData(GL_ARRAY_BUFFER, getNumParticles() * 3 * 
		sizeof(GLfloat), this->randTexCoord, GL_DYNAMIC_DRAW);
	glVertexAttribPointer(ATTRIBUTE_TEXTURE0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	// Position data in attribute index 0, 3 floats per vertex
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texID);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_1D, this->tex2);
	glDrawArrays(GL_POINTS, 0, this->numParticles );

	glDisableVertexAttribArray(ATTRIBUTE_VERTEX);
	glDisableVertexAttribArray(ATTRIBUTE_COLOR);
	glDisableVertexAttribArray(ATTRIBUTE_TEXTURE0);
	glDisable(GL_BLEND);
	glDepthMask(1);
	glEnable(GL_DEPTH_TEST);

	modelviewStack.pop();
}

void ParticleStandard::setBall(Ball* _pBall, glm::vec3 _offSet) {
	pBall = _pBall;
	playerOffset = _offSet * 80.0f;
}

void ParticleStandard::Update(float dt) {

} 

void ParticleStandard::Update(Camera* cam, float dt) {

	frame->SetOrigin(cam->GetPosition() + cam->GetViewDirection() * 120.0f);

	if(pBall != NULL){
		//update the number of particle to spawn
		ratio = pBall->getfVelocity() / MAX_VELOCITY;

		target = (GLint)(this->numParticles * ratio);

		//std::cout << "hot many particles> " << this->curParts << "how many we want: " << this->target << std::endl;
	}

	glm::vec3 update = pBall->getfVelocity() * 0.1f * glm::normalize( pBall->getVelocity() );
	GLint i =0;GLint end = 0;
	for(i=0, end = numParticles*3; i < end; i+=3) 
	{
		positions[i] += vel[i] + ( update.x);
		positions[i+1] += vel[i+1]  + ( update.y);
		positions[i+2] += vel[i+2] + ( update.z);

		//update vel
		vel[i] += accel[i] ;
		vel[i+1] += accel[i+1];
		vel[i+2] += accel[i+2];

		//restrain velocity
		for(int j= 0; j<3; ++j) {
			if(glm::abs(vel[j]) > MAX_VEL[j]) {
					vel[j] = MAX_VEL[j];
			}
		}
		life[i] -= 30;
		if( life[i] < 0)
		{
			//if this just changed to from live particle we need to record that
			if(life[i] + 30 >= 0) {
				this->curParts--;
			} else {
				life[i] = -100;
			}
			createParticle(i);
		}
	}
}

void ParticleStandard::Init() {
	this->bindParticle();
	glEnable(GL_TEXTURE_2D);
}

void ParticleStandard::InitArray() {

	curParts = 0;
	target = numParticles;

	positions = new GLfloat[numParticles * 3];
	randTexCoord = new GLfloat[numParticles * 3];	//only require 2 per particle. But this is easier for iterating over
	colours = new GLfloat[numParticles * 3];
	vel = new GLfloat[numParticles * 3];
	accel = new GLfloat[numParticles * 3];
	//wasting much space for ease of use runtime
	life = new GLfloat[numParticles * 3];

	// lets initialise with some lovely random values!
	std::srand((GLint)std::time(0));
	for (int i=0;i<numParticles * 3;i++) {
		
		colours[i] = ( std::rand() % 100 ) / 100.0f;
		
	}
	//set random offset placed within plane
	for (int i=0;i<numParticles * 3;i+=3) {
		createParticle(i);
		
		//std::cout << "garbage numbers: " << positions[]std::endl;
	}
	iNumBuffers = 3;
	vbo = new GLuint[iNumBuffers];
	this->bindParticle();
}

void ParticleStandard::setSpawnPos(glm::vec3 _pos) {
	frame->SetOrigin(_pos);
}

ParticleStandard::~ParticleStandard() {

	glDeleteBuffers(iNumBuffers, vbo);
	glDeleteVertexArrays(1, &vao);

	if(this->spawnPlane) {
		delete spawnPlane;}

	delete frame;
	delete [] positions;
	delete [] colours;
	delete [] randTexCoord;
	delete [] vel;
	delete [] accel;
	delete [] life;
	delete [] vbo;
}