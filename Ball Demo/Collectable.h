#pragma once

#ifndef COLLECTABLE_H
#define COLLECTABLE_H

#include "OBJ_Model.h"
#include "stafx.h"

class Collectable
{

public:
	Collectable(glm::vec3 iPosition, glm::vec3 direction, char *fileName, ShaderManager shaderManager);
	~Collectable(void);
	bool CollidesWith(Ball *ball);
	void Render(MatrixStack &projectionStack, MatrixStack &modelviewStack, bool WIRE_FRAME);
	void Cleanup(void);

private:
	void SetUpModel(char *fileName);
	void SetUpFrame(glm::vec3 iPosition, glm::vec3 direction);
	void SetUpVBOs(void);
	OBJ_Model *model;
	GLfloat	radius;
	Frame *frame;
	GLuint vao;
	GLuint vbo[3];
	GLuint shader;
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec3> normals;
	std::vector<glm::vec2> texCoords;
};


#endif