#ifndef XBOX_CONTROLLER_H
#define XBOX_CONTROLLER_H

// No MFC
#define WIN32_LEAN_AND_MEAN

// We need the Windows Header and the XInput Header
#include <windows.h>
#include <XInput.h>
#include <glm/glm.hpp>
#include <iostream>

// Now, the XInput Library
// NOTE: COMMENT THIS OUT IF YOU ARE NOT USING A COMPILER THAT SUPPORTS THIS METHOD OF LINKING LIBRARIES
#pragma comment(lib, "XInput.lib")

#define XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE  7849

// XBOX Controller Class Definition
class CXBOXController
{
private:
	XINPUT_STATE _controllerState;
	int _controllerNum;

	float magnitudeL;
	float magnitudeR;

public:
	CXBOXController(int playerNumber);
	XINPUT_STATE GetState(void);
	float GetLeftThumbX(void);
	float GetLeftThumbY(void);
	float GetLeftMagnitude(void);
	float GetRightThumbX(void);
	float GetRightThumbY(void);
	float GetRightMagnitude(void);
	bool IsConnected(void);
	int GetControllerNumb()  { return this->_controllerNum; }
	void PrintDisconnectionError(void)
	{
		std::cout << "ERROR! PLAYER " << _controllerNum + 1 << " - XBOX 360 Controller Not Found!\n";
		std::cout << "Press Any Key To Continue... ";
		std::cin.get();
	}
	void Vibrate(int leftVal = 0, int rightVal = 0);
};

#endif