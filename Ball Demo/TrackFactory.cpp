#include "TrackFactory.h"


TrackFactory::TrackFactory(ShaderManager _shaderManager)
{
	physics = nullptr;
	shaderManager = _shaderManager;
	physics = new PhysicsManager();

	trackLoader = new OBJ_Model();
}

TrackFactory::~TrackFactory() {
	//don't take this literally
	delete trackLoader;
	if(nullptr != physics )
	{
		delete physics;
	}
}

glm::vec3 TrackFactory::TranslateSection(glm::vec3 _point, glm::vec3 diff)
{
	glm::vec3 point = _point;
	glm::mat4 translation;
	glm::vec4 homPoint;

	translation = glm::mat4(1.0,0.0,0.0, diff.x,  //create a transform matrix
							0.0,1.0,0.0,diff.y,
							0.0,0.0,1.0,diff.z,
							0.0,0.0,0.0,1.0);

	homPoint = glm::vec4(point, 1);  //create a homfsfhsuidbfhus point......you know the one I mean(1 means its a position)
	homPoint = homPoint * translation; //move the point
	point = (glm::vec3)homPoint;   //cast back into a normal point

	return point;
}

void TrackFactory::RotateSection(glm::vec3 &_point, glm::vec2 v1, glm::vec2 v2)
{
	GLfloat angleY = glm::atan2(v2.y,v2.x) - glm::atan2(v1.y,v1.x);

	glm::mat3 rotY = glm::mat3(cos(angleY), 0.0, sin(angleY),		//create a Y rotation matrix
						     0.0, 1.0,         0.0,
						-sin(angleY), 0.0, cos(angleY));

	_point = _point * rotY;
}

RaceTrack* TrackFactory::GenerateTrack( const std::vector< TRACK_BENDS > theBends )
{
	RaceTrack* track = new RaceTrack(physics);
	
	track->AssignShader(shaderManager.TOON);


	Plane* pTempPlane = NULL;

	glm::vec3 globalBottomLeft, globalTopRight;

	TrackSect* pSect = NULL;

	glm::vec3 diff;

	glm::vec3 last, secondlast;

	glm::vec2 v1,v2;

	
	glm::vec3 norms[3];
	glm::vec2 tex[3];

	glm::vec3 sidePoint;

	if(theBends[0] == TRK_LEFT)
		trackLoader->Load("Res/trk_Left.obj");
	else
	if(theBends[0] == TRK_RIGHT)
		trackLoader->Load("Res/trk_Right.obj");
	
	track->BeginMesh( trackLoader->GetVertices().size() * theBends.size());

	for(unsigned int i= 0; i < theBends.size(); ++i)   //loop for number of track sections
	{
		pSect = new TrackSect(physics);
		std::vector<glm::vec3> points(3);
		//reset points for new section

		if(i != 0)
		{
			trackLoader->ClearVectors();
			if(theBends[i] == TRK_LEFT)
			{
				trackLoader->Load("Res/trk_Left.obj");
			}
			else
			if(theBends[i] == TRK_RIGHT)
			{
				trackLoader->Load("Res/trk_Right.obj");
			}
		}

		vertices = trackLoader->GetVertices();
		normals = trackLoader->GetNormals();
		texCoords = trackLoader->GetTexCoords();

		for(GLuint j = 0; j < vertices.size(); j+=3)   //loop for every triangle
		{
			//slight problem were blender didn't export in right order so code later on does alot of points might not make sence 
			points[0] = vertices[j];
			points[1] = vertices[j+1];
			points[2] = vertices[j+2];

			if(i != 0)  //if not the first time round the main loop
			{
				if(j == 0) //if first time round second loop(aka first triangle of the new track)
				{
					sidePoint = vertices[j+5];   // point to the right on the second triangle
					diff = last - points[2];   //calc the difference from the last point in the previous track to the first left point(again left point isn't the first >.<)
					
					for(int k =0; k < 3; k++) //loop for the three points in triangle
					{
						points[k] = TranslateSection(points[k], diff);
					}

					sidePoint = TranslateSection(sidePoint, diff);
					
					////project vectors onto x,z plane for Y rotation calculation
					v1 = glm::vec2(secondlast.x,secondlast.z) -  glm::vec2(last.x,last.z);   //get the target vector
					v2 = glm::vec2(sidePoint.x, sidePoint.z) - glm::vec2(points[2].x, points[2].z);  //get the vector you wish to rotate from


					for(int k = 0; k<3; k++)  //loop for the three points in the triangle
					{
						points[k] = TranslateSection(points[k], -diff);  //translate to centre
						RotateSection(points[k], v1, v2);    //rotate
						//norms[k] = RotateSection(norms[k], v1, v2);
						points[k] = TranslateSection(points[k], diff);   //translate back
					}

					points[2] = last;
				}
				else   //now for the rest of the triangles(may need to add a seperate case for the second triangle as it shares points with the first)
				{
					int loopSize = 3;
					if(j == 3) //because this point on the second triangle shares with the already rotated points
					{
						points[2] = secondlast;
						loopSize = 2;
					}					


					for(int k = 0; k<loopSize; k++) //loop for the three points in the triangle
					{
						RotateSection(points[k], v1, v2);     //rotate
						points[k] = TranslateSection(points[k], diff);    //translate back
						//norms[k] = normals[j + k];
					}


					if(j == 3)
						points[1] = last;
				}
			}

			norms[0] = normals[j];
			norms[1] = normals[j+1];
			norms[2] = normals[j+2];

			tex[0] = texCoords[j];
			tex[1] = texCoords[j+1];
			tex[2] = texCoords[j+2];	
			
			pTempPlane = new Plane(points[0], points[1], points[2] );
			pTempPlane->setOutOfBounds((theBends[i] == TRK_OUTOFBOUNDS));

			pSect->addPlane(pTempPlane);
			track->AddIndexedTriangle(points, norms, tex);
			
		}
		//finished building one section
		//place into the track
		last = points[2];
		secondlast = points[1];

		//create an outofbounds plane for the track
		pSect->CompPoints();
		//add section to track
		track->addSection(pSect);	
	}

	//ApplyOverLap(track);

	pSect = new TrackSect(physics);

	globalBottomLeft = last * 1000.0f;
	globalTopRight = secondlast * 1000.0f;

	AddOutOfBounds(globalBottomLeft, globalTopRight, pSect);
	track->addSection(pSect);
	track->addFinishPositions(last,secondlast);

	track->EndMesh();


	return track;
}

/*
	Take in two points for the bounding box of the current section

	flatten these values and add some padding to width

	//then create two triangles at the bottom of bounding box and add this
*/
void TrackFactory::AddOutOfBounds(glm::vec3 _bottomLeft, glm::vec3 _topRight, TrackSect* _pSect) {

	if(_bottomLeft.y > _topRight.y) {
		_bottomLeft.y = _topRight.y;
	} else {
		_topRight.y = _bottomLeft.y;
	}
	_bottomLeft.y *= 5.0f;
	_topRight.y *= 5.0f;

	glm::vec3 mainpoints[4];

	mainpoints[0] = _bottomLeft;
	mainpoints[1] = _bottomLeft * glm::vec3(-1.0, 1.0, 1.0);
	mainpoints[2] = _topRight;
	mainpoints[3] = _topRight * glm::vec3(-1.0, 1.0, 1.0);

	Plane* tempPlane = new Plane( mainpoints[0], mainpoints[2], mainpoints[1] );
	tempPlane->setOutOfBounds(true);

	_pSect->addPlane(tempPlane);
	//swizzle	
	tempPlane = new Plane( mainpoints[1], mainpoints[3], mainpoints[2] );
	tempPlane->setOutOfBounds(true);

	_pSect->addPlane(tempPlane);
}


std::vector< TRACK_BENDS > TrackFactory::SelectTrack( int _select )
{
	std::vector< TRACK_BENDS >theBends;
	switch(_select)
	{
	case 0 :

		theBends = GetEasyTrack();
		std::cout << "Selecting the Easy Track" << std::endl;
		break;

	case 1 :

		theBends = GetNormalTrack();
		std::cout << "Selecting the Normal Track" << std::endl;
		break;

	case 2 :

		theBends = GetToughTrack();
		std::cout << "Selecting the Tough Track" << std::endl;
		break;

	default:
		theBends = GetNormalTrack();
		std::cout << "Selecting the default Track" << std::endl;
		break;
	}
	
	return theBends;
}

std::vector< TRACK_BENDS > TrackFactory::GetEasyTrack()
{
	TRACK_BENDS	track[] = { TRK_LEFT, TRK_LEFT, TRK_LEFT, TRK_LEFT,TRK_RIGHT, TRK_LEFT, TRK_LEFT,TRK_RIGHT, TRK_LEFT, TRK_LEFT,TRK_LEFT, TRK_LEFT,TRK_LEFT, TRK_LEFT,
	TRK_LEFT, TRK_LEFT,TRK_LEFT,TRK_RIGHT, TRK_RIGHT, TRK_LEFT,TRK_LEFT, TRK_LEFT, TRK_LEFT,TRK_LEFT, TRK_LEFT, TRK_LEFT,TRK_LEFT, TRK_LEFT, TRK_LEFT,TRK_LEFT, TRK_LEFT};
	return std::vector< TRACK_BENDS >( track, track + sizeof(track) / sizeof(track[0]) );
}

std::vector< TRACK_BENDS > TrackFactory::GetToughTrack()
{
	TRACK_BENDS	track[] = { TRK_RIGHT, TRK_RIGHT, TRK_RIGHT, TRK_LEFT, TRK_LEFT, TRK_RIGHT, TRK_RIGHT, TRK_RIGHT, 
		TRK_LEFT, TRK_RIGHT, TRK_LEFT, TRK_RIGHT, TRK_RIGHT, TRK_RIGHT, TRK_LEFT, TRK_RIGHT, TRK_RIGHT,
		TRK_RIGHT, TRK_LEFT, TRK_RIGHT, TRK_LEFT, TRK_RIGHT, TRK_RIGHT, TRK_RIGHT,
		TRK_LEFT,TRK_LEFT, TRK_LEFT, TRK_LEFT,TRK_LEFT, TRK_LEFT,
		TRK_RIGHT, TRK_RIGHT,TRK_LEFT, TRK_LEFT, TRK_RIGHT, 
		TRK_RIGHT, TRK_RIGHT,TRK_RIGHT, TRK_RIGHT,TRK_RIGHT,
		TRK_LEFT,TRK_RIGHT,TRK_LEFT,TRK_RIGHT,TRK_LEFT,TRK_LEFT, TRK_LEFT
	};
	return std::vector< TRACK_BENDS >( track, track + sizeof(track) / sizeof(track[0]) );
}

std::vector< TRACK_BENDS > TrackFactory::GetNormalTrack()
{
	TRACK_BENDS	track[] = { TRK_RIGHT, TRK_LEFT, TRK_LEFT, TRK_RIGHT, TRK_LEFT, TRK_RIGHT, TRK_LEFT, TRK_RIGHT, TRK_LEFT, TRK_RIGHT, TRK_LEFT,TRK_RIGHT,TRK_LEFT,TRK_RIGHT,TRK_LEFT,
		TRK_RIGHT, TRK_LEFT, TRK_RIGHT,TRK_LEFT,TRK_LEFT, TRK_LEFT, TRK_LEFT,
		TRK_RIGHT, TRK_LEFT, TRK_RIGHT, TRK_LEFT, TRK_RIGHT, TRK_RIGHT, TRK_RIGHT, TRK_LEFT, TRK_RIGHT, TRK_RIGHT, TRK_RIGHT,TRK_LEFT,TRK_RIGHT,TRK_LEFT,TRK_RIGHT,TRK_LEFT,TRK_LEFT, TRK_LEFT, TRK_LEFT,
		TRK_LEFT,TRK_LEFT, TRK_LEFT, TRK_LEFT};
	return std::vector< TRACK_BENDS >( track, track + sizeof(track) / sizeof(track[0]) );
}


glm::vec3 TrackFactory::findNorm(const glm::vec3 &v0, const glm::vec3 &v1) {
	return glm::cross(v1, v0);
}