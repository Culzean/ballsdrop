#include "SoundEffect.h"

void SoundEffect::loadSound(char *filename){
	Buffer.loadFromFile(filename);
	Sound.setBuffer(Buffer);
}

void SoundEffect::play(){
	Sound.play();
}

void SoundEffect::stop()
{
	Sound.stop();
}

void SoundEffect::SetLoop(bool loop){
	if (loop){
		Sound.setLoop(true);
	}
	else{
		Sound.setLoop(false);
	}
}