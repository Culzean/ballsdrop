#include "Collectable.h"


Collectable::Collectable(glm::vec3 iPosition, glm::vec3 direction, char *fileName, ShaderManager shaderManager)
{
	this->SetUpModel(fileName);
	this->SetUpFrame(iPosition, direction);
	this->SetUpVBOs();
	this->shader = shaderManager.COLOR_ONLY;
}

Collectable::~Collectable(void)
{

}

void Collectable::SetUpModel(char *fileName)
{
	model = new OBJ_Model();
	model->Load(fileName);
	vertices = model->GetVertices();
	normals = model->GetNormals();
	texCoords = model->GetTexCoords();
	radius = 0.0f;
	for(GLuint i = 0; i < vertices.size(); i++)
	{ 
		if(glm::abs(vertices[i].x) > radius)			radius = glm::abs(vertices[i].x);
		if(glm::abs(vertices[i].y) > radius)			radius = glm::abs(vertices[i].y);
		if(glm::abs(vertices[i].z) > radius)			radius = glm::abs(vertices[i].z);
	}
	delete model;
}

void Collectable::SetUpFrame(glm::vec3 iPosition, glm::vec3 direction)
{
	frame = new Frame();
	frame->SetOrigin(iPosition);
	frame->SetForwardVector(direction);
	frame->TranslateLocal( radius, radius, radius );
}

void Collectable::SetUpVBOs(void)
{
	glGenVertexArrays(1, &vao); // Allocate & assign Vertex Array Object (VAO)
	glBindVertexArray(vao); // Bind VAO as current object

	glGenBuffers(3, vbo);

	glBindBuffer(GL_ARRAY_BUFFER, vbo[0]); //Bind the vertex buffer
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * vertices.size(), &vertices[0], GL_STATIC_DRAW);
	glVertexAttribPointer(ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(ATTRIBUTE_VERTEX);

	glBindBuffer(GL_ARRAY_BUFFER, vbo[1]); //Bind the normal buffer
	glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(glm::vec3), &normals[0] , GL_STATIC_DRAW);
	glVertexAttribPointer(ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(ATTRIBUTE_COLOR);
}

bool Collectable::CollidesWith(Ball *ball)
{
	float distX = glm::abs(frame->GetOriginX() - ball->GetPosition().x);
	float distY = glm::abs(frame->GetOriginY() - ball->GetPosition().y);
	float distZ = glm::abs(frame->GetOriginZ() - ball->GetPosition().z);
	distX -= radius;// what is collectables radius?
	distY -= radius;
	distZ -= radius;
	
	if (((distX * distX) + (distY * distY) + (distZ * distZ)) <= ball->getRadius() * ball->getRadius())
	{
		return true;
	}
	return false;
}

void Collectable::Render(MatrixStack &projectionStack, MatrixStack &modelviewStack, bool WIRE_FRAME)
{
	glBindVertexArray(vao);
	glUseProgram(shader);
	
	glm::mat4 matrix(1.0);

	modelviewStack.push(matrix);
	matrix = frame->GetMatrix();
	modelviewStack.multiplyMatrix(matrix);

	int uniformIndex = glGetUniformLocation(shader, "projection");
	glUniformMatrix4fv(uniformIndex, 1,GL_FALSE,glm::value_ptr(projectionStack.getMatrix()));
	uniformIndex = glGetUniformLocation(shader, "modelview");
	glUniformMatrix4fv(uniformIndex, 1,GL_FALSE,glm::value_ptr(modelviewStack.getMatrix()));
			
	glEnableVertexAttribArray(ATTRIBUTE_VERTEX);
	glEnableVertexAttribArray(ATTRIBUTE_COLOR);
					
	if (WIRE_FRAME)
	{
		glDrawArrays(GL_LINES, 0, vertices.size());
	}
	else
	{
		glDrawArrays(GL_TRIANGLES, 0, vertices.size());
	}

	glDisableVertexAttribArray(ATTRIBUTE_VERTEX);
	glDisableVertexAttribArray(ATTRIBUTE_COLOR);

	modelviewStack.pop();

	glUseProgram(0);
	glBindVertexArray(0);
}

void Collectable::Cleanup(void)
{
	delete frame;
}