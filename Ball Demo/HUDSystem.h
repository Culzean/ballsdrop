#pragma once

#ifndef HUDSYSTEM_H
#define HUDSYSTEM_H

#include "WindowManager.h"
#include "TextArea.h"
#include "Ball.h"
#include "Camera.h"

class HUDSystem
{
public:
	void Init(WindowManager windowManager, ShaderManager shaderManager);
	void Update(Ball *ball, Camera *camera);
	void SetPaused(bool PAUSE_STATE)	{ PAUSED = PAUSE_STATE; }
	bool IsPaused(void) { return PAUSED; }
	void SetDisconnected(bool DISCONNECTED_STATE)	{ DISCONNECTED = DISCONNECTED_STATE; }
	void RenderDynamic(MatrixStack &projectionStack, MatrixStack &modelviewStack, bool WIRE_FRAME);
	void RenderStatic(MatrixStack &projectionStack, MatrixStack &modelviewStack, bool WIRE_FRAME);
	void UpdateTimer(float timerValue) { timer = timerValue; }
	void Cleanup(void);

private:
	glm::mat4	HUD_matrix;
	TextArea	*HUD_velocity;
	TextArea	*HUD_altitude;
	TextArea	*HUD_altitudeValue;
	TextArea	*HUD_disconnected;
	TextArea	*HUD_timer;
	TextArea	*HUD_timerValue;
	TextArea	*HUD_back;
	TextArea	*HUD_ok;
	TextArea	*HUD_resume;
	TextArea	*HUD_menu;
	bool		PAUSED;
	float		timer;
	bool		DISCONNECTED;
};

#endif