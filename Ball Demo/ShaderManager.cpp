#include "ShaderManager.h"



void ShaderManager::InitShaders(void)
{
	COLOR_ONLY		= LoadShader("Shaders/minimal.vert","Shaders/minimal.frag");
	TOON			= LoadShader("Shaders/celShading.vert","Shaders/celShading.frag");
	SKYBOX			= LoadShader("Shaders/Skybox.vert","Shaders/Skybox.frag");
	PARTICLE		= LoadShader("Shaders/particle.vert", "Shaders/particle.frag");
	TEXTURE			= LoadShader("Shaders/texture.vert","Shaders/texture.frag");
	TEXTURE_ONLY	= LoadShader("Shaders/textshader.vert","Shaders/textshader.frag");
	GRIND		= LoadShader("Shaders/grind.vert", "Shaders/grind.frag");

}

GLuint ShaderManager::LoadShader(char *vertFile, char *fragFile)
{
	//check the shaderList to see if this is already loaded
	itr = shaderTable.find(vertFile);

	if(itr != shaderTable.end())
	{
		//this shader is already present
		return itr->second;
	}

	GLuint p, f, v;

	v = glCreateShader(GL_VERTEX_SHADER);
	f = glCreateShader(GL_FRAGMENT_SHADER);

	const char *vertSource = LoadFile(vertFile);
	const char *fragSource = LoadFile(fragFile);

	glShaderSource(v, 1, &vertSource, 0);
	glShaderSource(f, 1, &fragSource, 0);
	
	GLint compiled, linked;

	glCompileShader(v);
	glGetShaderiv(v, GL_COMPILE_STATUS, &compiled);
	if (!compiled)
	{
		std::cout << "Vertex shader not compiled." << std::endl;
		ShaderManager::PrintShaderInfoLog(v);
	} 

	glCompileShader(f);
	glGetShaderiv(f, GL_COMPILE_STATUS, &compiled);
	if (!compiled)
	{
		std::cout << "Fragment shader not compiled." << std::endl;
		ShaderManager::PrintShaderInfoLog(f);
	} 
	
	p = glCreateProgram();
		
	glAttachShader(p,v);
	glAttachShader(p,f);
	
	glBindAttribLocation(p,ATTRIBUTE_VERTEX,"in_Position");
	glBindAttribLocation(p,ATTRIBUTE_NORMAL,"in_Normal");
	glBindAttribLocation(p,ATTRIBUTE_COLOR,"in_Color");
	glBindAttribLocation(p,ATTRIBUTE_TEXTURE0,"in_TexCoord");

	glLinkProgram(p);
	glGetProgramiv(p, GL_LINK_STATUS, &linked);
	if (!linked)
	{
		std::cout << "Program not linked." << std::endl;
	}
	glUseProgram(p);

	return p;
}

char* ShaderManager::LoadFile(char *fileName)
{
	int size;
	char * filetext;

	// file read based on example in cplusplus.com tutorial
	// ios::ate opens file at the end
	std::ifstream file (fileName, std::ios::in|std::ios::binary|std::ios::ate);
	if (file.is_open())
	{
		size = (int) file.tellg(); // get location of file pointer i.e. file size
		filetext = new char [size];
		file.seekg (0, std::ios::beg);
		file.read (filetext, size);
		file.close();
		filetext[size] = 0;
		std::cout << "file " << fileName << " loaded" << std::endl;
	}
	else
	{
		std::cout << "Unable to open file " << fileName << std::endl;
		// should ideally set a flag or use exception handling
		// so that calling function can decide what to do now
	}
	return filetext;
}

void ShaderManager::PrintShaderInfoLog(GLint shader)
{
	int infoLogLen = 0;
	int charsWritten = 0;
	GLchar *infoLog;

	glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLogLen);

	// should additionally check for OpenGL errors here

	if (infoLogLen > 0)
	{
		infoLog = new GLchar[infoLogLen];
		// error check for fail to allocate memory omitted
		glGetShaderInfoLog(shader,infoLogLen, &charsWritten, infoLog);
		std::cout << "InfoLog:" << std::endl << infoLog << std::endl;
		delete [] infoLog;
	}

	// should additionally check for OpenGL errors here
}

void ShaderManager::ClearShaders(void)
{
	glDeleteProgram(COLOR_ONLY);
	glDeleteProgram(TOON);
	glDeleteProgram(SKYBOX);
	glDeleteProgram(PARTICLE);
	glDeleteProgram(TEXTURE);
	glDeleteProgram(TEXTURE_ONLY);
}