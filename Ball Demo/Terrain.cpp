#include "Terrain.h"

Terrain::Terrain(void)
{
}

Terrain::~Terrain(void)
{
}

void Terrain::ApplyTexture(GLuint texture)
{
	this->texture = texture;
}

void Terrain::ApplyShader(GLuint shader)
{
	this->shader = shader;
}

void Terrain::genVerts(int _sizeX, int _sizeZ)
{
	
	tempVerts = new glm::vec3[numVerts];

	float stepX = (float)_sizeX / numCellsWide;
	float stepZ = (float)_sizeZ / numCellsHigh;

	//set start position
	glm::vec3 pos =  glm::vec3(0.0f, 0.0f, 0.0f);

	int count = 0;
	//loop across and up
	for(int z = 0; z < numVertsZ; z++)
	{
		pos.x = 0.0f;

		for(int x = 0; x < numVertsX; x++)
		{
			//Create the verts
			tempVerts[count].x = pos.x;
			tempVerts[count].y = height->getHeight(x,z) / 50;
			tempVerts[count].z = pos.z;
			//increment x across
			pos.x += stepX;

			count++;

		}
		//increment Z
		pos.z+=stepZ;
	}
}

void Terrain::createIndexList(void)
{
	int count = 0;
	int vIndex = 0;

	indices = new int[numVerts * 6];

	for(int z = 0; z < numCellsHigh; z++)
	{
		for(int x = 0; x < numCellsWide; x++)
		{
			//first traingle
			indices[count++] = vIndex;
			indices[count++] = vIndex + numVertsX;
			indices[count++] = vIndex + numVertsX + 1;


			//second triangle
			indices[count++] = vIndex;
			indices[count++] = vIndex + 1;
			indices[count++] = vIndex + numVertsX + 1;

			vIndex++;	
		}

		vIndex++;
	}

	indexSize = count;

}

void Terrain::calcNormals(void)
{
	tempNormals = new glm::vec3[numVerts];
	float zSize = (GLfloat)numVertsZ;
	float xSize = (GLfloat)numVertsX;
	int count = 0;
	glm::vec3 v1, v2, v3, v4, v5, v6 , n1, n2, n3, n4, n5, n6, tempNorm;

	for(int z = 0; z < numVertsZ-1; z++)
	{

		for(int x = 0; x < numVertsX-1; x++)
		{
			if(z == 0)
			{
				if(x == 0)
				{
					v1 = glm::normalize(tempVerts[numVertsX] - tempVerts[0]);
					v2 = glm::normalize(tempVerts[1] - tempVerts[0]);

					tempNormals[count] = v1 * v2;
				}
				else
				if(x == (numVertsX-1))
				{
					v1 = glm::normalize(tempVerts[count - 1] - tempVerts[count]);
					v2 = glm::normalize(tempVerts[count + (numVertsX-1)] - tempVerts[count]);

					n1 = v1 * v2;

					v3 = glm::normalize(tempVerts[count + numVertsX] - tempVerts[count]);

					n2 = v2 * v3;

					tempNorm.x = (n1.x + n2.x)/2;
					tempNorm.y = (n1.y + n2.y)/2;
					tempNorm.z = (n1.z + n2.z)/2;
					tempNormals[count] = tempNorm;

				}
				else
				{
					v1 = glm::normalize(tempVerts[count-1] - tempVerts[count]);
					v2 = glm::normalize(tempVerts[count+(numVertsX-1)] - tempVerts[count]);

					n1 = v1 * v2;

					v3 = glm::normalize(tempVerts[count + numVertsX] - tempVerts[count]);

					n2 = v2 * v3;

					v4 = glm::normalize(tempVerts[count+1] - tempVerts[count]);

					n3 = v3 * v4;

					tempNorm.x = (n1.x + n2.x + n3.x)/3;
					tempNorm.y = (n1.y + n2.y + n3.y)/3;
					tempNorm.z = (n1.z + n2.z + n3.z)/3;
					tempNormals[count] = tempNorm;

				}
			}
			else
			if(x == 0)
			{
				if(z == (numVertsZ -1))
				{
					v1 = glm::normalize(tempVerts[count - numVertsX] - tempVerts[count]);
					v2 = glm::normalize(tempVerts[count - (numVertsX - 1)] - tempVerts[count]);

					n1 = v1 * v2;

					v3 = glm::normalize(tempVerts[count + 1] - tempVerts[count]);

					n2 = v2 * v3;

					tempNorm.x = (n1.x + n2.x)/2;
					tempNorm.y = (n1.y + n2.y)/2;
					tempNorm.z = (n1.z + n2.z)/2;
					tempNormals[count] = tempNorm;
				}
				else
				{
					v1 = glm::normalize(tempVerts[count - numVertsX] - tempVerts[count]);
					v2 = glm::normalize(tempVerts[count - (numVertsX-1)] - tempVerts[count]);

					n1 = v1 * v2;

					v3 = glm::normalize(tempVerts[count + 1] - tempVerts[count]);

					n2 = v2 * v3;

					v4 = glm::normalize(tempVerts[count + numVertsX] - tempVerts[count]);

					n3 = v3 * v3;

					tempNorm.x = (n1.x + n2.x + n3.x)/3;
					tempNorm.y = (n1.y + n2.y + n3.y)/3;
					tempNorm.z = (n1.z + n2.z + n3.z)/3;
					tempNormals[count] = tempNorm;

				}
			}
			else
			if(x == numVertsX-1)
			{
				if(z == numVertsZ-1)
				{
					v1 = glm::normalize(tempVerts[count - 1] - tempVerts[count]);
					v2 = glm::normalize(tempVerts[count - numVertsX] - tempVerts[count]);

					tempNormals[count] = v1 * v2;

				}
				else
				{
					v1 = glm::normalize(tempVerts[count - numVertsX] - tempVerts[count]);
					v2 = glm::normalize(tempVerts[count - 1] - tempVerts[count]);

					n1 = v1 * v2;

					v3 = glm::normalize(tempVerts[count + (numVertsX-1)] - tempVerts[count]);

					n2 = v2 * v3;

					v4 = glm::normalize(tempVerts[count + numVertsX] - tempVerts[count]);

					n3 = v3 * v4;

					tempNorm.x = (n1.x + n2.x + n3.x)/3;
					tempNorm.y = (n1.y + n2.y + n3.y)/3;
					tempNorm.z = (n1.z + n2.z + n3.z)/3;
					tempNormals[count] = tempNorm;

				}
			}
			else
			if(z == numVertsZ - 1)
			{
				v1 = glm::normalize(tempVerts[count - 1] - tempVerts[count]);
				v2 = glm::normalize(tempVerts[count - numVertsX] - tempVerts[count]);

				n1 = v1 * v2;

				v3 = glm::normalize(tempVerts[count - (numVertsX-1)] - tempVerts[count]);

				n2 = v2 * v3;

				v4 = glm::normalize(tempVerts[count + 1] - tempVerts[count]);

				n3 = v3 * v4;

				tempNorm.x = (n1.x + n2.x + n3.x)/3;
				tempNorm.y = (n1.y + n2.y + n3.y)/3;
				tempNorm.z = (n1.z + n2.z + n3.z)/3;
				tempNormals[count] = tempNorm;

			}
			else
			{
				v1 = glm::normalize(tempVerts[count - numVertsX] - tempVerts[count]);
				v2 = glm::normalize(tempVerts[count - (numVertsX-1)] - tempVerts[count]);

				n1 = v1 * v2;

				v3 = glm::normalize(tempVerts[count + 1] - tempVerts[count]);

				n2 = v2 * v3;

				v4 = glm::normalize(tempVerts[count + numVertsX] - tempVerts[count]);

				n3 = v3 * v4;

				v5 = glm::normalize(tempVerts[count + (numVertsX-1)] - tempVerts[count]);

				n4 = v4 * v5;

				v6 = glm::normalize(tempVerts[count - 1] - tempVerts[count]);

				n5 = v5 * v6;

				n6 = v6 * v1;

				tempNorm.x = (n1.x + n2.x + n3.x + n4.x + n5.x + n6.x)/6;
				tempNorm.y = (n1.y + n2.y + n3.y + n4.y + n5.y + n6.y)/6;
				tempNorm.z = (n1.z + n2.z + n3.z + n4.z + n5.z + n6.z)/6;
				tempNormals[count] = tempNorm;

			}


			count++;
		}
	}

}

void Terrain::calcTexCoords(void)
{
	tempTexCoords = new glm::vec3[numVerts];
	int count = 0;

	for(int z = 0; z < numVertsZ; z++)
	{
		for(int x = 0; x < numVertsX; x++)
		{			
			tempTexCoords[count].x = (float)x / numVertsX;
			tempTexCoords[count].y = (float)z / numVertsZ;

			count++;
		}
	}

}

void Terrain::indexVerts(void)
{
	int indexValue = 0;
	vertices = new glm::vec3[indexSize];
	normals = new glm::vec3[indexSize];
	texCoords = new glm::vec2[indexSize];

	for(int i = 0; i < indexSize; i++)
	{
		indexValue = indices[i];

		vertices[i].x = tempVerts[indexValue].x;
		vertices[i].y = tempVerts[indexValue].y;
		vertices[i].z = tempVerts[indexValue].z;

		normals[i].x = tempNormals[indexValue].x;
		normals[i].y = tempNormals[indexValue].y;
		normals[i].z = tempNormals[indexValue].z;

		texCoords[i].x = tempTexCoords[indexValue].x;
		texCoords[i].y = tempTexCoords[indexValue].y;

	}

}

void Terrain::Generate(int _numCellsX, int _numCellsZ, int _sizeX, int _sizeZ)
{
	numCellsWide = _numCellsX;
	numCellsHigh = _numCellsZ;

	numVertsX = numCellsWide + 1;
	numVertsZ = numCellsHigh + 1;

	numVerts = numVertsX * numVertsZ;


	//////////////////////////////////
	//Light Settings/////////////////
	////////////////////////////////
	light0.ambient = glm::vec4( 0.7, 0.7, 0.7, 1.0 );
	light0.diffuse = glm::vec4(0.7, 0.7, 0.7, 1.0 );
	light0.specular = glm::vec4(0.7, 0.7, 0.7, 1.0 );

	lightPosition = glm::vec4(-1.0, 5.0, -4.0, 1.0 );


	/////////////////////////////////
	//Material setting//////////////
	///////////////////////////////
	material0.ambient = glm::vec4(0.5, 0.5, 0.5, 1.0);
	material0.diffuse = glm::vec4(1.0, 1.0, 1.0, 1.0);
	material0.specular = glm::vec4(0.5, 0.5, 0.5, 1.0);
	material0.shininess = 1.0;


	height = new HeightField();
	height->Create("Res/heightmap.bmp", numVertsX, numVertsZ);

	/////////////////////////////////
	/////GENERATE VERTICES//////////
	///////////////////////////////
	genVerts(_sizeX, _sizeZ);

	///////////////////////////////////////////
	/////CREATE INDEX LIST////////////////////
	/////////////////////////////////////////
	createIndexList();


	///////////////////////////////////////////
	//Gernerate Normals///////////////////////
	/////////////////////////////////////////
	calcNormals();
	

	////////////////////////////////////////////
	////Calculate Texture Coordinates//////////
	//////////////////////////////////////////
	calcTexCoords();


	//////////////////////////////////////////
	//INDEX THE VERTICES/////////////////////
	////////////////////////////////////////
	indexVerts();


	initBuffers();
}

void Terrain::initBuffers(void)
{
	glGenVertexArrays(1, &vao); // Allocate & assign Vertex Array Object (VAO)
	glBindVertexArray(vao); // Bind VAO as current object



	glGenBuffers(3, vbo); // Allocate three Vertex Buffer Object (VBO)
	
	glBindBuffer(GL_ARRAY_BUFFER, vbo[0]); //Bind 1st VBO as active uffer object
	//Copy the vertex data from model to VBO
	glBufferData(GL_ARRAY_BUFFER, indexSize * sizeof(glm::vec3), vertices, GL_STATIC_DRAW);
	// Position data is going into attribute index 0 & has 3 floats per vertex
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);  //Enable attribute index 0 (position)

	glBindBuffer(GL_ARRAY_BUFFER, vbo[1]); //Bind 2nd VBO as active uffer object
	//Copy the normal data from Model to the VBO
	glBufferData(GL_ARRAY_BUFFER, indexSize * sizeof(glm::vec3), normals, GL_STATIC_DRAW);
	// Normal data is going into attribute index 1 & has 3 floats per vertex
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(2);  //Enable attribute index 1 (Normals)

	glBindBuffer(GL_ARRAY_BUFFER, vbo[2]); //Bind 3rd VBO as active uffer object
	//Copy the texture coord data from Model to the VBO
	glBufferData(GL_ARRAY_BUFFER, indexSize * sizeof(glm::vec2), texCoords, GL_STATIC_DRAW);
	// Normal data is going into attribute index 1 & has 2 floats per vertex
	glVertexAttribPointer(4, 2, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(4);  //Enable attribute index 2 (texCoords)

	glBindVertexArray(0);
}

void Terrain::Update(glm::vec3 position)
{
	this->terrainX = (GLint)position.x  - 1125.0f;
	this->terrainY = (GLint)position.y  - 3500.0f;
	this->terrainZ = (GLint)position.z  - 1700.0f;
}

void Terrain::Render(MatrixStack &projectionStack, MatrixStack &modelviewStack)
{
	//glDisable(GL_CULL_FACE);
	glBindVertexArray(vao); // Bind VAO as current object
	glUseProgram(shader);


	glm::mat4 terrainModelView = modelviewStack.getMatrix();


	terrainModelView = glm::translate(terrainModelView, glm::vec3(terrainX, terrainY, terrainZ));

	terrainModelView = glm::scale(terrainModelView, glm::vec3(750.0f,950.0f,750.0f));

	


	int uniformIndex = glGetUniformLocation(shader, "projection");
	glUniformMatrix4fv(uniformIndex, 1,GL_FALSE,glm::value_ptr(projectionStack.getMatrix()));
	uniformIndex = glGetUniformLocation(shader, "modelview");
	glUniformMatrix4fv(uniformIndex, 1,GL_FALSE,glm::value_ptr(terrainModelView));
	

	// light0 is C++ struct similar to GLSL struct shown
	uniformIndex = glGetUniformLocation(shader, "light.ambient");
	glUniform4fv(uniformIndex, 1, glm::value_ptr(light0.ambient ));
	
	uniformIndex = glGetUniformLocation(shader, "light.diffuse");
	glUniform4fv(uniformIndex, 1, glm::value_ptr(light0.diffuse));
	
	uniformIndex = glGetUniformLocation(shader, "light.specular");
	glUniform4fv(uniformIndex, 1, glm::value_ptr(light0.specular));

	lightPosition = glm::vec4(-1.0, 1.0, -4.0, 1.0 );


	uniformIndex = glGetUniformLocation(shader, "lightPosition");
	glUniform4fv(uniformIndex, 1, glm::value_ptr(lightPosition));


	// material0 is C++ struct similar to GLSL struct shown
	uniformIndex = glGetUniformLocation(shader, "material.ambient");
	glUniform4fv(uniformIndex, 1, glm::value_ptr(material0.ambient));
	
	uniformIndex = glGetUniformLocation(shader, "material.diffuse");
	glUniform4fv(uniformIndex, 1, glm::value_ptr(material0.diffuse));
	
	uniformIndex = glGetUniformLocation(shader, "material.specular");
	glUniform4fv(uniformIndex, 1, glm::value_ptr(material0.specular));
	
	uniformIndex = glGetUniformLocation(shader, "material.shininess");
	glUniform1f(uniformIndex, material0.shininess);
	
	
	glBindTexture(GL_TEXTURE_2D, texture);
	glDrawArrays(GL_TRIANGLES, 0, indexSize);


	glUseProgram(0);
	glBindVertexArray(0);

	//glEnable(GL_CULL_FACE);
}