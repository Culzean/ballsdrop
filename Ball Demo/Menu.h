#pragma once

#ifndef MENU_H
#define MENU_H

#include "State.h"
#include "TextArea.h"

class Menu : public State
{
	public:
		Menu(void);
		~Menu(void);
		void Init(WindowManager windowManager, ShaderManager shaderManager, int musicSelection);
		void Update(GLfloat dt);
		void GetInput(CXBOXController *Player1);
		void KeyHandler( SDL_Event sdlEvent);
		void Render(bool WIRE_FRAME);
		void Cleanup(void);
		int  GetSelection(void) { return selection; }
		int  GetMusic(void) {return 0;}
		float GetTime(void) { return 0;}
		void AddTime(float time) {;}

		void SetSelection(int value) {;}

	private:
		MatrixStack projectionStack;
		MatrixStack modelviewStack;
		TextArea *single;
		TextArea *multi;
		TextArea *options;
		TextArea *quit;
		XINPUT_STATE prevState;
		SoundBank *soundBank;
		int selection;
};

#endif