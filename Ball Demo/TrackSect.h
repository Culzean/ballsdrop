#ifndef TRACKSECT_H
#define TRACKSECT_H

#include "Mesh.h"

#include "stafx.h"
#include <vector>

class Plane;
class MatrixStack;
class PhysicsManager;

class TrackSect {

public:
	TrackSect(PhysicsManager* _physics) ;
	~TrackSect();

	Plane*	getPlane(glm::vec3 _point);
	void	addPlane( Plane* lhs )		{	planes.push_back(lhs);	this->iNumbPlanes++;	};

	Plane* getStart()					{	return planes[0];	};
	Plane* getPlane(GLint index)			{	return planes[index];	}

	bool findDimensions();
	
	void CompPoints(  );

	GLfloat getWidth()					{	return width;	}
	GLfloat getDepth()					{	return depth;	}
	GLfloat getHeight()					{	return height;	}

	glm::vec3 getPos()					{	return pos;	}	

	std::vector<Plane*>	getPlanes()		{	return planes;	}
	GLint GetNumb()						{	return planes.size();	}

	

private:
	//vector pointing where the next turn is
	PhysicsManager* physics;
	
	GLfloat width, depth, height;
	glm::vec3 pos;
	glm::vec3 startPos;

	void setDimensions(glm::vec3 _bottomLeft, glm::vec3 _topRight);

	std::vector<Plane*> planes;
	//std::vector<glm::vec3> points;
	GLint iNumbPlanes;
};

#endif