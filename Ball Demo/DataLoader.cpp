#include "DataLoader.h"

//requires a file with a header tag that states the size of the array.

DataLoader::DataLoader(){
}

GLfloat* DataLoader::loadData(char* fileName){
	GLfloat *returnArray;			//instansiate an array of GLfloat.
	int i = 0;						//counter to keep track of the array.
	float f;						//counter to keep track of values read in by the character stream.
	int numFloats;					//variable to handle the header tag in the txt file, to establish the necessary size of the array. 
	char *inname = fileName;		//a character string to hold the namee of the file.
	std::ifstream infile(inname);	//open the file to be read.
	infile >> numFloats;			//read in the first line of the file aka the tag.
	returnArray = new GLfloat[numFloats];	//initialise the array, with the size established with the tag, cast to int as tag is a char. 
	while(infile >> f){						//loop while there is something in file to be read.
		returnArray[i] = f;					//add contents of f to the array.
		i++;								//increment through the array.
	}
	return returnArray;
}

GLubyte* DataLoader::loadGLubyte (char* fileName){
	GLubyte *returnArray;			//instansiate an array of GLfloat.
	int i = 0;						//counter to keep track of the array.
	float f;						//counter to keep track of values read in by the character stream.
	int numFloats;					//variable to handle the header tag in the txt file, to establish the necessary size of the array. 
	char *inname = fileName;		//a character string to hold the namee of the file.
	std::ifstream infile(inname);	//open the file to be read.
	infile >> numFloats;			//read in the first line of the file aka the tag.
	returnArray = new GLubyte [numFloats];	//initialise the array, with the size established with the tag, cast to int as tag is a char. 
	while(infile >> f){						//loop while there is something in file to be read.
		returnArray[i] = (GLubyte)f;					//add contents of f to the array.
		i++;								//increment through the array.
	}
	return returnArray;
}

char* DataLoader::loadFile(char* fname)
{
	//probably want this to be vectors
	//using new here and can't be reponsible for the delete here!
	int size;
	char * filetext;

	// file read based on example in cplusplus.com tutorial
	// ios::ate opens file at the end
	std::ifstream file (fname, std::ios::in|std::ios::binary|std::ios::ate);
	if (file.is_open())
	{
		size = (int) file.tellg(); // get location of file pointer i.e. file size
		filetext = new char [size];
		file.seekg (0, std::ios::beg);
		file.read (filetext, size);
		file.close();
		filetext[size] = 0;
		std::cout << "file " << fname << " loaded" << std::endl;
	}
	else
	{
		std::cout << "Unable to open file " << fname << std::endl;
		// should ideally set a flag or use exception handling
		// so that calling function can decide what to do now
	}
	return filetext;
}

DataLoader::~DataLoader(){
}