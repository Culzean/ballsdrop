#include "Finish.h"

Finish::Finish()
{
	selection = 0;
	musicSelection = 0;
	subMenu = false;
}

Finish::~Finish()
{
}

void Finish::Init(WindowManager windowManager, ShaderManager shaderManager, int musicSelection)
{
	projectionStack.loadMatrix(glm::perspective(45.0f, 4.0f / 3.0f, 0.5f, 1000.f));
	soundBank = SoundBank::GetInstance();
	SDL_Color white = {255, 255, 255};
	SDL_Color blue = {0, 0, 100};
	score1 = new TextArea(glm::vec3(0.0, 1.0, 0.0), "digital-7.ttf", "0", blue, shaderManager);
	score2 = new TextArea(glm::vec3(0.0, 0.0, 0.0), "digital-7.ttf", "0", blue, shaderManager);
	score3 = new TextArea(glm::vec3(0.0, -1.0, 0.0), "digital-7.ttf", "0", blue, shaderManager);
	you = new TextArea(glm::vec3(0.0, 0.0, 0.0), "GROBOLD.ttf", "You: ", blue, shaderManager);

	ok = new TextArea(glm::vec3(-2.0, -4.0, -3.0), "Res/a.bmp", shaderManager);
	back = new TextArea(glm::vec3(1.2, -4.0, -3.0), "Res/b.bmp", shaderManager);
	resume = new TextArea(glm::vec3(-0.5, -3.3, 0.0), "GROBOLD.ttf", "RESUME", blue, shaderManager);
	menu = new TextArea(glm::vec3(2.0, -3.3, 0.0), "GROBOLD.ttf", "MENU", blue, shaderManager);

	score1->UpdateTextFloat(scores[0]);
	score2->UpdateTextFloat(scores[1]);
	score3->UpdateTextFloat(scores[2]);

	if (scores[0] == tempTime)
	{
		you->SetPosition(score1->GetPosition() - glm::vec3(1.0, 0.0, 0.0));
	}
	if (scores[1] == tempTime)
	{
		you->SetPosition(score2->GetPosition() - glm::vec3(1.0, 0.0, 0.0));
	}
	if (scores[2] == tempTime)
	{
		you->SetPosition(score3->GetPosition() - glm::vec3(1.0, 0.0, 0.0));
	}
}

void Finish::AddTime(float time)
{
	std::ifstream scoreReader;
	scoreReader.open("scores.txt");
	scoreReader >> scores[0];
	scoreReader >> scores[1];
	scoreReader >> scores[2];
	scoreReader.close();

	if (time < scores[0])
	{
		scores[2] = scores[1];
		scores[1] = scores[0];
		scores[0] = time;
	}
	else if (time < scores[1])
	{
		scores[2] = scores[1];
		scores[1] = time;
	}
	else if (time < scores[2])
	{
		scores[2] = time;
	}

	std::ofstream scoreMaker;
	scoreMaker.open("scores.txt");
	scoreMaker << scores[0] << ' ' << scores[1] << ' ' << scores[2];
	scoreMaker.close();

	tempTime = time;
}

void Finish::Update(GLfloat dt)
{
}

void Finish::GetInput(CXBOXController *Player1)
{
}

void Finish::Render(bool WIRE_FRAME)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	if (WIRE_FRAME)
		glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	else
		glClearColor(0.0f, 0.8f, 0.4f, 1.0f);

	modelviewStack.loadIdentity();
	modelviewStack.push();

	score1->Render(projectionStack, modelviewStack, WIRE_FRAME, true);
	score2->Render(projectionStack, modelviewStack, WIRE_FRAME, true);
	score3->Render(projectionStack, modelviewStack, WIRE_FRAME, true);
	you->Render(projectionStack, modelviewStack, WIRE_FRAME, true);

	ok->Render(projectionStack, modelviewStack, WIRE_FRAME, true);
	resume->Render(projectionStack, modelviewStack, WIRE_FRAME, true);
	back->Render(projectionStack, modelviewStack, WIRE_FRAME, true);
	menu->Render(projectionStack, modelviewStack, WIRE_FRAME, true);

	modelviewStack.pop();
}

void Finish::Cleanup(void)
{
	delete score1;
	delete score2;
	delete score3;
	delete you;
}