#ifdef _MSC_VER
	#pragma once
#endif

#ifndef PHYSICS_MODEL_H
#define PHYSICS_MODEL_H

#include <vector>

#include "Ball.h"
#include "RaceTrack.h"
#include "PhysicsManager.h"
#include "CXBOXController.h"
#include "RaceTrack.h"
#include "DebugRend.h"

class RaceTrack;
class PhysicsManager;
class DebugRend;

extern const GLfloat GRAVITY;

class PhysicsModel
{
	public:
		PhysicsModel(RaceTrack *raceTrack);
		~PhysicsModel();

		void AddBall(Ball *ball);
		void AddWall(void);
		void Update(glm::vec3 cameraVec, GLfloat dt);
		Plane* GetCurrentPlane(int index) { return collisionLookup[index]; }
		glm::quat GetNewOrientation(void);
		void HandleSideCollision(Ball *ball, Plane *currentPlane);
		void Cleanup(void);
	


		void addDebug(DebugRend* _debug)		{	debug = _debug;	}

	private:
		DebugRend		*debug;
		PhysicsManager *physics;
		RaceTrack *track;
		Plane *currentPlane;
		TrackSect *currentSect;
		std::vector<Ball*> balls;
		std::vector< Plane* >collisionLookup;

		void ResolveBallCollision( Ball* b0, Ball* b1 );
};

#endif