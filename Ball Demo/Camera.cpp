#include "Camera.h"
#include <cmath>

//Camera Constants
const glm::vec3 CAMERA_ACCELERATION = glm::vec3(8.0f, 8.0f, 8.0f);
const float   CAMERA_FOVX = 90.0f;
const glm::vec3 CAMERA_POS = glm::vec3(0.0f, 1.0f, 0.0f);
const float   CAMERA_SPEED_ROTATION = 0.2f;
const float   CAMERA_SPEED_FLIGHT_YAW = 100.0f;
const glm::vec3 CAMERA_VELOCITY = glm::vec3(2.0f, 2.0f, 2.0f);
const float   CAMERA_ZFAR = 100.0f;
const float   CAMERA_ZNEAR = 0.1f;

const glm::vec3 Camera::WORLD_XAXIS(1.0f, 0.0f, 0.0f);
const glm::vec3 Camera::WORLD_YAXIS(0.0f, 1.0f, 0.0f);
const glm::vec3 Camera::WORLD_ZAXIS(0.0f, 0.0f, 1.0f);

Camera::Camera(){

	EPSILON = 1e-6f;
	c_Properties = CAMERA_FIRST_PERSON;

	c_Fovx = CAMERA_FOVX;
	c_ZNear = CAMERA_ZNEAR;
	c_ZFar =  CAMERA_ZFAR;
	c_AspectRatio = 0.0f;

	c_PitchDegrees = 0.0f;

	c_Eye = glm::vec3(0.0f, 0.0f, 0.0f);
	c_XAxis = glm::vec3(1.0f, 0.0f, 0.0f);
	c_YAxis = glm::vec3(0.0f, 1.0f, 0.0f);
	c_ZAxis = glm::vec3(0.0f, 0.0f, 1.0f);
	c_ViewDir = glm::vec3(0.0f, 0.0f, -1.0f);

	c_Acceleration = glm::vec3(0.0f, 0.0f, 0.0f);
	c_CurrentVelocity = glm::vec3(0.0f, 0.0f, 0.0f);
	c_Velocity = glm::vec3(0.0f, 0.0f, 0.0f);

	identity = glm::mat4(1.0);
	c_Orientation = glm::quat_cast(identity);
	
	c_IdMatrix = glm::mat4(1.0);
	c_ViewMatrix = glm::mat4(1.0);
	c_ProjMatrix = glm::mat4(1.0);
}

Camera::Camera(glm::mat4 &modelview, glm::mat4 &projection){

	EPSILON = 1e-6f;
	c_Properties = CAMERA_FIRST_PERSON;

	c_Fovx = CAMERA_FOVX;
	c_ZNear = CAMERA_ZNEAR;
	c_ZFar =  CAMERA_ZFAR;
	c_AspectRatio = 0.0f;
	c_AspectRatio = 0.0f;

	c_PitchDegrees = 0.0f;

	c_Eye = glm::vec3(0.0f, 0.0f, 0.0f);
	c_XAxis = glm::vec3(1.0f, 0.0f, 0.0f);
	c_YAxis = glm::vec3(0.0f, 1.0f, 0.0f);
	c_ZAxis = glm::vec3(0.0f, 0.0f, 1.0f);
	c_ViewDir = glm::vec3(0.0f, 0.0f, 1.0f);

	c_Acceleration = glm::vec3(0.0f, 0.0f, 0.0f);
	c_CurrentVelocity = glm::vec3(0.0f, 0.0f, 0.0f);
	c_Velocity = glm::vec3(0.0f, 0.0f, 0.0f);

	identity = glm::mat4(1.0);
	c_Orientation = glm::quat_cast(identity);

	c_ViewMatrix = modelview;
	c_ProjMatrix = projection;
	c_IdMatrix = glm::mat4(1.0);
}

Camera::~Camera(){

}

void Camera::LookAt(const glm::vec3 &target){
	LookAt(c_Eye, target, c_YAxis);
}

void Camera::LookAt(const glm::vec3 &eye, const glm::vec3 &target, const glm::vec3 &up){
	c_Eye = eye;

	c_ZAxis = eye - target;
	glm::normalize(c_ZAxis);

	c_ViewDir = -c_ZAxis;

	c_XAxis = glm::cross(up, c_ZAxis);
	glm::normalize(c_XAxis);

	c_YAxis = glm::cross(c_ZAxis, c_XAxis);
	glm::normalize(c_YAxis);
	glm::normalize(c_XAxis);

	c_ViewMatrix[0][0] = c_XAxis.x;
	c_ViewMatrix[1][0] = c_XAxis.y;
	c_ViewMatrix[2][0] = c_XAxis.z;
	c_ViewMatrix[3][0] = -glm::dot(c_XAxis, eye);

	c_ViewMatrix[0][1] = c_YAxis.x;
	c_ViewMatrix[1][1] = c_YAxis.y;
	c_ViewMatrix[2][1] = c_YAxis.z;
	c_ViewMatrix[3][1] = -glm::dot(c_YAxis, eye);

	c_ViewMatrix[0][2] = c_ZAxis.x;
	c_ViewMatrix[1][2] = c_ZAxis.y;
	c_ViewMatrix[2][2] = c_ZAxis.z;
	c_ViewMatrix[3][2] = -glm::dot(c_ZAxis, eye);

	//Take the pitch angle from the view matrix
	c_PitchDegrees = glm::degrees(asinf(c_ViewMatrix[1][2]));

	c_Orientation = glm::quat_cast(c_ViewMatrix);
}

void Camera::Move(float x, float y, float z){
	//Move the camera by x world units to the left or right
	//By Y world units Upwards or Downards
	//By Z world units forwards or backwards

	glm::vec3 eye = c_Eye;
	glm::vec3 forward;

	if(c_Properties == CAMERA_FIRST_PERSON){
		// Calculate the forwards direction. Can't just use the camera's local
		// z axis as doing so will cause the camera to move more slowly as the
		// camera's view approaches 90 degrees straight up and down.
		forward = glm::cross(WORLD_YAXIS, c_XAxis);
		glm::normalize(forward);
	}else{
		forward = c_ViewDir;
	}

	eye += c_XAxis * x;
	eye += WORLD_YAXIS * y;
	eye += forward * z;

	SetPosition(eye);
}

void Camera::Move(const glm::vec3 &direction, const GLfloat &amount){
	//Move the camera the specified amount of world units 
	//in the specified direction in world space
	c_Eye.x += direction.x * amount;
	c_Eye.y += direction.y * amount;
	c_Eye.z += direction.z * amount;

	UpdateViewMatrix();
}

void Camera::Move(const glm::vec3 &direction, const glm::vec3 &amount){
	//Move the camera the specified amount of world units 
	//in the specified direction in world space
	c_Eye.x += direction.x * amount.x;
	c_Eye.y += direction.y * amount.y;
	c_Eye.z += direction.z * amount.z;

	UpdateViewMatrix();
}

void Camera::Perspective(float fovx, float aspect, float znear, float zfar){
	//Create a projection matrix based on the horizontal field of view
	// "fovx" rather than the traditional vertical field of view "fovy"

	float e = 1.0f / tanf(glm::degrees(fovx) / 2.0f);
	float aspectInv = 1.0f / aspect;
	float fovy = 2.0f * atanf(aspectInv / e);
	float xScale = 1.0f / tanf(0.5f * fovy);
	float yScale = xScale / aspectInv;

	c_ProjMatrix[0][0] = xScale;
	c_ProjMatrix[0][1] = 0.0f;
	c_ProjMatrix[0][2] = 0.0f;
	c_ProjMatrix[0][3] = 0.0f;

	c_ProjMatrix[1][0] = 0.0f;
	c_ProjMatrix[1][1] = yScale;
	c_ProjMatrix[1][2] = 0.0f;
	c_ProjMatrix[1][3] = 0.0f;

	c_ProjMatrix[2][0] = 0.0f;
	c_ProjMatrix[2][1] = 0.0f;
	c_ProjMatrix[2][2] = (zfar + znear) / (znear - zfar);
	c_ProjMatrix[2][3] = -1.0f;

	c_ProjMatrix[3][0] = 0.0f;
	c_ProjMatrix[3][1] = 0.0f;
	c_ProjMatrix[3][2] = (2.0f * zfar * znear) / (znear - zfar);
	c_ProjMatrix[3][3] = 0.0f;

	c_Fovx = fovx;
	c_AspectRatio = aspect;
	c_ZNear = znear;
	c_ZFar = zfar;
}

void Camera::Rotate(float headingDegrees, float pitchDegrees, float rollDegrees){
	//Rotates the camera based on it current property
	//Not all behaivours support rolling.

	pitchDegrees = -pitchDegrees;
	headingDegrees = -headingDegrees;
	rollDegrees = -rollDegrees;

	switch(c_Properties){
	case CAMERA_FIRST_PERSON:
		RotateFirstPerson(headingDegrees, pitchDegrees);
		break;
	case CAMERA_FLIGHT:
		RotateFlight(headingDegrees, pitchDegrees, rollDegrees);
		break;
	}
	UpdateViewMatrix();
}

void Camera::RotateFlight(float headingDegrees, float pitchDegrees, float rollDegrees){
	//Implements the rotation for the flight style camera behaviour.
	glm::quat rot;

	//rot = (glm::quat)glm::yawPitchRoll(headingDegrees, pitchDegrees, rollDegrees);
	rot = (glm::quat)glm::angleAxis(headingDegrees, glm::vec3(0.0f, 1.0f, 0.0f));
	c_Orientation =  c_Orientation * rot;


	rot = (glm::quat)glm::angleAxis(pitchDegrees, glm::vec3(1.0f, 0.0f, 0.0f));
	c_Orientation = rot * c_Orientation ;
}

void Camera::RotateFirstPerson(float headingDegrees, float pitchDegrees){
	//Implement the rotation for the FPS style camera. Roll is ignored.
	c_PitchDegrees += pitchDegrees;

	if(c_PitchDegrees > 90.0f){
		pitchDegrees = 90.0f - (c_PitchDegrees - pitchDegrees);
		c_PitchDegrees = 90.0f;
	}

	if(c_PitchDegrees < -90.0f){
		pitchDegrees = -90.0f - (c_PitchDegrees - pitchDegrees);
		c_PitchDegrees = -90.0f;
	}

	glm::quat rot;
	//Rotate the camera aboout the world y axis
	//The order the quaternions are multiplied. THIS IS IMPORTANT!!!!
	if (headingDegrees != 0.0f){
		rot = glm::quat_cast(glm::axisAngleMatrix(WORLD_YAXIS, headingDegrees));
		c_Orientation = rot * c_Orientation;
	}

	// Rotate camera about its local x axis.
	// Note the order the quaternions are multiplied. That is important!
	if (pitchDegrees != 0.0f){
		rot = glm::quat_cast(glm::axisAngleMatrix(WORLD_XAXIS, pitchDegrees));
		c_Orientation = c_Orientation * rot;
	}
}
/*
//set the camera to face the direction the player is currently pointing

void Camera::ResetView(Ball* pBall) {
	
	SetPosition(pBall->GetPosition());
	glm::vec3 vec = pBall->getFrame()->GetForwardVector();
	Move(-vec, glm::vec3(3.0f) );
	LookAt(pBall->GetPosition());
	this->UpdateViewMatrix();
}*/



void Camera::SetAcceleration(float x, float y, float z){
	c_Acceleration = glm::vec3(x,y,z);
}

void Camera::SetAcceleration(const glm::vec3 &acceleration){
	c_Acceleration = acceleration;
}

void Camera::SetProperties(CamProperties newProperties){
	glm::vec3 worldYaxis = WORLD_YAXIS;
	if(c_Properties == CAMERA_FLIGHT && newProperties == CAMERA_FIRST_PERSON){
		// Moving from flight-simulator mode to first-person.
		// Need to ignore camera roll, but retain existing pitch and heading.
		LookAt(c_Eye,c_Eye + Inverse(c_ZAxis), worldYaxis);
	}
	c_Properties = newProperties;
}

void Camera::SetCurrentVelocity(float x, float y, float z){
	c_CurrentVelocity = glm::vec3(x, y, z);
}

void Camera::SetCurrentVelocity(const glm::vec3 &currentVelocity){
	c_CurrentVelocity = currentVelocity;
}

void Camera::SetOrientation(const glm::quat &orientation){

	glm::mat4 m = glm::mat4_cast(orientation);

	c_PitchDegrees = glm::degrees(asinf(m[1][2]));
	c_Orientation = orientation;

	glm::vec3 worldYaxis = WORLD_YAXIS;
	if(c_Properties == CAMERA_FIRST_PERSON){
		LookAt(c_Eye, c_Eye + c_ViewDir, worldYaxis); 
	}

	UpdateViewMatrix();
}

void Camera::SetPosition(float x, float y, float z){
	c_Eye = glm::vec3(x, y, z);
	UpdateViewMatrix();
}

void Camera::SetPosition(const glm::vec3 &position){
	c_Eye = position;
	UpdateViewMatrix();
}

void Camera::SetVelocity(float x, float y, float z){
	c_Velocity = glm::vec3(x, y, z);
}

void Camera::SetVelocity(const glm::vec3 &velocity){
	c_Velocity = velocity;
}

void Camera::SetViewDirection(float x, float y, float z){
	c_ViewDir = glm::vec3(x,y,z);
}

void Camera::SetViewDirection(glm::vec3 &direction){
	c_ViewDir = direction;
	UpdateViewMatrix();
}

void Camera::SetViewMatrix(const glm::mat4 &modelview){
	c_ViewMatrix = modelview;
}

void Camera::SetProjMatrix(const glm::mat4 &projection){
	c_ProjMatrix = projection;
}

void Camera::UpdatePosition(const glm::vec3 &direction, float secondsPassed){
	// Moves the camera using Newton's second law of motion. Unit mass is
	// assumed here to somewhat simplify the calculations. The direction vector
	// is in the range [-1,1].

	if(MagnitudeSqaured(c_CurrentVelocity) != 0.0f){
		//Only move the camera if the velocity vector is not of zero length.
		//Doing this gaurds against the camera slowly creeping around due to
		//floating point rounding errors.
		glm::vec3 displacement = (c_CurrentVelocity * secondsPassed) + 
			(0.5f * c_Acceleration * secondsPassed * secondsPassed);

		// Floating point rounding errors will slowly accumulate and cause the
		// camera to move along each axis. To prevent any unintended movement
		// the displacement vector is clamped to zero for each direction that
		// the camera isn't moving in. Note that the updateVelocity() method
		// will slowly decelerate the camera's velocity back to a stationary
		// state when the camera is no longer moving along that direction. To
		// account for this the camera's current velocity is also checked.
		if(direction.x == 0.0f && CloseEnough(c_CurrentVelocity.x, 0.0f)){
			displacement.x = 0.0f;
		}
		if(direction.y == 0.0f && CloseEnough(c_CurrentVelocity.y, 0.0f)){
			displacement.y = 0.0f;
		}
		if(direction.z == 0.0f && CloseEnough(c_CurrentVelocity.z, 0.0f)){
			displacement.z = 0.0f;
		}
	}

	// Continuously update the camera's velocity vector even if the camera
	// hasn't moved during this call. When the camera is no longer being moved
	// the camera is decelerating back to its stationary state.
	UpdateVelocity(direction, secondsPassed);
}

void Camera::UpdateVelocity(const glm::vec3 &direction, float secondsPassed){
	//Updates the camereas celocity based on the supplied movement direction
	//& the sceonds passed (since this method was last called). The movement
	//direction is in the range[-1,1].
	if(direction.x != 0.0f){
		//Camera us moving along the x axis.
		//Lineraly decelerate back to stationary state.

		c_CurrentVelocity.x += direction.x * c_Acceleration.x * secondsPassed;

		if(c_CurrentVelocity.x > c_Velocity.x){
			c_CurrentVelocity.x = c_Velocity.x;
		}
		if(c_CurrentVelocity.x < -c_Velocity.x){
			c_CurrentVelocity.x = -c_Velocity.x;
		}
	}else{
		//Camera is no longer moving along the x axis.
		//Lineraly decelerate back to stationary state.
		if(c_CurrentVelocity.x > 0.0f){
			if((c_CurrentVelocity.x -= c_Acceleration.x * secondsPassed) < 0.0f){
				c_CurrentVelocity.x = 0.0f;
			}
		}else{
			if((c_CurrentVelocity.x += c_Acceleration.x * secondsPassed) > 0.0f){
				c_CurrentVelocity.x = 0.0f;
			}
		}

		if (direction.y != 0.0f){
			// Camera is moving along the y axis.
			// Linearly accelerate up to the camera's max speed.

			c_CurrentVelocity.y += direction.y * c_Acceleration.y * secondsPassed;

			if (c_CurrentVelocity.y > c_Velocity.y)
				c_CurrentVelocity.y = c_Velocity.y;
			else if (c_CurrentVelocity.y < -c_Velocity.y)
				c_CurrentVelocity.y = -c_Velocity.y;
		}
		else{
			// Camera is no longer moving along the y axis.
			// Linearly decelerate back to stationary state.

			if (c_CurrentVelocity.y > 0.0f){
				if ((c_CurrentVelocity.y -= c_Acceleration.y * secondsPassed) < 0.0f)
					c_CurrentVelocity.y = 0.0f;
			}
			else{
				if ((c_CurrentVelocity.y += c_Acceleration.y * secondsPassed) > 0.0f)
					c_CurrentVelocity.y = 0.0f;
			}
		}

		if (direction.z != 0.0f){
			// Camera is moving along the z axis.
			// Linearly accelerate up to the camera's max speed.
			c_CurrentVelocity.z += direction.z * c_Acceleration.z * secondsPassed;

			if (c_CurrentVelocity.z > c_Velocity.z)
				c_CurrentVelocity.z = c_Velocity.z;
			else if (c_CurrentVelocity.z < -c_Velocity.z)
				c_CurrentVelocity.z = -c_Velocity.z;
		}
		else{
			// Camera is no longer moving along the z axis.
			// Linearly decelerate back to stationary state.
			if (c_CurrentVelocity.z > 0.0f){
				if ((c_CurrentVelocity.z -= c_Acceleration.z * secondsPassed) < 0.0f)
					c_CurrentVelocity.z = 0.0f;
			}
			else{
				if ((c_CurrentVelocity.z += c_Acceleration.z * secondsPassed) > 0.0f)
					c_CurrentVelocity.z = 0.0f;
			}
		}
	}
}

void Camera::UpdateViewMatrix(){
	//Reconstruct the view matrix
	c_ViewMatrix = glm::mat4_cast(c_Orientation);

	c_XAxis = glm::vec3(c_ViewMatrix[0][0], c_ViewMatrix[1][0], c_ViewMatrix[2][0]);
	c_YAxis = glm::vec3(c_ViewMatrix[0][1], c_ViewMatrix[1][1], c_ViewMatrix[2][1]);
	c_ZAxis = glm::vec3(c_ViewMatrix[0][2], c_ViewMatrix[1][2], c_ViewMatrix[2][2]);
	c_ViewDir = -c_ZAxis;

	c_ViewMatrix[3][0] = -glm::dot(c_XAxis, c_Eye);
	c_ViewMatrix[3][1] = -glm::dot(c_YAxis, c_Eye);
	c_ViewMatrix[3][2] = -glm::dot(c_ZAxis, c_Eye);
}
